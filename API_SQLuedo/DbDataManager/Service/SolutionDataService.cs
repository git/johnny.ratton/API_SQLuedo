using DbContextLib;
using Entities;
using Shared;

namespace DbDataManager.Service;

public class SolutionDataService : ISolutionService<SolutionEntity>
{
    private UserDbContext DbContext { get; set; }

    public SolutionDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }

    public SolutionEntity GetSolutionByInquiryId(int id)
    {
        var solution = DbContext.Solutions.FirstOrDefault(s => s.OwnerId == id);
        if (solution == null)
        {
            throw new ArgumentException($"Impossible de trouver la solution pour l'enquête d'id {id}", nameof(id));
        }
        return solution;
    }
}