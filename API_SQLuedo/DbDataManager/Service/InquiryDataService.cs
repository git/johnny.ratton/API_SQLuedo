﻿using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;
using Shared;

namespace DbDataManager.Service;

public class InquiryDataService : IInquiryService<InquiryEntity>
{
    private UserDbContext DbContext { get; set; }

    public InquiryDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }

    public IEnumerable<InquiryEntity> GetInquiries(int page, int number, InquiryOrderCriteria orderCriteria)
    {
        if (page <= 0)
        {
            page = 1;
        }
        if (number <= 0)
        {
            number = 10;
        }
        IQueryable<InquiryEntity> query = DbContext.Inquiries;
        switch (orderCriteria)
        {
            case InquiryOrderCriteria.None:
                break;
            case InquiryOrderCriteria.ByTitle:
                query = query.OrderBy(s => s.Title);
                break;
            case InquiryOrderCriteria.ByDescription:
                query = query.OrderBy(s => s.Description);
                break;
            case InquiryOrderCriteria.ByIsUser:
                query = query.OrderBy(s => s.IsUser);
                break;
            case InquiryOrderCriteria.ById:
                query = query.OrderBy(s => s.Id);
                break;
            default:
                break;
        }

        query = query.Skip((page - 1) * number).Take(number);
        var inquiries = query.ToList();
        return inquiries;
    }

    public int GetNumberOfInquiries()
    {
        return DbContext.Inquiries.Count();
    }

    public InquiryEntity GetInquiryById(int id)
    {
        var inquiryEntity = DbContext.Inquiries.FirstOrDefault(i => i.Id == id);
        if (inquiryEntity == null)
        {
            throw new ArgumentException("Impossible de trouver l'enquête", nameof(id));
        }

        return inquiryEntity;
    }

    public InquiryEntity GetInquiryByTitle(string title)
    {
        var inquiryEntity = DbContext.Inquiries.FirstOrDefault(i => i.Title == title);
        if (inquiryEntity == null)
        {
            throw new ArgumentException("Impossible de trouver l'enquête", nameof(title));
        }

        return inquiryEntity;
    }

    public InquiryEntity CreateInquiry(string title, string description, bool isUser)
    {
        var newInquiryEntity = new InquiryEntity
        {
            Title = title,
            Description = description,
            IsUser = isUser
        };
        DbContext.Inquiries.Add(newInquiryEntity);
        DbContext.SaveChangesAsync();
        return newInquiryEntity;
    }

    public bool DeleteInquiry(int id)
    {
        var inquiryEntity = DbContext.Inquiries.FirstOrDefault(u => u.Id == id);
        if (inquiryEntity == null)
        {
            return false;
        }

        DbContext.Inquiries.Remove(inquiryEntity);
        DbContext.SaveChanges();
        return true;
    }

    public InquiryEntity UpdateInquiry(int id, InquiryEntity inquiry)
    {
        var updatingInquiry = DbContext.Inquiries.FirstOrDefault(u => u.Id == id);
        if (updatingInquiry == null)
        {
            throw new ArgumentException("Impossible de trouver l'enquête", nameof(id));
        }

        foreach (var pptt in typeof(InquiryEntity).GetProperties()
                     .Where(p => p.CanWrite && p.Name != nameof(InquiryEntity.Id)))
        {
            pptt.SetValue(updatingInquiry, pptt.GetValue(inquiry));
        }

        DbContext.SaveChangesAsync();
        return updatingInquiry;
    }
}