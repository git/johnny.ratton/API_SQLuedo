using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;
using Shared;

namespace DbDataManager.Service;

public class BlackListDataService : IBlackListService<BlackListEntity>
{
    private UserDbContext DbContext { get; set; }

    public BlackListDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }
    
    public int GetNumberOfBannedUsers()
    {
        return DbContext.BlackLists.Count();
    }
    public IEnumerable<BlackListEntity> GetBannedUsers(int page, int number, BlackListOdrerCriteria orderCriteria)
    {
        if (page <= 0)
        {
            page = 1;
        }

        if (number <= 0)
        {
            number = 10;
        }
        IQueryable<BlackListEntity> query = DbContext.BlackLists.Skip((page - 1) * number).Take(number);
        switch (orderCriteria)
        {
            case BlackListOdrerCriteria.None:
                break;
            case BlackListOdrerCriteria.ByEmail:
                query = query.OrderBy(s => s.Email);
                break;
            case BlackListOdrerCriteria.ByExpirationDate:
                query = query.OrderBy(s => s.ExpirationDate);
                break;
            default:
                break;
        }

        var blackList = query.ToList();
        return blackList;
    }
    public BlackListEntity? GetUserBannedByEmail(string email)
    {
        var blackListEntity = DbContext.BlackLists.FirstOrDefault(b => b.Email == email);
        return blackListEntity;
    }

    public bool BanUser(string username)
    {
        var userEntity = DbContext.Users.FirstOrDefault(u => u.Username == username);
        if (userEntity == null)
        {
            return false;
        }

        DbContext.BlackLists.Add(new BlackListEntity
            { Email = userEntity.Email, ExpirationDate = DateOnly.FromDateTime(DateTime.Now.AddYears(2)) });
        DbContext.Users.Remove(userEntity);
        DbContext.SaveChangesAsync();
        return true;
    }
    
    public bool UnbanUser(string email)
    {
        var blackListEntity = DbContext.BlackLists.FirstOrDefault(b => b.Email == email);
        if (blackListEntity == null)
        {
            return false;
        }

        DbContext.BlackLists.Remove(blackListEntity);
        DbContext.SaveChangesAsync();
        return true;
    }
}