using DbContextLib;
using Entities;
using Shared;

namespace DbDataManager.Service;

public class NotepadDataService : INotepadService<NotepadEntity>
{
    private UserDbContext DbContext { get; set; }

    public NotepadDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }
    public NotepadEntity GetNotepadFromUserAndInquiryId(int userId, int inquiryId)
    {
        var user = DbContext.Users.FirstOrDefault(u => u.Id == userId);
        if (user == null)
        {
            throw new ArgumentException("Erreur, aucun utilisateur ne possède l'ID fourni");
        }
        var inquiry = DbContext.Inquiries.FirstOrDefault(i => i.Id == inquiryId);
        if (inquiry == null)
        {
            throw new ArgumentException("Erreur, aucune enquête ne possède l'ID fourni");
        }
        var notepad = DbContext.Notepads.FirstOrDefault(n => n.UserId == userId && n.InquiryId == inquiryId);
        if (notepad == null)
        {
            throw new ArgumentException("Erreur, aucun bloc-notes n'existe pour l'utilisateur et l'enquête donnés");
        }
        return notepad;
    }

    public void SetNotepadFromUserAndInquiryId(int userId, int inquiryId, string notes)
    {
        var user = DbContext.Users.FirstOrDefault(u => u.Id == userId);
        if (user == null)
        {
            throw new ArgumentException("Erreur, aucun utilisateur ne possède l'ID fourni");
        }
        var inquiry = DbContext.Inquiries.FirstOrDefault(i => i.Id == inquiryId);
        if (inquiry == null)
        {
            throw new ArgumentException("Erreur, aucune enquête ne possède l'ID fourni");
        }
        var notepad = DbContext.Notepads.FirstOrDefault(n => n.UserId == userId && n.InquiryId == inquiryId);
        if (notepad != null)
        {
            notepad.Notes = notes;
            //throw new ArgumentException("Erreur, un bloc-notes existe déjà pour l'utilisateur et l'enquête donnée");
        }
        else
        {
            DbContext.Notepads.Add(new NotepadEntity { UserId = userId, InquiryId = inquiryId, Notes = notes });
        }

        DbContext.SaveChangesAsync();
    }

    public void UpdateNotepadFromUserAndInquiryId(int userId, int inquiryId, string notes)
    {
        var notepad = GetNotepadFromUserAndInquiryId(userId, inquiryId);
        notepad.Notes = notes;
        DbContext.SaveChangesAsync();
    }
}