﻿using DbContextLib;
using Entities;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;
using Shared;

namespace DbDataManager.Service;

public class LessonDataService : ILessonService<LessonEntity>
{
    private UserDbContext DbContext { get; set; }

    public LessonDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }

    public IEnumerable<LessonEntity> GetLessons(int page, int number, LessonOrderCriteria orderCriteria)
    {
        if (page <= 0)
        {
            page = 1;
        }

        if (number <= 0)
        {
            number = 10;
        }

        IQueryable<LessonEntity> query = DbContext.Lessons;
        switch (orderCriteria)
        {
            case LessonOrderCriteria.None:
                break;
            case LessonOrderCriteria.ByTitle:
                query = query.OrderBy(s => s.Title);
                break;
            case LessonOrderCriteria.ByLastPublisher:
                query = query.OrderBy(s => s.LastPublisher);
                break;
            case LessonOrderCriteria.ByLastEdit:
                query = query.OrderBy(s => s.LastEdit);
                break;
            case LessonOrderCriteria.ById:
                query = query.OrderBy(s => (int) s.Id);
                break;
            default:
                break;
        }

        query = query.Skip((page - 1) * number).Take(number);
        

        var lessons = query.ToList();
        return lessons;
    }

    public int GetNumberOfLessons()
    {
        return DbContext.Lessons.Count();
    }

    public LessonEntity GetLessonById(int id)
    {
        var lessonEntity = DbContext.Lessons.FirstOrDefault(u => u.Id == id);
        if (lessonEntity == null)
        {
            throw new ArgumentException("Impossible de trouver la leçon", nameof(id));
        }

        return lessonEntity;
    }

    public LessonEntity GetLessonByTitle(string title)
    {
        var lessonEntity = DbContext.Lessons.FirstOrDefault(u => u.Title == title);
        if (lessonEntity == null)
        {
            throw new ArgumentException("Impossible de trouver la leçon", nameof(title));
        }

        return lessonEntity;
    }

    public bool DeleteLesson(int id)
    {
        var lessonEntity = DbContext.Lessons.FirstOrDefault(l => l.Id == id);
        if (lessonEntity == null)
            return false;

        DbContext.Lessons.Remove(lessonEntity);
        DbContext.SaveChanges();
        return true;
    }

    public LessonEntity UpdateLesson(int id, LessonEntity lesson)
    {
        var updatingLesson = DbContext.Lessons.FirstOrDefault(l => l.Id == id);
        if (updatingLesson == null)
            throw new ArgumentException("Impossible de trouver la leçon", nameof(id));

        updatingLesson.Title = lesson.Title;
        updatingLesson.LastPublisher = lesson.LastPublisher;
        updatingLesson.LastEdit = lesson.LastEdit;

        DbContext.SaveChanges();
        return updatingLesson;
    }

    public LessonEntity CreateLesson(int id, string title, string lastPublisher, DateOnly lastEdit)
    {
        var newLessonEntity = new LessonEntity()
        {
            Id = id > 0 && DbContext.Lessons.All(l => l.Id != id) ? id : 0,
            Title = title,
            LastPublisher = lastPublisher,
            LastEdit = lastEdit,
        };

        DbContext.Lessons.Add(newLessonEntity);
        DbContext.SaveChanges();
        return newLessonEntity;
    }
}