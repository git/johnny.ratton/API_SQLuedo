﻿using DbContextLib;
using Entities;
using Model.OrderCriteria;
using Shared;

namespace DbDataManager.Service;

public class UserDataService : IUserService<UserEntity>
{
    private UserDbContext DbContext { get; set; }

    public UserDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }

    public UserEntity GetUserById(int id)
    {
        var userEntity = DbContext.Users.FirstOrDefault(u => u.Id == id);
        if (userEntity == null)
        {
            throw new ArgumentException("Impossible de trouver l'utilisateur", nameof(id));
        }

        return userEntity;
    }

    public UserEntity GetUserByUsername(string username)
    {
        var userEntity = DbContext.Users.FirstOrDefault(u => u.Username == username);
        if (userEntity == null)
        {
            throw new ArgumentException("Impossible de trouver l'utilisateur", nameof(username));
        }

        return userEntity;
    }

    public UserEntity GetUserByEmail(string email)
    {
        var userEntity = DbContext.Users.FirstOrDefault(u => u.Email == email);
        if (userEntity == null)
        {
            throw new ArgumentException("Impossible de trouver l'utilisateur", nameof(email));
        }

        return userEntity;
    }

    public IEnumerable<UserEntity> GetUsers(int page, int number, UserOrderCriteria orderCriteria)
    {
        if (page <= 0)
        {
            page = 1;
        }
        if (number <= 0)
        {
            number = 10;
        }

        IQueryable<UserEntity> query = DbContext.Users;
        switch (orderCriteria)
        {
            case UserOrderCriteria.None:
                break;
            case UserOrderCriteria.ById:
                query = query.OrderBy(s => s.Id);
                break;
            case UserOrderCriteria.ByUsername:
                query = query.OrderBy(s => s.Username);
                break;
            case UserOrderCriteria.ByEmail:
                query = query.OrderBy(s => s.Email);
                break;
            case UserOrderCriteria.ByIsAdmin:
                query = query.OrderBy(s => s.IsAdmin);
                break;
            default:
                break;
        }
        query = query.Skip((page - 1) * number).Take(number);

        var users = query.ToList();
        return users;
    }

    public IEnumerable<UserEntity> GetNotAdminUsers(int page, int number, UserOrderCriteria orderCriteria)
    {
        if (page <= 0)
        {
            page = 1;
        }
        if (number <= 0)
        {
            number = 10;
        }
        IQueryable<UserEntity> query = DbContext.Users.Where(u => u.IsAdmin == false).Skip((page - 1) * number).Take(number);
        switch (orderCriteria)
        {
            case UserOrderCriteria.None:
                break;
            case UserOrderCriteria.ById:
                query = query.OrderBy(s => s.Id);
                break;
            case UserOrderCriteria.ByUsername:
                query = query.OrderBy(s => s.Username);
                break;
            case UserOrderCriteria.ByEmail:
                query = query.OrderBy(s => s.Email);
                break;
            case UserOrderCriteria.ByIsAdmin:
                query = query.OrderBy(s => s.IsAdmin);
                break;
            default:
                break;
        }

        var users = query.ToList();
        return users;
    }
    public int GetNumberOfUsers()
    {
        return DbContext.Users.Count();
    }

    public bool DeleteUser(int id)
    {
        var userEntity = DbContext.Users.FirstOrDefault(u => u.Id == id);
        if (userEntity == null)
        {
            return false;
        }

        DbContext.Users.Remove(userEntity);
        DbContext.SaveChangesAsync();
        return true;
    }

    public bool DeleteUserByUsername(string username)
    {
        var userEntity = DbContext.Users.FirstOrDefault(u => u.Username == username);
        if (userEntity == null)
        {
            return false;
        }
        DbContext.Users.Remove(userEntity);
        DbContext.SaveChangesAsync();
        return true;
    }

    public UserEntity UpdateUser(int id, UserEntity user)
    {
        var updatingUser = DbContext.Users.FirstOrDefault(u => u.Id == id);
        if (updatingUser == null)
        {
            throw new ArgumentException("Impossible de trouver l'utilisateur", nameof(id));
        }

        foreach (var pptt in typeof(UserEntity).GetProperties()
                     .Where(p => p.CanWrite && p.Name != nameof(UserEntity.Id)))
        {
            pptt.SetValue(updatingUser, pptt.GetValue(user));
        }

        DbContext.SaveChangesAsync();
        return updatingUser;
    }

    public UserEntity CreateUser(string username, string password, string email, bool isAdmin)
    {
        var newUserEntity = new UserEntity
        {
            Username = username,
            Password = password,
            Email = email,
            IsAdmin = isAdmin
        };
        DbContext.Users.Add(newUserEntity);
        DbContext.SaveChangesAsync();
        return newUserEntity;
    }

    public bool IsEmailTaken(string email)
    {
        var isEmail = DbContext.Users.Any(u => u.Email == email);
        return isEmail;
    }

    public bool IsUsernameTaken(string username)
    {
        var isUsername = DbContext.Users.Any(u => u.Username == username);
        return isUsername;
    }

    public UserEntity PromoteUser(int id)
    {
        var userEdit = GetUserById(id);
        var newUserEntity = UpdateUser(id,new UserEntity{Id = id,Username = userEdit.Username,Password = userEdit.Password,Email = userEdit.Email,IsAdmin = true});
        DbContext.SaveChangesAsync();
        return newUserEntity;
    }
}