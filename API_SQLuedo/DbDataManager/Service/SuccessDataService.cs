﻿using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;
using Shared;

namespace DbDataManager.Service;

public class SuccessDataService : ISuccessService<SuccessEntity>
{
    private UserDbContext DbContext { get; set; }

    public SuccessDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }

    public IEnumerable<SuccessEntity> GetSuccesses(int page, int number, SuccessOrderCriteria orderCriteria)
    {
        if (page <= 0)
        {
            page = 1;
        }
        if (number <= 0)
        {
            number = 10;
        }
        IQueryable<SuccessEntity> query = DbContext.Successes.Skip((page - 1) * number).Take(number);
        switch (orderCriteria)
        {
            case SuccessOrderCriteria.None:
                break;
            case SuccessOrderCriteria.ByUserId:
                query = query.OrderBy(s => s.UserId);
                break;
            case SuccessOrderCriteria.ByInquiryId:
                query = query.OrderBy(s => s.InquiryId);
                break;
            case SuccessOrderCriteria.ByIsFinished:
                query = query.OrderBy(s => s.IsFinished);
                break;
            default:
                break;
        }

        var successes = query.ToList();
        return successes;
    }

    public IEnumerable<SuccessEntity> GetSuccessesByUserId(int id)
    {
        UserIdIsValid(id);
        var successes = DbContext.Successes.Where(u => u.UserId == id);
        return successes;
    }

    public IEnumerable<SuccessEntity> GetSuccessesByInquiryId(int id)
    {
        InquiryIdIsValid(id);
        var successes = DbContext.Successes.Where(u => u.InquiryId == id);
        return successes;
    }

    public bool DeleteSuccess(int idUser, int idInquiry)
    {
        var successEntity = DbContext.Successes.FirstOrDefault(u => u.UserId == idUser && u.InquiryId == idInquiry);
        if (successEntity == null)
        {
            return false;
        }

        DbContext.Successes.Remove(successEntity);
        DbContext.SaveChangesAsync();
        return true;
    }

    public SuccessEntity UpdateSuccess(int idUser, int idInquiry, SuccessEntity success)
    {
        AreValdIds(idUser, idInquiry);

        var updatingSuccess =
            DbContext.Successes.FirstOrDefaultAsync(u => u.UserId == idUser && u.InquiryId == idInquiry);
        if (updatingSuccess.Result == null) throw new ArgumentException("success", nameof(idUser));

        /*foreach (var pptt in typeof(SuccessEntity).GetProperties().Where(p =>
                     p.CanWrite && p.Name != nameof(SuccessEntity.UserId) && p.Name != nameof(SuccessEntity.InquiryId)))
        {
            pptt.SetValue(updatingSuccess, pptt.GetValue(success));
        }*/
        updatingSuccess.Result.IsFinished = success.IsFinished;
        DbContext.SaveChangesAsync();
        return updatingSuccess.Result;
    }

    public SuccessEntity CreateSuccess(int userId, int inquiryId, bool isFinished)
    {
        AreValdIds(userId, inquiryId);

        var newSuccessEntity = new SuccessEntity
        {
            UserId = userId,
            InquiryId = inquiryId,
            IsFinished = isFinished,
        };

        var success = DbContext.Successes.FirstOrDefaultAsync(s => s.UserId == userId && s.InquiryId == inquiryId);
        if (success.Result != null) throw new ArgumentException("success", nameof(userId));
        DbContext.Successes.Add(newSuccessEntity);
        DbContext.SaveChangesAsync();
        return newSuccessEntity;
    }

    private void AreValdIds(int userId, int inquiryId)
    {
        UserIdIsValid(userId);
        InquiryIdIsValid(inquiryId);
    }

    private void UserIdIsValid(int userId)
    {
        var user = DbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);
        if (user.Result == null)
        {
            throw new ArgumentException("userId");
        }
    }

    private void InquiryIdIsValid(int inquiryId)
    {
        var inquiry = DbContext.Inquiries.FirstOrDefaultAsync(u => u.Id == inquiryId);
        if (inquiry.Result == null)
        {
            throw new ArgumentException("inquiryId");
        }
    }
}