﻿using DbContextLib;
using Entities;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;
using Shared;

namespace DbDataManager.Service;

public class ParagraphDataService : IParagraphService<ParagraphEntity>
{
    private UserDbContext DbContext { get; set; }

    public ParagraphDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }

    public IEnumerable<ParagraphEntity> GetParagraphs(int page, int number, ParagraphOrderCriteria orderCriteria)
    {
        if (page <= 0)
        {
            page = 1;
        }
        if (number <= 0)
        {
            number = 10;
        }
        IQueryable<ParagraphEntity> query = DbContext.Paragraphs.Skip((page - 1) * number).Take(number);
        switch (orderCriteria)
        {
            case ParagraphOrderCriteria.None:
                break;
            case ParagraphOrderCriteria.ByTitle:
                query = query.OrderBy(s => s.Title);
                break;
            case ParagraphOrderCriteria.ByContent:
                query = query.OrderBy(s => s.Content);
                break;
            case ParagraphOrderCriteria.ByInfo:
                query = query.OrderBy(s => s.Info);
                break;
            case ParagraphOrderCriteria.ByQuery:
                query = query.OrderBy(s => s.Query);
                break;
            case ParagraphOrderCriteria.ByComment:
                query = query.OrderBy(s => s.Comment);
                break;
            default:
                break;
        }

        var paragraphs = query.ToList();
        return paragraphs;
    }

    public IEnumerable<ParagraphEntity> GetParagraphsByLessonId(int lessonId)
    {
        var lesson = DbContext.Lessons.FirstOrDefault(l => l.Id == lessonId);
        if (lesson == null)
        {
            throw new ArgumentException($"Erreur, la leçon ayant pour id {lessonId} est introuvable.");
        }
        var list = DbContext.Paragraphs.Where(p => p.LessonId == lessonId);
        return list;
    }

    public ParagraphEntity GetParagraphById(int id)
    {
        var paragraphEntity = DbContext.Paragraphs.FirstOrDefault(u => u.Id == id);
        if (paragraphEntity == null)
        {
            throw new ArgumentException("Impossible de trouver le paragraphe", nameof(id));
        }

        return paragraphEntity;
    }

    public ParagraphEntity GetParagraphByTitle(string title)
    {
        var paragraphEntity = DbContext.Paragraphs.FirstOrDefault(u => u.Title == title);
        if (paragraphEntity == null)
        {
            throw new ArgumentException("Impossible de trouver le paragraphe", nameof(title));
        }

        return paragraphEntity;
    }

    public bool DeleteParagraph(int id)
    {
        var paragraphEntity = DbContext.Paragraphs.FirstOrDefault(p => p.Id == id);
        if (paragraphEntity == null)
        {
            return false;
        }

        DbContext.Paragraphs.Remove(paragraphEntity);
        DbContext.SaveChanges();
        return true;
    }

    public ParagraphEntity UpdateParagraph(int id, ParagraphEntity paragraph)
    {
        var updatingParagraph = DbContext.Paragraphs.FirstOrDefault(p => p.Id == id);
        if (updatingParagraph == null)
        {
            throw new ArgumentException("Impossible de trouver le paragraphe", nameof(id));
        }

        foreach (var pptt in typeof(ParagraphEntity).GetProperties().Where(p =>
                     p.CanWrite && p.Name != nameof(ParagraphEntity.Id) && p.Name != nameof(ParagraphEntity.LessonId)))
        {
            pptt.SetValue(updatingParagraph, pptt.GetValue(paragraph));
        }
        /*updatingParagraph.ContentTitle = paragraph.ContentTitle;
        updatingParagraph.ContentContent = paragraph.ContentContent;
        updatingParagraph.Title = paragraph.Title;
        updatingParagraph.Content = paragraph.Content;
        updatingParagraph.Info = paragraph.Info;
        updatingParagraph.Query = paragraph.Query;
        updatingParagraph.Comment = paragraph.Comment;
        updatingParagraph.LessonId = paragraph.LessonId;*/
        DbContext.SaveChangesAsync();
        return updatingParagraph;
    }

    public ParagraphEntity CreateParagraph(string contentTitle, string contentContent, string title, string content, string info, string query, string comment,
        int lessonId)
    {
        var newParagraphEntity = new ParagraphEntity()
        {
            ContentContent = contentContent,
            ContentTitle = contentTitle,
            Title = title,
            Content = content,
            Info = info,
            Query = query,
            Comment = comment,
            LessonId = lessonId
        };
        DbContext.Paragraphs.Add(newParagraphEntity);
        DbContext.SaveChangesAsync();
        return newParagraphEntity;
    }
}