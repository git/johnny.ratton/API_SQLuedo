using DbContextLib;
using Entities;
using Shared;

namespace DbDataManager.Service;

public class InquiryTableDataService : IInquiryTableService<InquiryTableEntity>
{
    private UserDbContext DbContext { get; set; }

    public InquiryTableDataService(UserDbContext context)
    {
        DbContext = context;
        context.Database.EnsureCreated();
    }
    
    public string GetDatabaseNameByInquiryId(int id)
    {
        var inquiryTable = DbContext.InquiryTables.FirstOrDefault(i => i.OwnerId == id);
        if (inquiryTable == null)
        {
            throw new ArgumentException($"Erreur, impossible de trouver l'objet InquiryTable pour l'enquête d'id {id}",
                nameof(id));
        }
        return inquiryTable.DatabaseName;
    }
}