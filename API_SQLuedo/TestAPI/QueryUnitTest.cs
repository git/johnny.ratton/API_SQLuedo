﻿using API.Controllers;
using Castle.Components.DictionaryAdapter.Xml;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;
using TestAPI.Extensions;

namespace TestAPI;

public class QueryUnitTest
{
    private readonly Mock<IQueryService<QueryDto>> _queryService;

    public QueryUnitTest()
    {
        _queryService = new Mock<IQueryService<QueryDto>>();
    }

    [Fact]
    public void Select_Users_Success()
    {
        var userList = GetUsersData();
        _queryService.Setup(x => x.ExecuteQuery("Select * from \"User\";","SQLuedo"))
            .Returns(new QueryDto{Result = userList.ToString()});
        var queryController = new QueryController(new NullLogger<QueryController>(), _queryService.Object);

        var queryResult = queryController.ExecuteQuery("Select * from \"User\";", "SQLuedo");

        if (queryResult is OkObjectResult okObjectResult)
        {
            
            Assert.NotNull(okObjectResult);
        }
    }

    [Fact]
    public void Select_Users_Failed_Cause_Database_Doesnt_Exists()
    {
        var userList = GetUsersData();
        _queryService.Setup(x => x.ExecuteQuery("Select * from \"User\";", "SQLuedo"))
            .Returns(new QueryDto { Result = userList.ToString() });
        var queryController = new QueryController(new NullLogger<QueryController>(), _queryService.Object);

        var queryResult = queryController.ExecuteQuery("Select * from \"User\";", "LABASEDEDONNEES");

        if (queryResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)

        {
            Assert.IsNotType<OkObjectResult>(queryResult);
        }
    }

    [Fact]
    public void Get_Tables_Success()
    {
        var tablesList = GetTables();
        _queryService.Setup(x => x.GetTables("SQLuedo"))
            .Returns(new QueryDto { Result = tablesList.ToString() });
        var queryController = new QueryController(new NullLogger<QueryController>(), _queryService.Object);

        var queryResult = queryController.GetTables("SQLuedo");

        if (queryResult is OkObjectResult okObjectResult)
        {

            Assert.NotNull(okObjectResult);
        }
    }

    [Fact]
    public void Get_Tables_Failed_Cause_Database_Doesnt_Exist()
    {
        var tablesList = GetTables();
        _queryService.Setup(x => x.GetTables("SQLuedo"))
            .Returns(new QueryDto { Result = tablesList.ToString() });
        var queryController = new QueryController(new NullLogger<QueryController>(), _queryService.Object);

        var queryResult = queryController.GetTables("LABSEEEEEEEEEEEEEEEEEEEE");

        if (queryResult is OkObjectResult okObjectResult)
        {

            Assert.NotNull(okObjectResult);
        }
    }


    [Fact]
    public void Get_Columns_Success()
    {
        var tablesList = GetColumns();
        _queryService.Setup(x => x.GetColumns("SQLuedo","User"))
            .Returns(new QueryDto { Result = tablesList.ToString() });
        var queryController = new QueryController(new NullLogger<QueryController>(), _queryService.Object);

        var queryResult = queryController.GetColumns("SQLuedo","User");

        if (queryResult is OkObjectResult okObjectResult)
        {

            Assert.NotNull(okObjectResult);
        }
    }

    [Fact]
    public void Get_Columns_Failed_Cause_Database_Doesnt_Exist()
    {
        var tablesList = GetColumns();
        _queryService.Setup(x => x.GetColumns("SQLuedo", "User"))
            .Returns(new QueryDto { Result = tablesList.ToString() });
        var queryController = new QueryController(new NullLogger<QueryController>(), _queryService.Object);

        var queryResult = queryController.GetColumns("UDHUE", "User");

        if (queryResult is OkObjectResult okObjectResult)
        {

            Assert.NotNull(okObjectResult);
        }
    }

    [Fact]
    public void Get_Columns_Failed_Cause_Table_Doesnt_Exist()
    {
        var tablesList = GetColumns();
        _queryService.Setup(x => x.GetColumns("SQLuedo", "User"))
            .Returns(new QueryDto { Result = tablesList.ToString() });
        var queryController = new QueryController(new NullLogger<QueryController>(), _queryService.Object);

        var queryResult = queryController.GetColumns("SQLuedo", "GEGEIU");

        if (queryResult is OkObjectResult okObjectResult)
        {

            Assert.NotNull(okObjectResult);
        }
    }


    private List<UserDto> GetUsersData()
    {
        List<UserDto> usersData = new List<UserDto>(4)
        {
            new(
                0,
                "Useruser",
                "motdepasse",
                "adressemail@gmail.com",
                true
            ),
            new
            (
                1,
                "Leuser",
                "motdepasse",
                "deuxadresse@gmail.com",
                false
            ),
            new
            (
                2,
                "gygyggyg",
                "ennodlavehc",
                "thirdadress@gmail.com",
                false
            ),
            new
            (
                "ferferf",
                "h_nh_78",
                "fourthadress@gmail.com",
                false
            ),
        };
        return usersData;
    }


    private List<string> GetColumns()
    {
        List<string> columns = new List<string>(4)
        {
            "Id",
            "Name",
            "Password",
            "IsAdmin"
        };
        return columns;
    }



    private List<string> GetTables()
    {
        List<string> columns = new List<string>(4)
        {
            "User",
            "Solution",
            "Inquiry",
            "Success"
        };
        return columns;
    }
}