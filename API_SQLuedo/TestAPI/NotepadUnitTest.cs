using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;
using TestAPI.Extensions;

namespace TestAPI;

public class NotepadUnitTest
{
    private readonly Mock<INotepadService<NotepadDto>> _notepadService;

    public NotepadUnitTest()
    {
        _notepadService = new Mock<INotepadService<NotepadDto>>();
    }

    [Fact]
    public void GetNotepadFromUserAndInquiryId()
    {
        var notepad = new NotepadDto(42, 42, "These are notes example.");
        _notepadService.Setup(x => x.GetNotepadFromUserAndInquiryId(42, 42))
            .Returns(notepad);
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.GetNotepadByUserAndInquiryById(42,42);

        if (notepadResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;
            Assert.NotNull(valeur);
            Assert.Equal(notepad, valeur);
        }
    }
    
    [Fact]
    public void GetNotepadFromUserAndInquiryId_ThrowingArgumentException()
    {
        var notepad = new NotepadDto(42, 42, "These are notes example.");
        _notepadService.Setup(x => x.GetNotepadFromUserAndInquiryId(42, 10))
            .Throws<ArgumentException>();
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.GetNotepadByUserAndInquiryById(42,10);
        Assert.NotNull(notepadResult);
        Assert.Equal(typeof(NotFoundResult),notepadResult.GetType());
    }
    
    [Fact]
    public void SetNotepadFromUserAndInquiryId_Success()
    {
        _notepadService.Setup(x => x.SetNotepadFromUserAndInquiryId(42, 42,"These are notes example."));
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, "These are notes example.")); 
        Assert.NotNull(notepadResult);
        Assert.Equal(typeof(OkResult),notepadResult.GetType());
        Assert.Equal(typeof(OkObjectResult) , notepadController.GetNotepadByUserAndInquiryById(42,42).GetType());
    }
    
    [Fact]
    public void SetNotepadFromUserAndInquiryId_Negative_UserId()
    {
        _notepadService.Setup(x => x.SetNotepadFromUserAndInquiryId(-42, 42,"These are notes example.")).Throws<ArgumentException>();
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(-42,42, "These are notes example.")); 
        Assert.NotNull(notepadResult);
        Assert.Equal(typeof(BadRequestResult),notepadResult.GetType());
    }
    
    [Fact]
    public void SetNotepadFromUserAndInquiryId_Negative_InquiryId()
    {
        _notepadService.Setup(x => x.SetNotepadFromUserAndInquiryId(42, -42,"These are notes example.")).Throws<ArgumentException>();
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,-42, "These are notes example.")); 
        Assert.NotNull(notepadResult);
        Assert.Equal(typeof(BadRequestResult),notepadResult.GetType());
    }
    
    [Fact]
    public void SetNotepadFromUserAndInquiryId_Null_Notes()
    {
        _notepadService.Setup(x => x.SetNotepadFromUserAndInquiryId(42, 42,"These are notes example.")).Throws<ArgumentException>();
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, null)); 
        Assert.NotNull(notepadResult);
        Assert.Equal(typeof(BadRequestResult),notepadResult.GetType());
    }
    
    [Fact]
    public void SetNotepadFromUserAndInquiryId_Throws_ArgumentException()
    {
        var notepad = new NotepadDto(42, 42, "These are notes example.");
        _notepadService.Setup(x => x.SetNotepadFromUserAndInquiryId(42, 42,"These are notes example.")).Throws<ArgumentException>();
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(notepad); 
        Assert.NotNull(notepadResult);
        Assert.Equal(typeof(NotFoundResult),notepadResult.GetType());
    }
    
    
    [Fact]
    public void UpdateNotepadFromUserAndInquiryId_Success()
    {
        _notepadService.Setup(x => x.UpdateNotepadFromUserAndInquiryId(42, 42,"These are the new notes"));
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, "These are notes example."));
        Assert.NotNull(notepadResult);
        Assert.Equal(typeof(OkResult),notepadResult.GetType());
        Assert.Equal(typeof(OkObjectResult) , notepadController.GetNotepadByUserAndInquiryById(42,42).GetType());
        var updateResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, "These are the new notes"));
        Assert.NotNull(updateResult);
        Assert.Equal(typeof(OkResult),updateResult.GetType());
    }
    
    [Fact]
    public void UpdateNotepadFromUserAndInquiryId_Negative_UserId()
    {
        _notepadService.Setup(x => x.UpdateNotepadFromUserAndInquiryId(42, 42,"These are the new notes"));
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, "These are notes example.")); 
        var updateResult = notepadController.UpdateNotepadByUserAndInquiryById(new NotepadDto(-42,42, "These are the new notes")); 
        Assert.NotNull(updateResult);
        Assert.Equal(typeof(BadRequestResult),updateResult.GetType());
    }
    
    [Fact]
    public void UpdateNotepadFromUserAndInquiryId_Negative_InquiryId()
    {
        _notepadService.Setup(x => x.UpdateNotepadFromUserAndInquiryId(42, -42,"These are the new notes"));
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, "These are notes example.")); 
        var updateResult = notepadController.UpdateNotepadByUserAndInquiryById(new NotepadDto(42,-42, "These are the new notes")); 
        Assert.NotNull(updateResult);
        Assert.Equal(typeof(BadRequestResult),updateResult.GetType());
    }
    
    [Fact]
    public void UpdateNotepadFromUserAndInquiryId_Null_Notes()
    {
        _notepadService.Setup(x => x.UpdateNotepadFromUserAndInquiryId(42, 42,null)).Throws<ArgumentException>();
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, "These are notes example.")); 
        var updateResult = notepadController.UpdateNotepadByUserAndInquiryById(new NotepadDto(42,42, null)); 
        Assert.NotNull(updateResult);
        Assert.Equal(typeof(BadRequestResult),updateResult.GetType());
    }
    
    [Fact]
    public void UpdateNotepadFromUserAndInquiryId_Throws_ArgumentException()
    {
        _notepadService.Setup(x => x.UpdateNotepadFromUserAndInquiryId(42, 10,"These are the new notes")).Throws<ArgumentException>();
        var notepadController =
            new NotepadController(_notepadService.Object, new NullLogger<NotepadController>());

        var notepadResult = notepadController.SetNotepadByUserAndInquiryById(new NotepadDto(42,42, "These are notes example.")); 
        var updateResult = notepadController.UpdateNotepadByUserAndInquiryById(new NotepadDto(42,10, "These are the new notes")); 
        Assert.NotNull(updateResult);
        Assert.Equal(typeof(NotFoundResult),updateResult.GetType());
    }
}