using Dto;

namespace TestAPI.Extensions;

public class BlackListDtoEqualityComparer : EqualityComparer<BlackListDto>
{
    public override bool Equals(BlackListDto x, BlackListDto y)
    {
        return x.Email == y.Email;
    }

    public override int GetHashCode(BlackListDto obj)
    {
        return obj.Email.GetHashCode();
    }
}