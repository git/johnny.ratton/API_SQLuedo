﻿using Dto;

namespace TestAPI.Extensions;

class InquiryIdEqualityComparer : EqualityComparer<InquiryDto>
{
    public override bool Equals(InquiryDto x, InquiryDto y)
    {
        return x.Id == y.Id;
    }

    public override int GetHashCode(InquiryDto obj)
    {
        return obj.Id;
    }
}