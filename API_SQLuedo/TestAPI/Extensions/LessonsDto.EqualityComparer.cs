﻿using Dto;

namespace TestAPI.Extensions;

class LessonIdEqualityComparer : EqualityComparer<LessonDto>
{
    public override bool Equals(LessonDto x, LessonDto y)
    {
        return x.Id == y.Id;
    }

    public override int GetHashCode(LessonDto obj)
    {
        return obj.Id;
    }
}