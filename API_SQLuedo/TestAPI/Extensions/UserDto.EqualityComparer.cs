﻿using Dto;

namespace TestAPI.Extensions;

class UserIdEqualityComparer : EqualityComparer<UserDto>
{
    public override bool Equals(UserDto x, UserDto y)
    {
        return x.Id == y.Id;
    }

    public override int GetHashCode(UserDto obj)
    {
        return obj.Id;
    }
}