﻿using Dto;

namespace TestAPI.Extensions;

class SuccessIdEqualityComparer : EqualityComparer<SuccessDto>
{
    public override bool Equals(SuccessDto x, SuccessDto y)
    {
        return x.UserId == y.UserId && x.InquiryId == y.InquiryId;
    }

    public override int GetHashCode(SuccessDto obj)
    {
        return obj.UserId * obj.InquiryId;
    }
}