﻿using Dto;

namespace TestAPI.Extensions;

class ParagraphIdEqualityComparer : EqualityComparer<ParagraphDto>
{
    public override bool Equals(ParagraphDto x, ParagraphDto y)
    {
        return x.Id == y.Id;
    }

    public override int GetHashCode(ParagraphDto obj)
    {
        return obj.Id;
    }
}