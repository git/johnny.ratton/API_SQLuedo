using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;
using TestAPI.Extensions;

namespace TestAPI;

public class UserUnitTest
{
    private readonly Mock<IUserService<UserDto>> _userService;

    public UserUnitTest()
    {
        _userService = new Mock<IUserService<UserDto>>();
    }

    [Fact]
    public void GetNumberOfUsersSuccess()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetNumberOfUsers())
            .Returns(userList.Count);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetNumberOfUsers();

        if (userResult is OkObjectResult okObjectResult)
        {
            var valeur = (okObjectResult.Value as KeyValuePair<string, int>?);
            Assert.NotNull(valeur);
            Assert.Equal(userList.Count, valeur.Value.Value);
        }
    }


    [Fact]
    public void GetNumberOfUsers_Fail_Cause_Not_Found()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetNumberOfUsers())
            .Returns(0);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetNumberOfUsers();

        if (userResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)

        {
            Assert.IsNotType<OkObjectResult>(userResult);
        }
    }

    [Fact]
    public void GetUsersListSuccess()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUsers(1, 4, 0))
            .Returns(userList);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUsers(1, 4, 0);

        if (userResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetUsersData().ToString(), valeur.ToString());
            Assert.True(userList.SequenceEqual(valeur as IEnumerable<UserDto>, new UserIdEqualityComparer()));
        }
    }

    [Fact]
    public void GetUserListFail()
    {
        _userService.Setup(x => x.GetUsers(1, 4, 0))
            .Returns(new List<UserDto>());
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUsers(2, 3, 0);

        if (userResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)

        {
            Assert.IsNotType<OkObjectResult>(userResult);
        }
    }

    [Fact]
    public void GetNotAdminUsersListSuccess()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetNotAdminUsers(1, 4, 0))
            .Returns(userList.Where(u => u.IsAdmin == false));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetNotAdminUsers(1, 4, 0);

        if (userResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)

        {
            Assert.IsNotType<OkObjectResult>(userResult);
        }
    }

    [Fact]
    public void GetNotAdminUsersListFail_Cause_Not_Found()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetNotAdminUsers(1, 4, 0))
            .Returns(userList.Where(u => u.IsAdmin == false));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetNotAdminUsers(100, 4, 0);

        if (userResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetUsersData().ToString(), valeur.ToString());
            Assert.True(userList.Where(u => u.IsAdmin == false)
                .SequenceEqual(valeur as IEnumerable<UserDto>, new UserIdEqualityComparer()));
        }
    }


    [Fact]
    public void GetUserIdSuccess()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUserById(1))
            .Returns(userList[1]);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUserById(1);
        if (userResult is OkObjectResult okObjectResult)
        {
            UserDto valeur = okObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.Equal("Leuser", valeur.Username);
            Assert.Equal("motdepasse", valeur.Password);
            Assert.Equal("deuxadresse@gmail.com", valeur.Email);
            Assert.IsType<UserDto>(valeur);
            Assert.Contains(valeur, userList);
        }
    }

    [Fact]
    public void GetUserIdFail_Id_Doesnt_Exist()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUserById(1))
            .Returns(userList[1]);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUserById(100);
        if (userResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<UserDto>(valeur);
            Assert.DoesNotContain(valeur, userList);
        }
    }


    [Fact]
    public void GetUserIdFail_Negative_Id()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUserById(1))
            .Returns(userList[1]);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUserById(-1);
        if (userResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<UserDto>(valeur);
            Assert.DoesNotContain(valeur, userList);
        }
    }

    [Fact]
    public void GetUserIdFail_Argument_Exception()
    {
        _userService.Setup(x => x.GetUserById(10000))
            .Throws<ArgumentException>();
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUserById(10000);
        if (userResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
        }
    }

    [Fact]
    public void GetUserUsernameSuccess()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUserByUsername("Useruser"))
            .Returns(userList[0]);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUserByUsername("Useruser");
        if (userResult is OkObjectResult okObjectResult)
        {
            UserDto valeur = okObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.IsType<UserDto>(valeur);
            Assert.Contains(valeur, userList);
            Assert.Equal("adressemail@gmail.com", valeur.Email);
            Assert.Equal("Useruser", valeur.Username);
            Assert.Equal("motdepasse", valeur.Password);
            Assert.True(valeur.IsAdmin);
        }
    }

    [Fact]
    public void GetUserUsernameFail_Argument_Exception()
    {
        _userService.Setup(x => x.GetUserByUsername("Usererererrere"))
            .Throws<ArgumentException>();
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUserByUsername("Usererererrere");
        if (userResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
        }
    }

    [Fact]
    public void GetUserEmailSuccess()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUserByEmail("adressemail@gmail.com"))
            .Returns(userList[0]);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetByEmail("adressemail@gmail.com");
        if (userResult is OkObjectResult okObjectResult)
        {
            UserDto valeur = okObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.IsType<UserDto>(valeur);
            Assert.Contains(valeur, userList);
            Assert.Equal("adressemail@gmail.com", valeur.Email);
            Assert.Equal("Useruser", valeur.Username);
            Assert.Equal("motdepasse", valeur.Password);
            Assert.True(valeur.IsAdmin);
        }
    }

    [Fact]
    public void GetUserUsernameFail()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUserByUsername("Useruser"))
            .Returns(userList[0]);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetUserByUsername("GYIIieihhh");

        if (userResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<UserDto>(valeur);
            Assert.DoesNotContain(valeur, userList);
            Assert.False(userList == valeur);
        }
    }

    [Fact]
    public void GetUserEmailFail()
    {
        var userList = GetUsersData();
        _userService.Setup(x => x.GetUserByEmail("adressemail@gmail.com"))
            .Returns(userList[0]);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.GetByEmail("GYIIieihhh");

        if (userResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<UserDto>(valeur);
            Assert.DoesNotContain(valeur, userList);
            Assert.False(userList == valeur);
        }
    }

    [Fact]
    public void DeleteUserSuccess()
    {
        _userService.Setup(x => x.DeleteUser(1))
            .Returns(true);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.DeleteUser(1);
        if (userResult is OkObjectResult okObjectResult)
        {
            bool valeur = (bool)okObjectResult.Value;

            Assert.True(valeur);
        }
    }

    [Fact]
    public void DeleteUserFail()
    {
        _userService.Setup(x => x.DeleteUser(1))
            .Returns(true);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.DeleteUser(100);
        if (userResult is NotFoundObjectResult nfObjectResult)
        {
            Assert.Null(nfObjectResult.Value);
            Assert.IsNotType<bool>(nfObjectResult.Value);
        }
    }

    [Fact]
    public void DeleteUser_By_Username_Success()
    {
        _userService.Setup(x => x.DeleteUserByUsername("Damn"))
            .Returns(true);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.DeleteUserByUsername("Damn");
        if (userResult is OkObjectResult okObjectResult)
        {
            Assert.Null(okObjectResult.Value);
        }
    }

    [Fact]
    public void DeleteUser_By_Username_Failed()
    {
        _userService.Setup(x => x.DeleteUserByUsername("IUDHEIUHDEHUDH"))
            .Returns(false);
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.DeleteUserByUsername("IUDHEIUHDEHUDH");
        if (userResult is NotFoundObjectResult notFoundObjectResult)
        {
            Assert.Null(notFoundObjectResult.Value);
        }
    }

    [Fact]
    public void CreateUserSuccess()
    {
        _userService.Setup(x => x.CreateUser("Nom", "Passssss", "heudfk@hdye.com", true))
            .Returns(new UserDto("Nom", "Passssss", "heudfk@hdye.com", true));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.CreateUser(new UserDto("Nom", "Passssss", "heudfk@hdye.com", true));
        if (userResult is CreatedResult createdObjectResult)
        {
            UserDto valeur = createdObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.Equal("Nom", valeur.Username);
            Assert.Equal("Passssss", valeur.Password);
            Assert.Equal("heudfk@hdye.com", valeur.Email);
            Assert.True(valeur.IsAdmin);
        }
    }

    [Fact]
    public void CreateUserFail_When_Field_Null()
    {
        GetUsersData();
        _userService.Setup(x => x.CreateUser("Nom", "Passssss", "heudfk@hdye.com", true))
            .Returns(new UserDto("Nom", "Passssss", "heudfk@hdye.com", true));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.CreateUser(new UserDto(null, "Passssss", "heudfk@hdye.com", true));

        if (userResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }


    [Fact]
    public void CreateUserWithExistingEmail()
    {
        GetUsersData();
        _userService.Setup(x => x.CreateUser("Nom", "Passssss", "adressemail@gmail.com", true))
            .Returns(new UserDto("Nom", "Passssss", "adressemail@gmail.com", true));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.CreateUser(new UserDto("user", "Passssss", "adressemail@gmail.com", true));

        if (userResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(409, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void CreateUserWithExistingUsername()
    {
        GetUsersData();
        _userService.Setup(x => x.CreateUser("Nom", "Passssss", "adressemail@gmail.com", true))
            .Returns(new UserDto("Nom", "Passssss", "adressemail@gmail.com", true));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.CreateUser(new UserDto("Useruser", "Passssss", "adressemail@gmail.com", true));

        if (userResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void UpdateUserSuccess()
    {
        _userService.Setup(x => x.UpdateUser(1, new UserDto("Nom", "Passssss", "heudfk@hdye.com", true)))
            .Returns(new UserDto("Nom", "Passssss", "heudfk@hdye.com", true));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.UpdateUser(1, new UserDto("Nom", "Passssss", "heudfk@hdye.com", true));
        if (userResult is OkObjectResult okObjectResult)
        {
            UserDto valeur = okObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.Equal("Nom", valeur.Username);
            Assert.Equal("Passssss", valeur.Password);
            Assert.Equal("heudfk@hdye.com", valeur.Email);
            Assert.True(valeur.IsAdmin);
        }
    }

    [Fact]
    public void UpdateUserFail()
    {
        _userService.Setup(x => x.UpdateUser(1, new UserDto(1, "Nom", "Passssss", "heudfk@hdye.com", true)))
            .Returns(new UserDto("Nom", "Passssss", "heudfk@hdye.com", true));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.UpdateUser(1, new UserDto(2, "Nom", "Passssss", "heudfk@hdye.com", true));

        if (userResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }
    [Fact]
    public void UpdateUserFailWithExistingUsername()
    {
        _userService.Setup(x => x.IsUsernameTaken("Useruser")).Returns(true);
        _userService.Setup(x => x.IsEmailTaken("heudfk@hdye.com")).Returns(false);
        _userService.Setup(x => x.GetUserById(1)).Returns(new UserDto(1, "Leuser", "motdepasse", "deuxadresse@gmail.com", true));
        _userService.Setup(x => x.UpdateUser(1, new UserDto(1, "Useruser", "Passssss", "heudfk@hdye.com", true)))
            .Returns(new UserDto("Useruser", "Passssss", "heudfk@hdye.com", true));

        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.UpdateUser(1, new UserDto(1,"Useruser", "Passssss", "heudfk@hdye.com", true));

        if (userResult is StatusCodeResult statusCodeResult)
        {
            Assert.Equal(409, statusCodeResult.StatusCode);
        }
    }
    [Fact]
    public void UpdateUserFailWithExistingEmail()
    {
        _userService.Setup(x => x.IsUsernameTaken("Test1234")).Returns(false);
        _userService.Setup(x => x.IsEmailTaken("adressemail@gmail.com")).Returns(true);
        _userService.Setup(x => x.GetUserById(1)).Returns(new UserDto(1, "Leuser", "motdepasse", "deuxadresse@gmail.com", true));
        _userService.Setup(x => x.UpdateUser(1, new UserDto(1, "Test1234", "Passssss", "adressemail@gmail.com", true)))
            .Returns(new UserDto("Test1234", "Passssss", "adressemail@gmail.com", true));

        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.UpdateUser(1, new UserDto(1,"Test1234", "Passssss", "adressemail@gmail.com", true));

        if (userResult is StatusCodeResult statusCodeResult)
        {
            Assert.Equal(409, statusCodeResult.StatusCode);
        }
    }
    [Fact]
    public void UpdateUserFailWithExistingEmailAndExistingUsername()
    {
        _userService.Setup(x => x.IsUsernameTaken("Useruser")).Returns(true);
        _userService.Setup(x => x.IsEmailTaken("adressemail@gmail.com")).Returns(true);
        _userService.Setup(x => x.GetUserById(1)).Returns(new UserDto(1, "Leuser", "motdepasse", "deuxadresse@gmail.com", true));
        _userService.Setup(x => x.UpdateUser(1, new UserDto(1, "Useruser", "Passssss", "adressemail@gmail.com", true)))
            .Returns(new UserDto("Useruser", "Passssss", "adressemail@gmail.com", true));

        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.UpdateUser(1, new UserDto(1,"Useruser", "Passssss", "adressemail@gmail.com", true));

        if (userResult is StatusCodeResult statusCodeResult)
        {
            Assert.Equal(409, statusCodeResult.StatusCode);
        }
    }
    [Fact]
    public void UpdateUserSucessWithSameEmail()
    {
        _userService.Setup(x => x.IsUsernameTaken("Test1234")).Returns(false);
        _userService.Setup(x => x.IsEmailTaken("deuxadresse@gmail.com")).Returns(true);
        _userService.Setup(x => x.GetUserById(1)).Returns(new UserDto(1, "Leuser", "motdepasse", "deuxadresse@gmail.com", true));
        _userService.Setup(x => x.UpdateUser(1, new UserDto(1, "Test1234", "Passssss", "deuxadresse@gmail.com", true)))
            .Returns(new UserDto("Test1234", "Passssss", "deuxadresse@gmail.com", true));

        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.UpdateUser(1, new UserDto(1,"Test1234", "Passssss", "deuxadresse@gmail.com", true));
        if (userResult is OkObjectResult okObjectResult)
        {
            UserDto valeur = okObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.Equal("Test1234", valeur.Username);
            Assert.Equal("Passssss", valeur.Password);
            Assert.Equal("deuxadresse@gmail.com", valeur.Email);
            Assert.True(valeur.IsAdmin);
        }
    }
    [Fact]
    public void UpdateUserSucessWithSameUsername()
    {
        _userService.Setup(x => x.IsUsernameTaken("Leuser")).Returns(true);
        _userService.Setup(x => x.IsEmailTaken("heudfk@hdye.com")).Returns(false);
        _userService.Setup(x => x.GetUserById(1)).Returns(new UserDto(1, "Leuser", "motdepasse", "deuxadresse@gmail.com", true));
        _userService.Setup(x => x.UpdateUser(1, new UserDto(1, "Test1234", "Passssss", "heudfk@hdye.com", true)))
            .Returns(new UserDto("Leuser", "Passssss", "heudfk@hdye.com", true));

        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.UpdateUser(1, new UserDto(1,"Leuser", "Passssss", "heudfk@hdye.com", true));
        if (userResult is OkObjectResult okObjectResult)
        {
            UserDto valeur = okObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.Equal("Leuser", valeur.Username);
            Assert.Equal("Passssss", valeur.Password);
            Assert.Equal("heudfk@hdye.com", valeur.Email);
            Assert.True(valeur.IsAdmin);
        }
    }


    [Fact]
    public void PromoteUserSuccess()
    {
        _userService.Setup(x => x.PromoteUser(1))
            .Returns(new UserDto(
                1,
                "Leuser",
                "motdepasse",
                "deuxadresse@gmail.com",
                true
            ));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.PromoteUser(1);
        if (userResult is OkObjectResult okObjectResult)
        {
            UserDto valeur = okObjectResult.Value as UserDto;

            Assert.NotNull(valeur);
            Assert.Equal("Leuser", valeur.Username);
            Assert.Equal("motdepasse", valeur.Password);
            Assert.Equal("deuxadresse@gmail.com", valeur.Email);
            Assert.True(valeur.IsAdmin);
        }
    }

    [Fact]
    public void PromoteUserFail()
    {
        _userService.Setup(x => x.PromoteUser(1))
            .Returns(new UserDto(
                1,
                "Leuser",
                "motdepasse",
                "deuxadresse@gmail.com",
                true
            ));
        var usersController = new UsersController(new NullLogger<UsersController>(), _userService.Object);

        var userResult = usersController.PromoteUser(356262);

        if (userResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(404, bdObjectResult.StatusCode);
        }
    }

    private List<UserDto> GetUsersData()
    {
        List<UserDto> usersData = new List<UserDto>(4)
        {
            new(
                0,
                "Useruser",
                "motdepasse",
                "adressemail@gmail.com",
                true
            ),
            new
            (
                1,
                "Leuser",
                "motdepasse",
                "deuxadresse@gmail.com",
                false
            ),
            new
            (
                2,
                "gygyggyg",
                "ennodlavehc",
                "thirdadress@gmail.com",
                false
            ),
            new
            (
                "ferferf",
                "h_nh_78",
                "fourthadress@gmail.com",
                false
            ),
        };
        return usersData;
    }
}