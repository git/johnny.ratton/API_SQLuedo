﻿using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;
using TestAPI.Extensions;

namespace TestAPI;

public class SuccessesUnitTest
{
    private readonly Mock<ISuccessService<SuccessDto>> _successService;

    public SuccessesUnitTest()
    {
        _successService = new Mock<ISuccessService<SuccessDto>>();
    }

    [Fact]
    public void GetSuccessesListSuccess()
    {
        var successesList = GetSuccessesData();
        _successService.Setup(x => x.GetSuccesses(1, 4, 0))
            .Returns(successesList);
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.GetSuccesses(1, 4, 0);

        if (successesResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetSuccessesData().ToString(), valeur.ToString());
            Assert.True(successesList.SequenceEqual(valeur as IEnumerable<SuccessDto>,
                new SuccessIdEqualityComparer()));
        }
    }

    [Fact]
    public void GetSuccessesListFail_When_Result_Equal_0()
    {
        _successService.Setup(x => x.GetSuccesses(1, 4, 0))
            .Returns(new List<SuccessDto>());
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.GetSuccesses(26373, 31771, 0);

        if (successesResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)
        {
            Assert.IsNotType<OkObjectResult>(successesResult);
        }
    }

    [Fact]
    public void GetSuccessesListFail_When_Page_Is_A_Negative()
    {
        _successService.Setup(x => x.GetSuccesses(1, 4, 0))
            .Returns(new List<SuccessDto>());
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.GetSuccesses(-1, 3, 0);

        if (successesResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)
        {
            Assert.IsNotType<OkObjectResult>(successesResult);
        }
    }

    [Fact]
    public void GetSuccessInquiryIdSuccess()
    {
        var successesList = GetSuccessesData();
        _successService.Setup(x => x.GetSuccessesByInquiryId(1))
            .Returns(new List<SuccessDto> { successesList[0], successesList[1] });
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByInquiryId(1);
        if (sucessesResult is OkObjectResult okObjectResult)
        {
            List<SuccessDto> valeur = okObjectResult.Value as List<SuccessDto>;

            Assert.NotNull(valeur);
            Assert.Equal(0, valeur[0].UserId);
            Assert.Equal(1, valeur[0].InquiryId);
            Assert.True(valeur[0].IsFinished);
            Assert.Equal(1, valeur[1].UserId);
            Assert.Equal(1, valeur[1].InquiryId);
            Assert.True(valeur[1].IsFinished);


            Assert.Equal(valeur[1].GetHashCode(), successesList[1].GetHashCode());
            Assert.True(valeur[1].Equals(successesList[1]));
            Assert.False(valeur.Equals(new object()));
            Assert.False(valeur.Equals(null));
            Assert.True(valeur.Equals(valeur));
            Assert.IsType<SuccessDto>(valeur[0]);
            Assert.Contains(valeur[1], successesList);
        }
    }

    [Fact]
    public void GetSuccessInquiryIdFail_When_Id_Not_Exist()
    {
        var successesList = GetSuccessesData();
        _successService.Setup(x => x.GetSuccessesByInquiryId(1))
            .Returns(new List<SuccessDto> { successesList[0], successesList[1] });
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByInquiryId(100);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<SuccessDto>(valeur);
            Assert.DoesNotContain(valeur, successesList);
        }
    }

    [Fact]
    public void GetSuccessInquiryIdFail_When_Id_Negative()
    {
        var successesList = GetSuccessesData();
        _successService.Setup(x => x.GetSuccessesByInquiryId(1))
            .Returns(new List<SuccessDto> { successesList[0], successesList[1] });
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByInquiryId(-1);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<SuccessDto>(valeur);
            Assert.DoesNotContain(valeur, successesList);
        }
    }

    [Fact]
    public void GetSuccessInquiryIdFail_Argument_Exception()
    {
        _successService.Setup(x => x.GetSuccessesByInquiryId(1000))
            .Throws<ArgumentException>();
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByInquiryId(1000);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
        }
    }

    [Fact]
    public void GetSuccessUserIdSuccess()
    {
        var successesList = GetSuccessesData();
        _successService.Setup(x => x.GetSuccessesByUserId(2))
            .Returns(new List<SuccessDto> { successesList[2], successesList[3] });
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByUserId(2);
        if (sucessesResult is OkObjectResult okObjectResult)
        {
            List<SuccessDto> valeur = okObjectResult.Value as List<SuccessDto>;

            Assert.NotNull(valeur);
            Assert.Equal(2, valeur[0].UserId);
            Assert.Equal(3, valeur[0].InquiryId);
            Assert.True(valeur[0].IsFinished);
            Assert.Equal(2, valeur[1].UserId);
            Assert.Equal(4, valeur[1].InquiryId);
            Assert.True(valeur[1].IsFinished);


            Assert.Equal(valeur[1].GetHashCode(), successesList[3].GetHashCode());
            Assert.True(valeur[1].Equals(successesList[3]));
            Assert.False(valeur.Equals(new object()));
            Assert.IsType<SuccessDto>(valeur[0]);
            Assert.Contains(valeur[1], successesList);
        }
    }

    [Fact]
    public void GetSuccessUserIdFail_When_Id_Not_Found()
    {
        var successesList = GetSuccessesData();
        _successService.Setup(x => x.GetSuccessesByUserId(2))
            .Returns(new List<SuccessDto> { successesList[2], successesList[3] });
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByUserId(200);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<SuccessDto>(valeur);
            Assert.DoesNotContain(valeur, successesList);
        }
    }


    [Fact]
    public void GetSuccessUserIdFail_When_Id_Negative()
    {
        var successesList = GetSuccessesData();
        _successService.Setup(x => x.GetSuccessesByUserId(2))
            .Returns(new List<SuccessDto> { successesList[2], successesList[3] });
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByUserId(-1);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<SuccessDto>(valeur);
            Assert.DoesNotContain(valeur, successesList);
        }
    }

    [Fact]
    public void GetSuccessUserIdFail_Argument_Exception()
    {
        _successService.Setup(x => x.GetSuccessesByUserId(1000))
            .Throws<ArgumentException>();
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.GetSuccessByUserId(1000);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
        }
    }

    [Fact]
    public void DeleteSuccessSuccess()
    {
        _successService.Setup(x => x.DeleteSuccess(1, 1))
            .Returns(true);
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.DeleteSuccess(1, 1);
        if (sucessesResult is OkObjectResult okObjectResult)
        {
            bool valeur = (bool)okObjectResult.Value;

            Assert.True(valeur);
        }
    }

    [Fact]
    public void DeleteSuccessFail_When_Not_Found()
    {
        _successService.Setup(x => x.DeleteSuccess(1, 1))
            .Returns(true);
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.DeleteSuccess(100, 278);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            Assert.Null(nfObjectResult.Value);
            Assert.IsNotType<bool>(nfObjectResult.Value);
        }
    }

    [Fact]
    public void DeleteSuccessFail_When_Negative()
    {
        _successService.Setup(x => x.DeleteSuccess(1, 1))
            .Returns(true);
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var sucessesResult = successesController.DeleteSuccess(-1, 278);
        if (sucessesResult is NotFoundObjectResult nfObjectResult)
        {
            Assert.Null(nfObjectResult.Value);
            Assert.IsNotType<bool>(nfObjectResult.Value);
        }
    }

    [Fact]
    public void CreateSuccessSuccess()
    {
        _successService.Setup(x => x.CreateSuccess(8, 8, true))
            .Returns(new SuccessDto(8, 8, true));
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.CreateSuccess(new SuccessDto(8, 8, true));
        if (successesResult is CreatedResult createdObjectResult)
        {
            SuccessDto valeur = createdObjectResult.Value as SuccessDto;

            Assert.NotNull(valeur);
            Assert.Equal(8, valeur.UserId);
            Assert.Equal(8, valeur.InquiryId);
            Assert.True(valeur.IsFinished);
        }
    }

    [Fact]
    public void CreateSuccessFail_When_Id_Not_Found()
    {
        _successService.Setup(x => x.CreateSuccess(8, 8, true))
            .Returns(new SuccessDto(8, 8, true));
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.CreateSuccess(new SuccessDto(882, 818, true));

        if (successesResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void CreateSuccessFail_When_Id_Negative()
    {
        _successService.Setup(x => x.CreateSuccess(8, 8, true))
            .Returns(new SuccessDto(8, 8, true));
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.CreateSuccess(new SuccessDto(-1, 818, true));

        if (successesResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void CreateSuccessFail_Exception()
    {
        _successService.Setup(x => x.CreateSuccess(89889, 82837, true))
            .Throws<Exception>();
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.CreateSuccess(new SuccessDto(89889, 82837, true));

        if (successesResult is NotFoundObjectResult notFoundObjectResult)
        {
            Assert.NotNull(notFoundObjectResult);
        }
    }

    [Fact]
    public void UpdateSuccessSuccess()
    {
        _successService.Setup(x => x.UpdateSuccess(1, 1, new SuccessDto(1, 1, true)))
            .Returns(new SuccessDto(1, 1, true));
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.UpdateSuccess(1, 1, new SuccessDto(1, 1, true));
        if (successesResult is OkObjectResult okObjectResult)
        {
            SuccessDto valeur = okObjectResult.Value as SuccessDto;

            Assert.NotNull(valeur);
            Assert.Equal(1, valeur.UserId);
            Assert.Equal(1, valeur.InquiryId);
            Assert.True(valeur.IsFinished);
        }
    }

    [Fact]
    public void UpdateSuccessFail_When_Ids_Are_Differents()
    {
        _successService.Setup(x => x.UpdateSuccess(1, 1, new SuccessDto(1, 2, true)))
            .Returns(new SuccessDto(1, 2, true));
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.UpdateSuccess(1, 1, new SuccessDto(1, 2, true));

        if (successesResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void UpdateSuccessFail_When_Id_Negative()
    {
        _successService.Setup(x => x.UpdateSuccess(1, 1, new SuccessDto(1, 2, true)))
            .Returns(new SuccessDto(1, 2, true));
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.UpdateSuccess(-2, 1, new SuccessDto(1, 2, true));

        if (successesResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void UpdateSuccessFail_When_Id_Not_Found()
    {
        _successService.Setup(x => x.UpdateSuccess(1, 1, new SuccessDto(1, 2, true)))
            .Returns(new SuccessDto(1, 2, true));
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.UpdateSuccess(1000, 1000, new SuccessDto(1000, 1000, true));

        if (successesResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void UpdateSuccessFail_Throw_Exception()
    {
        _successService.Setup(x => x.UpdateSuccess(108871, 117683, new SuccessDto(1, 2, true)))
            .Throws<Exception>();
        var successesController =
            new SuccessesController(_successService.Object, new NullLogger<SuccessesController>());

        var successesResult = successesController.UpdateSuccess(108871, 117683, new SuccessDto(1, 2, true));

        if (successesResult is NotFoundObjectResult notFoundObjectResult)
        {
            Assert.NotNull(notFoundObjectResult);
        }
    }



    private List<SuccessDto> GetSuccessesData()
    {
        List<SuccessDto> successesData = new List<SuccessDto>(4)
        {
            new(0, 1, true),
            new(1, 1, true),
            new(2, 3, true),
            new(2, 4, true),
        };
        return successesData;
    }
}