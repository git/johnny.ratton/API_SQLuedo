﻿using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;
using API.Controllers;
using TestAPI.Extensions;

namespace TestAPI;

public class InquiryUnitTest
{
    private readonly Mock<IInquiryService<InquiryDto>> _inquiryService;

    public InquiryUnitTest()
    {
        _inquiryService = new Mock<IInquiryService<InquiryDto>>();
    }

    [Fact]
    public void GetInquiriesListSuccess()
    {
        var inquiryList = GetInquiriesData();
        _inquiryService.Setup(x => x.GetInquiries(1, 4, 0))
            .Returns(inquiryList);
        var inquiryController = new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult = inquiryController.GetInquiries(1, 4, 0);

        if (inquiriesResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetInquiriesData().ToString(), valeur.ToString());
            Assert.True(inquiryList.SequenceEqual(valeur as IEnumerable<InquiryDto>, new InquiryIdEqualityComparer()));
        }
    }
    
    [Fact]
    public void GetNumberOfInquiries()
    {
        var inquiryList = GetInquiriesData();
        _inquiryService.Setup(x => x.GetNumberOfInquiries())
            .Returns(4);
        var inquiryController = new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult = inquiryController.GetNumberOfInquiries();

        if (inquiriesResult is OkObjectResult okObjectResult)
        {
            var valeur = (KeyValuePair<string,int>)okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetInquiriesData().Count, valeur.Value);
        }
    }

    [Fact]
    public void GetInquiresListFail()
    {
        _inquiryService.Setup(x => x.GetInquiries(1, 4, 0))
            .Returns(new List<InquiryDto>());
        var inquiryController = new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult = inquiryController.GetInquiries(2, 3, 0);

        if (inquiriesResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)
        {
            Assert.IsNotType<OkObjectResult>(inquiriesResult);
        }
    }

    [Fact]
    public void GetInquiryIdSuccess()
    {
        var inquiryList = GetInquiriesData();
        _inquiryService.Setup(x => x.GetInquiryById(1))
            .Returns(inquiryList[1]);
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiryResult = inquiriesController.GetInquiryById(1);
        if (inquiryResult is OkObjectResult okObjectResult)
        {
            InquiryDto valeur = okObjectResult.Value as InquiryDto;

            Assert.NotNull(valeur);
            Assert.Equal("titre 2", valeur.Title);
            Assert.Equal("Description", valeur.Description);
            Assert.True(valeur.IsUser);
            Assert.Equal(valeur.GetHashCode(), inquiryList[1].GetHashCode());
            Assert.True(valeur.Equals(inquiryList[1]));
            Assert.False(valeur.Equals(new object()));
            Assert.IsType<InquiryDto>(valeur);
            Assert.Contains(valeur, inquiryList);
        }
    }

    [Fact]
    public void GetInquiryIdFail()
    {
        var inquiryList = GetInquiriesData();
        _inquiryService.Setup(x => x.GetInquiryById(1))
            .Returns(inquiryList[1]);
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiryResult = inquiriesController.GetInquiryById(100);
        if (inquiryResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<InquiryDto>(valeur);
            Assert.DoesNotContain(valeur, inquiryList);
        }
    }

    [Fact]
    public void GetInquiryTitleSuccess()
    {
        var inquiryList = GetInquiriesData();
        _inquiryService.Setup(x => x.GetInquiryByTitle("ZUDZU"))
            .Returns(inquiryList[2]);
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult = inquiriesController.GetInquiryByTitle("ZUDZU");
        if (inquiriesResult is OkObjectResult okObjectResult)
        {
            InquiryDto valeur = okObjectResult.Value as InquiryDto;

            Assert.NotNull(valeur);
            Assert.IsType<InquiryDto>(valeur);
            Assert.Contains(valeur, inquiryList);
            Assert.Equal("ZUDZU", valeur.Title);
            Assert.Equal("OUHHHH", valeur.Description);
            Assert.Equal(2, valeur.Id);
            Assert.False(valeur.IsUser);
        }
    }

    [Fact]
    public void GetInquiryTitleFail()
    {
        var inquiryList = GetInquiriesData();
        _inquiryService.Setup(x => x.GetInquiryByTitle("ZUDZU"))
            .Returns(inquiryList[2]);
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult = inquiriesController.GetInquiryByTitle("GYIIieihhh");

        if (inquiriesResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<InquiryDto>(valeur);
            Assert.DoesNotContain(valeur, inquiryList);
            Assert.False(inquiryList == valeur);
        }
    }

    [Fact]
    public void DeleteInquirySuccess()
    {
        _inquiryService.Setup(x => x.DeleteInquiry(1))
            .Returns(true);
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiryResult = inquiriesController.DeleteInquiry(1);
        if (inquiryResult is OkObjectResult okObjectResult)
        {
            bool valeur = (bool)okObjectResult.Value;

            Assert.True(valeur);
        }
    }

    [Fact]
    public void DeleteInquiryFail()
    {
        _inquiryService.Setup(x => x.DeleteInquiry(1))
            .Returns(true);
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiryResult = inquiriesController.DeleteInquiry(100);
        if (inquiryResult is NotFoundObjectResult nfObjectResult)
        {
            Assert.Null(nfObjectResult.Value);
            Assert.IsNotType<bool>(nfObjectResult.Value);
        }
    }

    [Fact]
    public void CreateInquirySuccess()
    {
        _inquiryService.Setup(x => x.CreateInquiry("Titros", "description", false))
            .Returns(new InquiryDto(4, "Titros", "description", false));
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult = inquiriesController.CreateInquiry(new InquiryDto("Titros", "description", false));
        if (inquiriesResult is CreatedResult createdObjectResult)
        {
            InquiryDto valeur = createdObjectResult.Value as InquiryDto;

            Assert.NotNull(valeur);
            Assert.Equal(4, valeur.Id);
            Assert.Equal("Titros", valeur.Title);
            Assert.Equal("description", valeur.Description);
            Assert.False(valeur.IsUser);
        }
    }

    [Fact]
    public void CreateInquiryFail()
    {
        _inquiryService.Setup(x => x.CreateInquiry("Titros", "description", false))
            .Returns(new InquiryDto(4, "Titros", "description", false));
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult = inquiriesController.CreateInquiry(new InquiryDto(4, null, "heudfk@hdye.com", true));

        if (inquiriesResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void UpdateInquirySuccess()
    {
        _inquiryService.Setup(x => x.UpdateInquiry(1, new InquiryDto(1, "Passssss", "heudfk@hdye.com", true)))
            .Returns(new InquiryDto(1, "Passssss", "heudfk@hdye.com", true));
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult =
            inquiriesController.UpdateInquiry(1, new InquiryDto(1, "Passssss", "heudfk@hdye.com", true));
        if (inquiriesResult is OkObjectResult okObjectResult)
        {
            InquiryDto valeur = okObjectResult.Value as InquiryDto;

            Assert.NotNull(valeur);
            Assert.Equal("Passssss", valeur.Title);
            Assert.Equal("heudfk@hdye.com", valeur.Description);
            Assert.True(valeur.IsUser);
        }
    }

    [Fact]
    public void UpdateInquiryFail()
    {
        _inquiryService.Setup(x => x.UpdateInquiry(1, new InquiryDto(1, "Passssss", "heudfk@hdye.com", true)))
            .Returns(new InquiryDto(1, "Passssss", "heudfk@hdye.com", true));
        var inquiriesController =
            new InquiriesController(_inquiryService.Object, new NullLogger<InquiriesController>());

        var inquiriesResult =
            inquiriesController.UpdateInquiry(1, new InquiryDto(2, "Passssss", "heudfk@hdye.com", true));

        if (inquiriesResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }


    private List<InquiryDto> GetInquiriesData()
    {
        List<InquiryDto> inquiriesData = new List<InquiryDto>(4)
        {
            new(0, "titre 1", "La desc", false),
            new(1, "titre 2", "Description", true),
            new(2, "ZUDZU", "OUHHHH", false),
            new("titre premium", "Ascendant", true),
        };
        return inquiriesData;
    }
}