using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;

namespace TestAPI;

public class SolutionUnitTest
{
    private readonly Mock<ISolutionService<SolutionDto>> _solutionService;

    public SolutionUnitTest()
    {
        _solutionService = new Mock<ISolutionService<SolutionDto>>();
    }
    
    [Fact]
    public void GetSolutionFromInquiryId_Success()
    {
        var solution = new SolutionDto(42,"Maxime","Sapountzis","La cuisine","Le couteau", "L'explication");
        _solutionService.Setup(x => x.GetSolutionByInquiryId(42))
            .Returns(solution);
        var solutionController =
            new SolutionController(_solutionService.Object, new NullLogger<SolutionController>());

        var solutionResult = solutionController.GetSolutionByInquiryById(42);

        if (solutionResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;
            Assert.NotNull(valeur);
            Assert.Equal(solution, valeur);
        }
    }
    
    [Fact]
    public void GetSolutionFromInquiryId_Throws_ArgumentException()
    {
        var solution = new SolutionDto(42,"Maxime","Sapountzis","La cuisine","Le couteau", "L'explication");
        _solutionService.Setup(x => x.GetSolutionByInquiryId(42))
            .Throws<ArgumentException>();
        var solutionController =
            new SolutionController(_solutionService.Object, new NullLogger<SolutionController>());

        var solutionResult = solutionController.GetSolutionByInquiryById(42);

        Assert.NotNull(solutionResult);
        Assert.Equal(typeof(NotFoundResult), solutionResult.GetType());
    }
}