﻿using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;
using TestAPI.Extensions;

namespace TestAPI;

public class ParagraphsUnitTest

{
    private readonly Mock<IParagraphService<ParagraphDto>> _paragraphService;

    public ParagraphsUnitTest()
    {
        _paragraphService = new Mock<IParagraphService<ParagraphDto>>();
    }

    [Fact]
    public void GetParagraphsListSuccess()
    {
        var paragraphsList = GetParagraphsData();
        _paragraphService.Setup(x => x.GetParagraphs(1, 4, 0))
            .Returns(paragraphsList);
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.GetParagraphs(1, 4, 0);

        if (paragraphsResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetParagraphsData().ToString(), valeur.ToString());
            Assert.True(paragraphsList.SequenceEqual(valeur as IEnumerable<ParagraphDto>,
                new ParagraphIdEqualityComparer()));
        }
    }

    [Fact]
    public void GetParagraphsListFail()
    {
        _paragraphService.Setup(x => x.GetParagraphs(1, 4, 0))
            .Returns(new List<ParagraphDto>());
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.GetParagraphs(1, 4, 0);

        if (paragraphsResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)

        {
            Assert.IsNotType<OkObjectResult>(paragraphsResult);
        }
    }

    [Fact]
    public void GetParagraphIdSuccess()
    {
        var paragraphsList = GetParagraphsData();
        _paragraphService.Setup(x => x.GetParagraphById(1))
            .Returns(paragraphsList[1]);
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.GetParagraphById(1);
        if (paragraphsResult is OkObjectResult okObjectResult)
        {
            ParagraphDto valeur = okObjectResult.Value as ParagraphDto;

            Assert.NotNull(valeur);
            Assert.Equal("Le titre", valeur.Title);
            Assert.Equal("pas contenu", valeur.Content);
            Assert.Equal("Pas d'infos ici", valeur.Info);
            Assert.Equal("Delete * from Earth", valeur.Query);
            Assert.Equal("Miam", valeur.Comment);
            Assert.Equal(2, valeur.LessonId);


            Assert.Equal(valeur.GetHashCode(), paragraphsList[1].GetHashCode());
            Assert.True(valeur.Equals(paragraphsList[1]));
            Assert.False(valeur.Equals(new object()));
            Assert.True(valeur.Equals(valeur));
            Assert.IsType<ParagraphDto>(valeur);
            Assert.Contains(valeur, paragraphsList);
        }
    }

    [Fact]
    public void GetParagraphIdFail()
    {
        var paragraphsList = GetParagraphsData();
        _paragraphService.Setup(x => x.GetParagraphById(1))
            .Returns(paragraphsList[1]);
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.GetParagraphById(100);
        if (paragraphsResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<ParagraphDto>(valeur);
            Assert.DoesNotContain(valeur, paragraphsList);
        }
    }

    [Fact]
    public void GetParagraphTitleSuccess()
    {
        var paragraphsList = GetParagraphsData();
        _paragraphService.Setup(x => x.GetParagraphByTitle("Title"))
            .Returns(paragraphsList[3]);
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.GetParagraphByTitle("Title");
        if (paragraphsResult is OkObjectResult okObjectResult)
        {
            ParagraphDto valeur = okObjectResult.Value as ParagraphDto;

            Assert.NotNull(valeur);
            Assert.Equal("Title", valeur.Title);
            Assert.Equal("Content", valeur.Content);
            Assert.Equal("Je ne parle pas anglais", valeur.Info);
            Assert.Equal("Select select from select", valeur.Query);
            Assert.Equal("Mais qui est ce commentaire", valeur.Comment);
            Assert.Equal(3, valeur.LessonId);
            Assert.IsType<ParagraphDto>(valeur);
            Assert.Contains(valeur, paragraphsList);
        }
    }

    [Fact]
    public void GetParagraphTitleFail()
    {
        var paragraphsList = GetParagraphsData();
        _paragraphService.Setup(x => x.GetParagraphByTitle("Title"))
            .Returns(paragraphsList[3]);
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.GetParagraphByTitle("IUHIUHU");

        if (paragraphsResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<ParagraphDto>(valeur);
            Assert.DoesNotContain(valeur, paragraphsList);
            Assert.False(paragraphsList == valeur);
        }
    }

    [Fact]
    public void DeleteParagraphSuccess()
    {
        _paragraphService.Setup(x => x.DeleteParagraph(1))
            .Returns(true);
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.DeleteParagraph(1);
        if (paragraphsResult is OkObjectResult okObjectResult)
        {
            bool valeur = (bool)okObjectResult.Value;

            Assert.True(valeur);
        }
    }

    [Fact]
    public void DeleteParagraphFail()
    {
        _paragraphService.Setup(x => x.DeleteParagraph(1))
            .Returns(true);
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.DeleteParagraph(100);
        if (paragraphsResult is NotFoundObjectResult nfObjectResult)
        {
            Assert.Null(nfObjectResult.Value);
            Assert.IsNotType<bool>(nfObjectResult.Value);
        }
    }

    [Fact]
    public void CreateParagraphSuccess()
    {
        _paragraphService.Setup(x => x.CreateParagraph("Le nouveau titre", "Le nouveau content", "Title","Le nouveau content","Les infos",
                "La requête requêtante", "Commentaires", 2))
            .Returns(new ParagraphDto("Le nouveau titre", "Le nouveau content","Title", "Le nouveau content", "Les infos", "La requête requêtante",
                "Commentaires", 2));
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.CreateParagraph(new ParagraphDto("Le nouveau titre", "Le nouveau content","Title",
            "Le nouveau content", "Les infos", "La requête requêtante", "Commentaires", 2));
        if (paragraphsResult is CreatedResult createdObjectResult)
        {
            ParagraphDto valeur = createdObjectResult.Value as ParagraphDto;
            Assert.NotNull(valeur);
            Assert.Equal("Title", valeur.Title);
            Assert.Equal("Le nouveau titre", valeur.ContentTitle);
            Assert.Equal("Le nouveau content", valeur.ContentContent);
            Assert.Equal("Le nouveau content", valeur.Content);
            Assert.Equal("Les infos", valeur.Info);
            Assert.Equal("La requête requêtante", valeur.Query);
            Assert.Equal("Commentaires", valeur.Comment);
            Assert.Equal(2, valeur.LessonId);
        }
    }

    [Fact]
    public void CreateParagraphFail()
    {
        _paragraphService.Setup(x => x.CreateParagraph("Le nouveau titre", "Le nouveau content", "Title","Les infos",
                "La requête requêtante", "Commentaires", "Comment", 2))
            .Returns(new ParagraphDto("Le nouveau titre", "Le nouveau content", "Les infos", "La requête requêtante",
                "Commentaires", 2));
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.CreateParagraph(new ParagraphDto(null, "Le nouveau content",
            "Les infos", "La requête requêtante", "Commentaires", 2));

        if (paragraphsResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void UpdateParagraphSuccess()
    {
        _paragraphService.Setup(x => x.UpdateParagraph(1,
                new ParagraphDto(1, "Le nouveau titre", "Le nouveau content", "Les infos", "La requête requêtante",
                    "Commentaires", 2)))
            .Returns(new ParagraphDto(1, "Le nouveau titre", "Le nouveau content", "Les infos", "La requête requêtante",
                "Commentaires", 2));
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.UpdateParagraph(1,
            new ParagraphDto(1, "Le nouveau titre", "Le nouveau content", "Les infos", "La requête requêtante",
                "Commentaires", 2));
        if (paragraphsResult is OkObjectResult okObjectResult)
        {
            ParagraphDto valeur = okObjectResult.Value as ParagraphDto;

            Assert.NotNull(valeur);
            Assert.Equal("Le nouveau titre", valeur.Title);
            Assert.Equal("Le nouveau content", valeur.Content);
            Assert.Equal("Les infos", valeur.Info);
            Assert.Equal("La requête requêtante", valeur.Query);
            Assert.Equal("Commentaires", valeur.Comment);
            Assert.Equal(2, valeur.LessonId);
        }
    }

    [Fact]
    public void UpdateParagraphFail()
    {
        _paragraphService.Setup(x => x.UpdateParagraph(1,
                new ParagraphDto(1, "Le nouveau titre", "Le nouveau content", "Les infos", "La requête requêtante",
                    "Commentaires", 2)))
            .Returns(new ParagraphDto(1, "Le nouveau titre", "Le nouveau content", "Les infos", "La requête requêtante",
                "Commentaires", 2));
        var paragraphsController =
            new ParagraphsController(_paragraphService.Object, new NullLogger<ParagraphsController>());

        var paragraphsResult = paragraphsController.UpdateParagraph(1,
            new ParagraphDto(2, "Le nouveau titre", "Le nouveau content", "Les infos", "La requête requêtante",
                "Commentaires", 2));

        if (paragraphsResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    private List<ParagraphDto> GetParagraphsData()
    {
        List<ParagraphDto> paragraphsData = new List<ParagraphDto>(4)
        {
            new(0, "Titre 1", "Le contenu", "Les infos de ce paragraphes sont ", "Select * from C#", "Le commentaire",
                1),
            new(1, "Le titre", "pas contenu", "Pas d'infos ici", "Delete * from Earth", "Miam", 2),
            new(2, "Ceci n'est pas un titre", "Certainement hmmm", "OUOOOOO ", "Autocommit = {true}", "First", 1),
            new("Title", "Content", "Je ne parle pas anglais", "Select select from select",
                "Mais qui est ce commentaire", 3),
        };
        return paragraphsData;
    }
}