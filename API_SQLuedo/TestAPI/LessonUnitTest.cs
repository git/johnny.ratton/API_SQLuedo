﻿using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;
using TestAPI.Extensions;
using Xunit.Sdk;

namespace TestAPI;

public class LessonUnitTest
{
    private readonly Mock<ILessonService<LessonDto>> _lessonService;

    public LessonUnitTest()
    {
        _lessonService = new Mock<ILessonService<LessonDto>>();
    }

    [Fact]
    public void GetLessonsListSuccess()
    {
        var lessonList = GetLessonsData();
        _lessonService.Setup(x => x.GetLessons(1, 4, 0))
            .Returns(lessonList);
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.GetLessons(1, 4, 0);

        if (lessonsResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetLessonsData().ToString(), valeur.ToString());
            Assert.True(lessonList.SequenceEqual(valeur as IEnumerable<LessonDto>, new LessonIdEqualityComparer()));
        }
    }

    [Fact]
    public void GetLessonsListFail()
    {
        _lessonService.Setup(x => x.GetLessons(1, 4, 0))
            .Returns(new List<LessonDto>());
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.GetLessons(2, 3, 0);

        if (lessonsResult is StatusCodeResult statusCodeResult && statusCodeResult.StatusCode == 204)

        {
            Assert.IsNotType<OkObjectResult>(lessonsResult);
        }
    }

    [Fact]
    public void GetLessonIdSuccess()
    {
        var lessonList = GetLessonsData();
        _lessonService.Setup(x => x.GetLessonById(1))
            .Returns(lessonList[0]);
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.GetLessonById(1);
        if (lessonsResult is OkObjectResult okObjectResult)
        {
            LessonDto valeur = okObjectResult.Value as LessonDto;

            Assert.NotNull(valeur);
            Assert.Equal("Le titre", valeur.Title);
            Assert.Equal("Clément", valeur.LastPublisher);
            Assert.Equal(new DateOnly(2024, 03, 10), valeur.LastEdit);
            Assert.NotEqual(2, valeur.Id);


            Assert.Equal(valeur.GetHashCode(), lessonList[0].GetHashCode());
            Assert.True(valeur.Equals(lessonList[0]));
            Assert.False(valeur.Equals(new object()));
            Assert.True(valeur.Equals(valeur));
            Assert.IsType<LessonDto>(valeur);
            Assert.Contains(valeur, lessonList);
        }
    }

    [Fact]
    public void GetLessonIdFail()
    {
        var lessonList = GetLessonsData();
        _lessonService.Setup(x => x.GetLessonById(1))
            .Returns(lessonList[0]);
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.GetLessonById(100);
        if (lessonsResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<LessonDto>(valeur);
            Assert.DoesNotContain(valeur, lessonList);
        }
    }

    [Fact]
    public void GetLessonIdFail_Argument_Exception()
    {
        _lessonService.Setup(x => x.GetLessonById(10000))
            .Throws<ArgumentException>();
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var result = lessonsController.GetLessonById(10000);

        if (result is NotFoundObjectResult nfObjectResult)
        {
            Assert.NotNull(nfObjectResult);
        }
    }


    [Fact]
    public void GetLessonTitleSuccess()
    {
        var lessonList = GetLessonsData();
        _lessonService.Setup(x => x.GetLessonByTitle("Chiant la"))
            .Returns(lessonList[2]);
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.GetLessonByTitle("Chiant la");
        if (lessonsResult is OkObjectResult okObjectResult)
        {
            LessonDto valeur = okObjectResult.Value as LessonDto;

            Assert.NotNull(valeur);
            Assert.Equal("Chiant la", valeur.Title);
            Assert.Equal("Une personne", valeur.LastPublisher);
            Assert.Equal(new DateOnly(2012, 12, 25), valeur.LastEdit);
            Assert.Equal(3, valeur.Id);
            Assert.IsType<LessonDto>(valeur);
            Assert.Contains(valeur, lessonList);
        }
    }

    [Fact]
    public void GetLessonTitleFail()
    {
        var lessonList = GetLessonsData();
        _lessonService.Setup(x => x.GetLessonByTitle("Chiant la"))
            .Returns(lessonList[2]);
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.GetLessonByTitle("IUHIUHU");

        if (lessonsResult is NotFoundObjectResult nfObjectResult)
        {
            var valeur = nfObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.IsNotType<LessonDto>(valeur);
            Assert.DoesNotContain(valeur, lessonList);
            Assert.False(lessonList == valeur);
        }
    }

    [Fact]
    public void GetLessonTitleFail_Argument_Exception()
    {
        _lessonService.Setup(x => x.GetLessonByTitle("title"))
            .Throws<ArgumentException>();
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var result = lessonsController.GetLessonByTitle("title");

        if (result is NotFoundObjectResult nfObjectResult)
        {
            Assert.NotNull(nfObjectResult);
        }
    }

    [Fact]
    public void DeleteLessonSuccess()
    {
        _lessonService.Setup(x => x.DeleteLesson(1))
            .Returns(true);
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.DeleteLesson(1);
        if (lessonsResult is OkObjectResult okObjectResult)
        {
            bool valeur = (bool)okObjectResult.Value;

            Assert.True(valeur);
        }
    }

    [Fact]
    public void DeleteLessonFail()
    {
        _lessonService.Setup(x => x.DeleteLesson(1))
            .Returns(true);
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.DeleteLesson(100);
        if (lessonsResult is NotFoundObjectResult nfObjectResult)
        {
            Assert.Null(nfObjectResult.Value);
            Assert.IsNotType<bool>(nfObjectResult.Value);
        }
    }

    [Fact]
    public void CreateLessonSuccess()
    {
        _lessonService.Setup(x => x.CreateLesson(96,"Le nouveau titre", "Le nouvel éditeur", new DateOnly(2024, 03, 16)))
            .Returns(new LessonDto("Le nouveau titre", "Le nouvel éditeur", new DateOnly(2024, 03, 16)));
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult =
            lessonsController.CreateLesson(new LessonDto("Le nouveau titre", "Le nouvel éditeur",
                new DateOnly(2024, 03, 16)));
        if (lessonsResult is CreatedResult createdObjectResult)
        {
            LessonDto valeur = createdObjectResult.Value as LessonDto;

            Assert.NotNull(valeur);
            Assert.Equal("Le nouveau titre", valeur.Title);
            Assert.Equal("Le nouvel éditeur", valeur.LastPublisher);
            Assert.Equal(new DateOnly(2024, 03, 16), valeur.LastEdit);
        }
    }

    [Fact]
    public void CreateLessonFail()
    {
        _lessonService.Setup(x => x.CreateLesson(42,"Le nouveau titre", "Le nouvel éditeur", new DateOnly(2024, 03, 16)))
            .Returns(new LessonDto("Le nouveau titre", "Le nouvel éditeur", new DateOnly(2024, 03, 16)));
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult =
            lessonsController.CreateLesson(new LessonDto(null, "Le nouvel éditeur", new DateOnly(2024, 03, 16)));

        if (lessonsResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }

    [Fact]
    public void CreateLessonFail_Argument_Exception()
    {
        _lessonService.Setup(x => x.CreateLesson(35672653, "duehduheudheu nouveau titre", "Le deudhe éditeur", new DateOnly(2024, 03, 16)))
            .Throws<ArgumentException>();
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var result = lessonsController.CreateLesson(new LessonDto(35672653, "duehduheudheu nouveau titre", "Le deudhe éditeur", new DateOnly(2024, 03, 16)));

        if (result is NotFoundObjectResult nfObjectResult)
        {
            Assert.NotNull(nfObjectResult);
        }
    }

    [Fact]
    public void UpdateLessonSuccess()
    {
        _lessonService.Setup(x =>
                x.UpdateLesson(1, new LessonDto(1, "Titre update", "Le dernier publisher", new DateOnly(2022, 02, 02))))
            .Returns(new LessonDto(1, "Titre update", "Le dernier publisher", new DateOnly(2022, 02, 02)));
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.UpdateLesson(1,
            new LessonDto(1, "Titre update", "Le dernier publisher", new DateOnly(2022, 02, 02)));
        if (lessonsResult is OkObjectResult okObjectResult)
        {
            LessonDto valeur = okObjectResult.Value as LessonDto;

            Assert.NotNull(valeur);
            Assert.Equal("Titre update", valeur.Title);
            Assert.Equal("Le dernier publisher", valeur.LastPublisher);
            Assert.Equal(new DateOnly(2022, 02, 02), valeur.LastEdit);
            Assert.Equal(1, valeur.Id);
        }
    }

    [Fact]
    public void UpdateLessonFail()
    {
        _lessonService.Setup(x =>
                x.UpdateLesson(1, new LessonDto(1, "Titre update", "Le dernier publisher", new DateOnly(2022, 02, 02))))
            .Returns(new LessonDto(1, "Titre update", "Le dernier publisher", new DateOnly(2022, 02, 02)));
        var lessonsController = new LessonsController(_lessonService.Object, new NullLogger<LessonsController>());

        var lessonsResult = lessonsController.UpdateLesson(1,
            new LessonDto(2, "Titre update", "Le dernier publisher", new DateOnly(2022, 02, 02)));

        if (lessonsResult is BadRequestResult bdObjectResult)
        {
            Assert.Equal(400, bdObjectResult.StatusCode);
        }
    }


    private List<LessonDto> GetLessonsData()
    {
        List<LessonDto> lessonsData = new List<LessonDto>(4)
        {
            new(1, "Le titre", "Clément", new DateOnly(2024, 03, 10)),
            new(2, "Pas titre", "Erwan", new DateOnly(2024, 02, 11)),
            new(3, "Chiant la", "Une personne", new DateOnly(2012, 12, 25)),
            new("Les fleurs du mal", "Baudelaire", new DateOnly(1872, 10, 01)),
        };
        return lessonsData;
    }
}