using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using Shared;

namespace TestAPI;

public class InquiryTableUnitTest
{
    private readonly Mock<IInquiryTableService<InquiryTableDto>> _inquiryTableService;

    public InquiryTableUnitTest()
    {
        _inquiryTableService = new Mock<IInquiryTableService<InquiryTableDto>>();
    }

    [Fact]
    public void GetDatabaseNameFromInquiryId_Success()
    {
        var database = "Inquiry1";
        _inquiryTableService.Setup(x => x.GetDatabaseNameByInquiryId(42))
            .Returns(database);
        var inquiryTableController =
            new InquiryTableController(_inquiryTableService.Object, new NullLogger<InquiryTableController>());

        var inquiryTableResult = inquiryTableController.GetDatabaseNameByInquiryById(42);

        if (inquiryTableResult is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;
            Assert.NotNull(valeur);
            Assert.Equal(database, ((KeyValuePair<string,string>)valeur).Value);
        }
    }
    
    [Fact]
    public void GetDatabaseNameFromInquiryId_Throws_ArgumentException()
    {
        _inquiryTableService.Setup(x => x.GetDatabaseNameByInquiryId(42))
            .Throws<ArgumentException>();
        var inquiryTableController =
            new InquiryTableController(_inquiryTableService.Object, new NullLogger<InquiryTableController>());

        var inquiryTableResult = inquiryTableController.GetDatabaseNameByInquiryById(42);

        Assert.NotNull(inquiryTableResult); 
        Assert.Equal(typeof(NotFoundResult), inquiryTableResult.GetType());
    }
}