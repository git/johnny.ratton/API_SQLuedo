using API.Controllers;
using Dto;
using Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Model.OrderCriteria;
using Moq;
using Shared;
using TestAPI.Extensions;

namespace TestAPI;

public class BlackListUnitTest
{
    private readonly Mock<IBlackListService<BlackListDto>> _blackListService;

    public BlackListUnitTest()
    {
        _blackListService = new Mock<IBlackListService<BlackListDto>>();
    }
    [Fact]
    public void IsBanned()
    {
        _blackListService.Setup(x => x.GetUserBannedByEmail("email@example.com"))
            .Returns(new BlackListDto { Email = "email@example.com", ExpirationDate = DateOnly.FromDateTime(DateTime.Now)});
        var usersController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);
        var result = usersController.GetUserBannedByEmail("email@example.com");
        Assert.Equal(typeof(OkObjectResult), result.GetType());
    }
    
    [Fact]
    public void IsBannedNotFound()
    {
        _blackListService.Setup(x => x.GetUserBannedByEmail("example@notfound.com"))
            .Returns<BlackListDto?>(null);
        var usersController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);
        var result = usersController.GetUserBannedByEmail("example@notfound.com");
        Assert.Equal(typeof(NotFoundObjectResult), result.GetType());
    }
    
    [Fact]
    public void BanUser()
    {
        _blackListService.Setup(x => x.BanUser("Test1"))
            .Returns(true);
        var usersController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var userResult = usersController.BanUser("Test1");
        Assert.Equal(typeof(OkObjectResult), userResult.GetType());
    }
    
    [Fact]
    public void BanUserNotFound()
    {
        _blackListService.Setup(x => x.BanUser("Test1"))
            .Returns(true);
        var usersController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var userResult = usersController.BanUser("Test42");
        Assert.Equal(typeof(NotFoundResult), userResult.GetType());
    }
    
    [Fact]
    public void UnbanUser()
    {
        _blackListService.Setup(x => x.UnbanUser("example@email.com"))
            .Returns(true);
        var usersController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var userResult = usersController.UnbanUser("example@email.com");
        Assert.Equal(typeof(OkObjectResult), userResult.GetType());
    }
    
    [Fact]
    public void UnbanUserNotFound()
    {
        _blackListService.Setup(x => x.UnbanUser("example@email.com"))
            .Returns(false);
        var usersController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var userResult = usersController.UnbanUser("example@email.com");
        Assert.Equal(typeof(NotFoundResult), userResult.GetType());
    }
    
    [Fact]
    public void GetBannedUsers_NoneOrderCriteria()
    {
        _blackListService.Setup(x => x.GetBannedUsers(1,10,BlackListOdrerCriteria.None))
            .Returns(new List<BlackListDto>()
            {
                new BlackListDto { Email = "example1@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
                new BlackListDto { Email = "example2@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
                new BlackListDto { Email = "example3@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) }
            });
        var blackListController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var result = blackListController.GetBannedUsers(1,10,BlackListOdrerCriteria.None);
        Assert.Equal(typeof(OkObjectResult), result.GetType());
        if (result is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetBlackList().ToString(), valeur.ToString());
            Assert.True(GetBlackList().SequenceEqual(valeur as IEnumerable<BlackListDto>, new BlackListDtoEqualityComparer()));
        }
    }
    
    [Fact]
    public void GetBannedUsers_OrderByEmail()
    {
        _blackListService.Setup(x => x.GetBannedUsers(1,10,BlackListOdrerCriteria.ByEmail))
            .Returns(new List<BlackListDto>()
            {
                new BlackListDto { Email = "example1@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
                new BlackListDto { Email = "example2@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
                new BlackListDto { Email = "example3@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) }
            });
        var blackListController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var result = blackListController.GetBannedUsers(1,10,BlackListOdrerCriteria.ByEmail);
        Assert.Equal(typeof(OkObjectResult), result.GetType());
        if (result is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetBlackList().ToString(), valeur.ToString());
            Assert.True(GetBlackList().SequenceEqual(valeur as IEnumerable<BlackListDto>, new BlackListDtoEqualityComparer()));
        }
    }
    
    [Fact]
    public void GetBannedUsers_OrderedByExpirationDate()
    {
        _blackListService.Setup(x => x.GetBannedUsers(1,10,BlackListOdrerCriteria.ByExpirationDate))
            .Returns(new List<BlackListDto>()
            {
                new BlackListDto { Email = "example1@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
                new BlackListDto { Email = "example2@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
                new BlackListDto { Email = "example3@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) }
            });
        var blackListController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var result = blackListController.GetBannedUsers(1,10,BlackListOdrerCriteria.ByExpirationDate);
        Assert.Equal(typeof(OkObjectResult), result.GetType());
        if (result is OkObjectResult okObjectResult)
        {
            var valeur = okObjectResult.Value;

            Assert.NotNull(valeur);
            Assert.Equal(GetBlackList().ToString(), valeur.ToString());
            Assert.True(GetBlackList().SequenceEqual(valeur as IEnumerable<BlackListDto>, new BlackListDtoEqualityComparer()));
        }
    }

    [Fact]
    public void Get_0_BannedUsers_OrderedByNone()
    {
        _blackListService.Setup(x => x.GetBannedUsers(1, 10, BlackListOdrerCriteria.None))
            .Returns(new List<BlackListDto>());
        var blackListController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var result = blackListController.GetBannedUsers(1, 10, BlackListOdrerCriteria.None);
        Assert.Equal(typeof(StatusCodeResult), result.GetType());
        if (result is NotFoundObjectResult notFoundObjectResult)
        {
            var valeur = notFoundObjectResult.Value;

            Assert.NotNull(valeur);
        }
    }

    [Fact]
    public void GetNbBannedUsers()
    {
        _blackListService.Setup(x => x.GetNumberOfBannedUsers())
            .Returns(10);
        var usersController = new BlackListController(new NullLogger<UsersController>(), _blackListService.Object);

        var userResult = usersController.GetNumberOfBannedUsers();
        Assert.Equal(typeof(OkObjectResult), userResult.GetType());
        Assert.Equal(10, ((KeyValuePair<string,int>)(userResult as OkObjectResult).Value).Value);
    }

    private IEnumerable<BlackListDto> GetBlackList()
    {
        return new List<BlackListDto>()
        {
            new BlackListDto { Email = "example1@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
            new BlackListDto { Email = "example2@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) },
            new BlackListDto { Email = "example3@email.com" , ExpirationDate = DateOnly.FromDateTime(DateTime.Now) }
        };
    }
}