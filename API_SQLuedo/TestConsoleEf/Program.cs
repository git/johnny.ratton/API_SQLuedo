﻿using System.Security.Cryptography;
using DbContextLib;
using Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using StubbedContextLib;

var connection = new SqliteConnection("DataSource=:memory:");
connection.Open();
var options = new DbContextOptionsBuilder<UserDbContext>()
    .UseSqlite(connection)
    .Options;


// Partie sur les utilisateurs
using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    // Test sur les utilisateurs
    var users = db.Users;

    // Affichage de tous les utilisateurs
    Console.WriteLine("Affichage des noms des utilisateurs");
    foreach (var user in users)
    {
        Console.WriteLine(user.Username);
    }

    // Affichage des utilisateurs filtrés
    Console.WriteLine("\nAffichage des utilisateurs contenant e");
    var filteredUsers = users.Where(u => u.Username.Contains("e"));
    foreach (var user in filteredUsers)
    {
        Console.WriteLine(user.Username);
    }

    // Affichage des utilisateurs triés
    Console.WriteLine("\nAffichage des utilisateurs triés selon leur nom");
    var orderedUsers = users.OrderBy(u => u.Username);
    foreach (var user in orderedUsers)
    {
        Console.WriteLine(user.Username);
    }

    // Ajout d'un utilisateur
    Console.WriteLine("\nAjout du nouvel utilisateur");
    var newUser = new UserEntity
        {
            Username = "le nouveau du groupe",
            Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: "motdepasse",
                salt: RandomNumberGenerator.GetBytes(128 / 8),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8)),
            Email = "Efff.fffff@etu.uca.fr",
            IsAdmin = true
        }
        ;
    if (!users.Any(u => u.Username == newUser.Username))
    {
        users.Add(newUser);
        await db.SaveChangesAsync();
    }
}

using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    var users = db.Users;
    // Affichage du nouvel utilisateur
    Console.WriteLine("\nAffichage du nouvel utilisateur");
    var u = await users.FirstOrDefaultAsync(u => u.Username == "le nouveau du groupe");
    foreach (var pptt in typeof(UserEntity).GetProperties().Where(p => p.CanWrite && p.Name != nameof(UserEntity.Id)))
    {
        Console.WriteLine(pptt.GetValue(u));
    }

    if (u != null)
    {
        // Suppression du nouvel utilisateur
        Console.WriteLine("\nSuppression du nouvel utilisateur");
        db.Users.Remove(u);
        await db.SaveChangesAsync();
    }
}

using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    var users = db.Users;
    // Affichage des utilisateurs après suppression
    Console.WriteLine("\nAffichage des utilisateurs après suppression");
    foreach (var user in users)
    {
        Console.WriteLine(user.Username);
    }

    // Modification d'un utilisateur
    Console.WriteLine("\nModification de l'utilisateur clement");
    var userToModify = await users.FirstOrDefaultAsync(u => u.Username == "clement");
    if (userToModify != null)
    {
        userToModify.Username = "clement modifié";
        await db.SaveChangesAsync();
    }
}

using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    var users = db.Users;
    // Affichage des utilisateurs après modification
    var userToModify = await users.FirstOrDefaultAsync(u => u.Username == "clement modifié");
    if (userToModify != null)
    {
        Console.WriteLine("\nAffichage de l'utilisateur après modification");
        Console.WriteLine(userToModify.Username);
        // Rollback
        userToModify = await users.FirstOrDefaultAsync(u => u.Username == "clement modifié");
        if (userToModify != null)
        {
            userToModify.Username = "clement";
            await db.SaveChangesAsync();
        }
    }
}

// Partie sur les enquetes
using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    // Test sur les enquetes
    var inquiries = db.Inquiries;

    // Affichage de toutes les enquetes
    Console.WriteLine("\nAffichage des noms des enquetes");
    foreach (var inquiry in inquiries)
    {
        Console.WriteLine(inquiry.Title);
    }

    // Affichage des enquetes filtrées
    Console.WriteLine("\nAffichage des enquetes contenant o");
    var filteredInquiries = inquiries.Where(i => i.Title.Contains("o"));
    foreach (var inquiry in filteredInquiries)
    {
        Console.WriteLine(inquiry.Title);
    }

    // Affichage des enquetes triées
    Console.WriteLine("\nAffichage des enquetes triées selon leur nom");
    var orderedInquiries = inquiries.OrderBy(i => i.Title);
    foreach (var inquiry in orderedInquiries)
    {
        Console.WriteLine(inquiry.Title);
    }

    var solutions = db.Solutions;

    // Affichage de la solution de l'enquete sur les orang outan
    Console.WriteLine("\nAffichage de la solution de l'enquete sur les orang outan");

    var inquiryEntity = await inquiries.FirstOrDefaultAsync(i => i.Title == "L'enquête sur les orang outan");
    var solution = await solutions.FirstOrDefaultAsync(s => s.OwnerId == inquiryEntity.Id);
    foreach (var pptt in typeof(SolutionEntity).GetProperties()
                 .Where(p => p.CanRead && p.Name != nameof(SolutionEntity.Owner)))
    {
        Console.WriteLine(pptt.GetValue(solution));
    }

    // Ajout d'une enquete
    Console.WriteLine("\nAjout de la nouvelle enquete");
    var newInquiry = new InquiryEntity
    {
        Title = "La nouvelle enquete",
        Description = "La description de la nouvelle enquete",
        IsUser = true
    };
    if (!inquiries.Any(inquiry => inquiry.Title == newInquiry.Title))
    {
        inquiries.Add(newInquiry);
        await db.SaveChangesAsync();
    }
}

using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    var inquiries = db.Inquiries;
    // Affichage de la nouvelle enquete
    Console.WriteLine("\nAffichage de la nouvelle enquete");
    var i = await inquiries.FirstOrDefaultAsync(i => i.Title == "La nouvelle enquete");
    foreach (var pptt in typeof(InquiryEntity).GetProperties()
                 .Where(p => p.CanWrite && p.Name != nameof(InquiryEntity.Id)))
    {
        Console.WriteLine(pptt.GetValue(i));
    }

    if (i != null)
    {
        // Suppression de la nouvelle enquete
        Console.WriteLine("\nSuppression de la nouvelle enquete");
        db.Inquiries.Remove(i);
        await db.SaveChangesAsync();
    }
}

using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    var inquiries = db.Inquiries;
    // Affichage des utilisateurs après suppression
    Console.WriteLine("\nAffichage des utilisateurs après suppression");
    foreach (var inquiry in inquiries)
    {
        Console.WriteLine(inquiry.Title);
    }

    // Modification d'une enquete
    Console.WriteLine("\nModification de l'enquete L'enquête de la carotte");
    var inquiryToModify = await inquiries.FirstOrDefaultAsync(i => i.Title == "L'enquête de la carotte");
    if (inquiryToModify != null)
    {
        inquiryToModify.Title = "L'enquête de la carotte modifiée";
        await db.SaveChangesAsync();
    }
}

using (var db = new StubbedContext(options))
{
    await db.Database.EnsureCreatedAsync();

    var inquiries = db.Inquiries;
    // Affichage des enquetes après modification
    var inquiryToModify = await inquiries.FirstOrDefaultAsync(i => i.Title == "L'enquête de la carotte modifiée");
    if (inquiryToModify != null)
    {
        Console.WriteLine("\nAffichage de l'enquete après modification");
        Console.WriteLine(inquiryToModify.Title);
        // Rollback
        inquiryToModify = await inquiries.FirstOrDefaultAsync(i => i.Title == "L'enquête de la carotte modifiée");
        if (inquiryToModify != null)
        {
            inquiryToModify.Title = "L'enquête de la carotte";
            await db.SaveChangesAsync();
        }
    }
}