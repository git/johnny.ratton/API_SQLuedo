﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class BlackListMapper
{
    public static BlackListDto FromModelToDto(this BlackList model)
    {
        return new BlackListDto(model.Email, model.ExpirationDate);
    }

    public static BlackListDto FromEntityToDto(this BlackListEntity ent)
    {
        return new BlackListDto(ent.Email, ent.ExpirationDate);
    }

    public static BlackList FromDtoToModel(this BlackListDto dto)
    {
        return new BlackList(dto.Email, dto.ExpirationDate);
    }

    public static BlackList FromEntityToModel(this BlackListEntity ent)
    {
        return new BlackList(ent.Email, ent.ExpirationDate);
    }

    public static BlackListEntity FromDtoToEntity(this BlackListDto dto)
    {
        return new BlackListEntity
        {
            Email = dto.Email,
            ExpirationDate = dto.ExpirationDate
        };
    }

    public static BlackListEntity FromModelToEntity(this BlackList model)
    {
        return new BlackListEntity
        {
            Email = model.Email,
            ExpirationDate = model.ExpirationDate
        };
    }
}