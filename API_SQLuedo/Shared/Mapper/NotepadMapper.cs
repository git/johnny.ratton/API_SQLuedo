﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class NotepadMapper
{
    public static Notepad FromDtoToModel(this NotepadDto dto)
    {
        return new Notepad(dto.Id, dto.UserId, dto.InquiryId, dto.Notes);
    }

    public static Notepad FromEntityToModel(this NotepadEntity ent)
    {
        return new Notepad(ent.Id, ent.UserId, ent.InquiryId, ent.Notes);
    }

    public static NotepadDto FromModelToDto(this Notepad not)
    {
        return new NotepadDto(not.Id, not.UserId, not.InquiryId, not.Notes);
    }

    public static NotepadDto FromEntityToDto(this NotepadEntity ent)
    {
        return new NotepadDto(ent.Id, ent.UserId, ent.InquiryId, ent.Notes);
    }

    public static NotepadEntity FromDtoToEntity(this NotepadDto dto)
    {
        return new NotepadEntity
        {
            Id = dto.Id,
            User = new UserEntity
            {
                Id = dto.UserId
            },
            UserId = dto.UserId,
            Inquiry = new InquiryEntity
            {
                Id = dto.InquiryId
            },
            Notes = dto.Notes
        };
    }

    public static NotepadEntity FromModelToEntity(this Notepad not)
    {
        return new NotepadEntity
        {
            Id = not.Id,
            User = new UserEntity
            {
                Id = not.UserId
            },
            UserId = not.UserId,
            Inquiry = new InquiryEntity
            {
                Id = not.InquiryId
            },
            Notes = not.Notes
        };
    }
}