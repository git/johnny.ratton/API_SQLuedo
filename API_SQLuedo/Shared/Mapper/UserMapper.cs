﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class UserMapper
{
    public static User FromDtoToModel(this UserDto dto)
    {
        return new User(dto.Id, dto.Username, dto.Password, dto.Email, dto.IsAdmin);
    }

    public static UserEntity FromDtoToEntity(this UserDto dto)
    {
        return new UserEntity
        {
            Id = dto.Id,
            Username = dto.Username,
            Password = dto.Password,
            Email = dto.Email,
            IsAdmin = dto.IsAdmin
        };
    }

    public static User FromEntityToModel(this UserEntity entity)
    {
        return new User(entity.Id, entity.Username, entity.Password, entity.Email, entity.IsAdmin);
    }

    public static UserDto FromEntityToDto(this UserEntity entity)
    {
        return new UserDto(entity.Id, entity.Username, entity.Password, entity.Email, entity.IsAdmin);
    }

    public static UserDto FromModelToDto(this User user)
    {
        return new UserDto(user.Id, user.Username, user.Password, user.Email, user.IsAdmin);
    }

    public static UserEntity FromModelToEntity(this User user)
    {
        return new UserEntity
        {
            Id = user.Id,
            Username = user.Username,
            Password = user.Password,
            Email = user.Email,
            IsAdmin = user.IsAdmin
        };
    }
}