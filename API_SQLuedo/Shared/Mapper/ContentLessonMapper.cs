using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class ContentLessonMapper
{
    public static ContentLessonDto FromEntityToDto(this ContentLessonEntity entity)
    {
        return new ContentLessonDto(entity.Id, entity.ContentContent, entity.ContentTitle, entity.LessonId);
    }
}