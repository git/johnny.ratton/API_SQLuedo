﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class InquiryMapper
{
    public static Inquiry FromDtoToModel(this InquiryDto inqDto)
    {
        return new Inquiry(inqDto.Id, inqDto.Title, inqDto.Description, inqDto.IsUser);
    }

    public static Inquiry FromEntityToModel(this InquiryEntity inqEntity)
    {
        return new Inquiry(inqEntity.Id, inqEntity.Title, inqEntity.Description, inqEntity.IsUser);
    }


    public static InquiryEntity FromModelToEntity(this Inquiry inq)
    {
        return new InquiryEntity
        {
            Id = inq.Id,
            Title = inq.Title,
            Description = inq.Description,
            IsUser = inq.IsUser
        };
    }

    public static InquiryEntity FromDtoToEntity(this InquiryDto inqDto)
    {
        return new InquiryEntity
        {
            Id = inqDto.Id,
            Title = inqDto.Title,
            Description = inqDto.Description,
            IsUser = inqDto.IsUser
        };
    }

    public static InquiryDto FromModelToDto(this Inquiry inq)
    {
        return new InquiryDto(inq.Id, inq.Title, inq.Description, inq.IsUser);
    }

    public static InquiryDto FromEntityToDto(this InquiryEntity inqEntity)
    {
        return new InquiryDto(inqEntity.Id, inqEntity.Title, inqEntity.Description, inqEntity.IsUser);
    }
}