﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class ParagraphMapper
{
    public static Paragraph FromDtoToModel(this ParagraphDto dto)
    {
        return new Paragraph(dto.ContentTitle, dto.ContentContent, dto.Info, dto.Query, dto.Comment);
    }

    public static Paragraph FromEntityToModel(this ParagraphEntity model)
    {
        return new Paragraph(model.ContentTitle, model.ContentContent, model.Info, model.Query, model.Comment);
    }

    public static ParagraphDto FromEntityToDto(this ParagraphEntity model)
    {
        return new ParagraphDto(model.Id, model.Title, model.Content, model.Info, model.Query,
            model.Comment, model.LessonId);
    }

    public static ParagraphDto FromModelToDto(this Paragraph model)
    {
        return new ParagraphDto(model.Id, model.ContentTitle, model.ContentContent, model.Info, model.Query,
            model.Comment, model.LessonId);
    }

    public static ParagraphEntity FromDtoToEntity(this ParagraphDto dto)
    {
        return new ParagraphEntity
        {
            Id = dto.Id,
            ContentTitle = dto.ContentTitle,
            ContentContent = dto.ContentContent,
            Title = dto.Title,
            Content = dto.Content,
            Info = dto.Info,
            Query = dto.Query,
            Comment = dto.Comment,
            LessonId = dto.LessonId
        };
    }

    public static ParagraphEntity FromModelToEntity(this Paragraph model)
    {
        return new ParagraphEntity
        {
            Id = model.Id,
            ContentTitle = model.ContentTitle,
            ContentContent = model.ContentContent,
            Info = model.Info,
            Query = model.Query,
            Comment = model.Comment, LessonId = model.LessonId
        };
    }
}