﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class LessonMapper
{
    public static LessonDto FromModelToDto(this Lesson model)
    {
        return new LessonDto(model.Id, model.Title, model.LastPublisher, model.LastEdit);
    }
    
    public static LessonDto FromEntityToDto(this LessonEntity model)
    {
        return new LessonDto(model.Id, model.Title, model.LastPublisher, model.LastEdit, model.Content.Select(c => c.FromEntityToDto()).ToList());
    }
    

    public static LessonEntity FromModelToEntity(this Lesson model)
    {
        return new LessonEntity
        {
            Id = model.Id,
            Title = model.Title,
            LastPublisher = model.LastPublisher,
            LastEdit = model.LastEdit
        };
    }
    public static LessonEntity FromDtoToEntity(this LessonDto dto)
    {
        DateTime date = DateTime.Now;
        return new LessonEntity
        {
            Id = dto.Id,
            Title = dto.Title,
            LastPublisher = dto.LastPublisher,
            LastEdit = dto.LastEdit ?? new DateOnly(date.Year,date.Month,date.Day)
        };
    }
    
    public static Lesson FromDtoToModel(this LessonDto dto)
    {
        DateTime date = DateTime.Now;
        return new Lesson(dto.Id, dto.Title, dto.LastPublisher, dto.LastEdit ?? new DateOnly(date.Year,date.Month,date.Day));
    }

    public static Lesson FromEntityToModel(this LessonEntity entity)
    {
        return new Lesson(entity.Id, entity.Title, entity.LastPublisher, entity.LastEdit);
    }
}