﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class SuccessMapper
{
    public static Success FromDtoToModel(this SuccessDto dto)
    {
        return new Success(dto.UserId, dto.InquiryId, dto.IsFinished);
    }

    public static Success FromEntityToModel(this SuccessEntity ent)
    {
        return new Success(ent.UserId, ent.InquiryId, ent.IsFinished);
    }

    public static SuccessDto FromModelToDto(this Success suc)
    {
        return new SuccessDto(suc.UserId, suc.InquiryId, suc.IsFinished);
    }

    public static SuccessDto FromEntityToDto(this SuccessEntity ent)
    {
        return new SuccessDto(ent.UserId, ent.InquiryId, ent.IsFinished);
    }

    public static SuccessEntity FromDtoToEntity(this SuccessDto dto)
    {
        return new SuccessEntity
        {
            UserId = dto.UserId,
            User = new UserEntity
            {
                Id = dto.UserId
            },
            InquiryId = dto.InquiryId,
            Inquiry = new InquiryEntity
            {
                Id = dto.InquiryId
            },
            IsFinished = dto.IsFinished
        };
    }

    public static SuccessEntity FromModelToEntity(this Success suc)
    {
        return new SuccessEntity
        {
            UserId = suc.UserId,
            User = new UserEntity
            {
                Id = suc.UserId
            },
            InquiryId = suc.InquiryId,
            Inquiry = new InquiryEntity
            {
                Id = suc.InquiryId
            },
            IsFinished = suc.IsFinished
        };
    }
}