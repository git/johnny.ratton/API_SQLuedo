﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class SolutionMapper
{
    public static Solution FromDtoToModel(this SolutionDto dto)
    {
        return new Solution(dto.OwnerId, dto.MurdererFirstName, dto.MurdererLastName, dto.MurderPlace, dto.MurderWeapon,
            dto.Explanation);
    }

    public static Solution FromEntityToModel(this SolutionEntity entity)
    {
        return new Solution(entity.OwnerId, entity.MurdererFirstName, entity.MurdererLastName, entity.MurderPlace,
            entity.MurderWeapon, entity.Explaination);
    }

    public static SolutionDto FromModelToDto(this Solution model)
    {
        return new SolutionDto(model.OwnerId, model.MurdererFirstName, model.MurdererLastName, model.MurderPlace,
            model.MurderWeapon, model.Explaination);
    }

    public static SolutionDto FromEntityToDto(this SolutionEntity entity)
    {
        return new SolutionDto(entity.OwnerId, entity.MurdererFirstName, entity.MurdererLastName, entity.MurderPlace,
            entity.MurderWeapon, entity.Explaination);
    }

    public static SolutionEntity FromModelToEntity(this Solution model)
    {
        return new SolutionEntity
        {
            OwnerId = model.OwnerId,
            Owner = new InquiryEntity
            {
                Id = model.OwnerId
            },
            MurdererFirstName = model.MurdererFirstName,
            MurdererLastName = model.MurdererLastName,
            MurderPlace = model.MurderPlace,
            MurderWeapon = model.MurderWeapon,
            Explaination = model.Explaination
        };
    }

    public static SolutionEntity FromDtoToEntity(this SolutionDto dto)
    {
        return new SolutionEntity
        {
            OwnerId = dto.OwnerId,
            Owner = new InquiryEntity
            {
                Id = dto.OwnerId
            },
            MurdererFirstName = dto.MurdererFirstName,
            MurdererLastName = dto.MurdererLastName,
            MurderPlace = dto.MurderPlace,
            MurderWeapon = dto.MurderWeapon,
            Explaination = dto.Explanation
        };
    }
}