﻿using Dto;
using Entities;
using Model;

namespace Shared.Mapper;

public static class InquiryTableMapper
{
    public static InquiryTable FromDtoToModel(this InquiryTableDto inqTDto)
    {
        return new InquiryTable(inqTDto.OwnerId, inqTDto.ConnectionInfo, inqTDto.DatabaseName);
    }

    public static InquiryTable FromEntityToModel(this InquiryTableEntity inqTEntity)
    {
        return new InquiryTable(inqTEntity.OwnerId, inqTEntity.ConnectionInfo, inqTEntity.DatabaseName);
    }

    public static InquiryTableDto FromModelToDto(this InquiryTable inqT)
    {
        return new InquiryTableDto(inqT.OwnerId, inqT.DatabaseName, inqT.ConnectionInfo);
    }

    public static InquiryTableDto FromEntityToDto(this InquiryTableEntity inqTEntity)
    {
        return new InquiryTableDto(inqTEntity.OwnerId, inqTEntity.DatabaseName, inqTEntity.ConnectionInfo);
    }

    public static InquiryTableEntity FromModelToEntity(this InquiryTable inqT)
    {
        return new InquiryTableEntity
        {
            OwnerId = inqT.OwnerId,
            DatabaseName = inqT.DatabaseName,
            ConnectionInfo = inqT.ConnectionInfo
        };
    }

    public static InquiryTableEntity FromDtoToEntity(this InquiryTableDto dto)
    {
        return new InquiryTableEntity
        {
            OwnerId = dto.OwnerId,
            DatabaseName = dto.DatabaseName,
            ConnectionInfo = dto.ConnectionInfo
        };
    }
}