namespace Shared;

public interface INotepadService<TNotepad>
{
    public TNotepad GetNotepadFromUserAndInquiryId(int userId, int inquiryId);

    public void SetNotepadFromUserAndInquiryId(int userId, int inquiryId, string notes);

    public void UpdateNotepadFromUserAndInquiryId(int userId, int inquiryId, string notes);
}