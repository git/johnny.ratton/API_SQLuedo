namespace Shared;

public interface IInquiryTableService<TInquiryTable>
{
    public string GetDatabaseNameByInquiryId(int id);
}