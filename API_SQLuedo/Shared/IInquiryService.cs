﻿using Model.OrderCriteria;

namespace Shared;

public interface IInquiryService<TInquiry>
{
    public IEnumerable<TInquiry> GetInquiries(int page, int number, InquiryOrderCriteria orderCriteria);
    public int GetNumberOfInquiries();
    public TInquiry GetInquiryById(int id);
    public TInquiry GetInquiryByTitle(string title);
    public bool DeleteInquiry(int id);
    public TInquiry UpdateInquiry(int id, TInquiry inquiry);
    public TInquiry CreateInquiry(string title, string description, bool isUser);
}