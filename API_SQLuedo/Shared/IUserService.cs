﻿using Model.OrderCriteria;

namespace Shared
{
    public interface IUserService<TUser>
    {
        public IEnumerable<TUser> GetUsers(int page, int number, UserOrderCriteria orderCriteria);
        public IEnumerable<TUser> GetNotAdminUsers(int page, int number, UserOrderCriteria orderCriteria);
        public int GetNumberOfUsers();
        public TUser GetUserById(int id);
        public TUser GetUserByUsername(string username);
        public TUser GetUserByEmail(string email);
        public bool DeleteUser(int id);
        public bool DeleteUserByUsername(string username);
        public TUser UpdateUser(int id, TUser user);
        public TUser CreateUser(string username, string password, string email, bool isAdmin);
        public bool IsEmailTaken(string email);
        public bool IsUsernameTaken(string username);
        public TUser PromoteUser(int id);
    }
}