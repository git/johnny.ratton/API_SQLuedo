using Model.OrderCriteria;

namespace Shared;

public interface IBlackListService<TBlackList>
{
    public IEnumerable<TBlackList> GetBannedUsers(int page, int number, BlackListOdrerCriteria orderCriteria);
    public int GetNumberOfBannedUsers();
    public TBlackList? GetUserBannedByEmail(string email);
    public bool BanUser(string username);
    public bool UnbanUser(string email);
}