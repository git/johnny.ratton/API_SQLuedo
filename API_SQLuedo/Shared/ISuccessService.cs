﻿using Model.OrderCriteria;

namespace Shared
{
    public interface ISuccessService<TSuccess>
    {
        public IEnumerable<TSuccess> GetSuccesses(int page, int number, SuccessOrderCriteria orderCriteria);
        public IEnumerable<TSuccess> GetSuccessesByUserId(int id);
        public IEnumerable<TSuccess> GetSuccessesByInquiryId(int id);
        public bool DeleteSuccess(int idUser, int idInquiry);
        public TSuccess UpdateSuccess(int idUser, int idInquiry, TSuccess success);
        public TSuccess CreateSuccess(int userId, int inquiryId, bool isFinished);
    }
}