namespace Shared;

public interface ISolutionService<TSolution>
{
    public TSolution GetSolutionByInquiryId(int id);
}