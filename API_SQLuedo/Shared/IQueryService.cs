﻿using Microsoft.AspNetCore.Mvc;
using Model.OrderCriteria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;

namespace Shared
{
    public interface IQueryService<TQuery>
    {
        public TQuery ExecuteQuery(string query, string database);
        public QueryDto GetTables(string database);
        public QueryDto GetColumns(string database,string table);
    }
}
