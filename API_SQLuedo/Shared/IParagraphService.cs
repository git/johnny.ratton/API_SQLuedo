﻿using Model.OrderCriteria;

namespace Shared
{
    public interface IParagraphService<TParagraph>
    {
        public IEnumerable<TParagraph> GetParagraphs(int page, int number, ParagraphOrderCriteria orderCriteria);
        public IEnumerable<TParagraph> GetParagraphsByLessonId(int lessonId);
        public TParagraph GetParagraphById(int id);
        public TParagraph GetParagraphByTitle(string title);
        public bool DeleteParagraph(int id);
        public TParagraph UpdateParagraph(int id, TParagraph paragraph);

        public TParagraph CreateParagraph(string contentTitle, string contentContent, string title, string content, string info, string query, string comment,
            int lessonId);
    }
}