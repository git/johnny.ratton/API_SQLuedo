﻿using Model.OrderCriteria;

namespace Shared;

public interface ILessonService<TLesson>
{
    public IEnumerable<TLesson> GetLessons(int page, int number, LessonOrderCriteria orderCriteria);
    public int GetNumberOfLessons();
    public TLesson GetLessonById(int id);
    public TLesson GetLessonByTitle(string title);
    public bool DeleteLesson(int id);
    public TLesson UpdateLesson(int id, TLesson lesson);
    public TLesson CreateLesson(int id, string title, string lastPublisher, DateOnly lastEdit);
}