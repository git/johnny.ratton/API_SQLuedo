﻿using System.Security.Cryptography;
using DbContextLib;
using DevOne.Security.Cryptography.BCrypt;
using Entities;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;

namespace StubbedContextLib;

public class StubbedContext : UserDbContext
{
    public StubbedContext(DbContextOptions<UserDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        builder.Entity<UserEntity>().HasData(
            new UserEntity
            {
                Id = 1,
                Username = "johnny",
                /*Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: "Motdepasse",
                    salt: RandomNumberGenerator.GetBytes(128 / 8),
                    prf: KeyDerivationPrf.HMACSHA256,
                    iterationCount: 100000,
                    numBytesRequested: 256 / 8)),*/
                Password = BCryptHelper.HashPassword("Motdepasse", BCryptHelper.GenerateSalt()),
                Email = "Johnny.RATTON@etu.uca.fr",
                IsAdmin = true
            },
            new UserEntity
            {
                Id = 2,
                Username = "maxime",
                /*Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: "Motdepasse",
                    salt: RandomNumberGenerator.GetBytes(128 / 8),
                    prf: KeyDerivationPrf.HMACSHA256,
                    iterationCount: 100000,
                    numBytesRequested: 256 / 8)),*/
                Password = BCryptHelper.HashPassword("Motdepasse", BCryptHelper.GenerateSalt()),
                Email = "Maxime.SAPOUNTZIS@etu.uca.fr",
                IsAdmin = true
            },
            new UserEntity
            {
                Id = 3,
                Username = "clement",
                /*Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: "Motdepasse",
                    salt: RandomNumberGenerator.GetBytes(128 / 8),
                    prf: KeyDerivationPrf.HMACSHA256,
                    iterationCount: 100000,
                    numBytesRequested: 256 / 8)),*/
                Password = BCryptHelper.HashPassword("Motdepasse", BCryptHelper.GenerateSalt()),
                Email = "Clement.CHIEU@etu.uca.fr",
                IsAdmin = true
            },
            new UserEntity
            {
                Id = 4,
                Username = "erwan",
                /*Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: "Motdepasse",
                    salt: RandomNumberGenerator.GetBytes(128 / 8),
                    prf: KeyDerivationPrf.HMACSHA256,
                    iterationCount: 100000,
                    numBytesRequested: 256 / 8)),*/
                Password = BCryptHelper.HashPassword("Motdepasse", BCryptHelper.GenerateSalt()),
                Email = "Erwan.MENAGER@etu.uca.fr",
                IsAdmin = true
            },
            new UserEntity
            {
                Id = 5,
                Username = "victor",
                /*Password = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                    password: "Motdepasse",
                    salt: RandomNumberGenerator.GetBytes(128 / 8),
                    prf: KeyDerivationPrf.HMACSHA256,
                    iterationCount: 100000,
                    numBytesRequested: 256 / 8)),*/
                Password = BCryptHelper.HashPassword("Motdepasse", BCryptHelper.GenerateSalt()),
                Email = "Victor.GABORIT@etu.uca.fr",
                IsAdmin = true
            });

        /*builder.Entity<InquiryTableEntity>().HasData(
            new InquiryTableEntity
            {
                OwnerId = 1,
                DatabaseName = "Inquiry1",
                ConnectionInfo =
                    "Server=localhost;Database=Inquiry1;Trusted_Connection=True;MultipleActiveResultSets=true"
            },
            new InquiryTableEntity
            {
                OwnerId = 2,
                DatabaseName = "Inquiry2",
                ConnectionInfo =
                    "Server=localhost;Database=Inquiry2;Trusted_Connection=True;MultipleActiveResultSets=true"
            },
            new InquiryTableEntity
            {
                OwnerId = 3,
                DatabaseName = "Inquiry3",
                ConnectionInfo =
                    "Server=localhost;Database=Inquiry3;Trusted_Connection=True;MultipleActiveResultSets=true"
            });

        builder.Entity<SolutionEntity>().HasData(
            new SolutionEntity
            {
                OwnerId = 1,
                MurdererFirstName = "Maxime",
                MurdererLastName = "Sapountzis",
                MurderPlace = "La cuisine",
                MurderWeapon = "Le couteau",
                Explaination = "Parce que c'est Maxime"
            },
            new SolutionEntity
            {
                OwnerId = 2,
                MurdererFirstName = "Johnny",
                MurdererLastName = "Ratton",
                MurderPlace = "La cuisine",
                MurderWeapon = "Le couteau",
                Explaination = "Parce que il est fou"
            },
            new SolutionEntity
            {
                OwnerId = 3,
                MurdererFirstName = "Erwan",
                MurdererLastName = "Menager",
                MurderPlace = "La salle de bain",
                MurderWeapon = "L'arachide",
                Explaination = "Parce que c'est Erwan"
            });

        builder.Entity<InquiryEntity>().HasData(
            new InquiryEntity
            {
                Id = 1,
                Title = "L'enquête de la carotte",
                Description = "La description de l'inquiry1",
                IsUser = true
            },
            new InquiryEntity
            {
                Id = 2,
                Title = "L'enquête sur les orang outan",
                Description = "The new description",
                IsUser = false
            },
            new InquiryEntity
            {
                Id = 3,
                Title = "L'enquête sur les parapluies",
                Description = "Il pleuvait",
                IsUser = false
            }
        );

        builder.Entity<SuccessEntity>().HasData(
            new SuccessEntity
            {
                UserId = 1,
                InquiryId = 1,
                IsFinished = true
            },
            new SuccessEntity
            {
                UserId = 2,
                InquiryId = 2,
                IsFinished = false
            },
            new SuccessEntity
            {
                UserId = 3,
                InquiryId = 3,
                IsFinished = true
            }
        );
        builder.Entity<LessonEntity>().HasData(
            new LessonEntity
            {
                Id = 1, Title = "Lesson N°1", LastEdit = DateOnly.FromDateTime(DateTime.Now), LastPublisher = "Clément"
            },
            new LessonEntity
            {
                Id = 2, Title = "Lesson N°2", LastEdit = DateOnly.FromDateTime(DateTime.Now), LastPublisher = "Clément"
            },
            new LessonEntity
            {
                Id = 3, Title = "Lesson N°3", LastEdit = DateOnly.FromDateTime(DateTime.Now), LastPublisher = "Clément"
            },
            new LessonEntity
            {
                Id = 4, Title = "Lesson N°4", LastEdit = DateOnly.FromDateTime(DateTime.Now), LastPublisher = "Clément"
            }
        );
        builder.Entity<ParagraphEntity>().HasData(
            new ParagraphEntity { Id = 1, LessonId = 1, ContentTitle = "Paragraph N°1", ContentContent = "Content of paragraph N°1", Title = "Title Paragraph N°1", Content = "Content of paragraph N°1", Info = "Infos", Query = "SELECT * FROM Table", Comment = "Comment of paragraph N°1"},
            new ParagraphEntity { Id = 2, LessonId = 1, ContentTitle = "Paragraph N°2", ContentContent = "Content of paragraph N°2", Title = "Title Paragraph N°2", Content = "Content of paragraph N°2", Info = "Infos", Query = "SELECT * FROM Table", Comment = "Comment of paragraph N°2"},
            new ParagraphEntity { Id = 3, LessonId = 1, ContentTitle = "Paragraph N°3", ContentContent = "Content of paragraph N°3", Title = "Title Paragraph N°3", Content = "Content of paragraph N°3", Info = "Infos", Query = "SELECT * FROM Table", Comment = "Comment of paragraph N°3"},
            new ParagraphEntity { Id = 4, LessonId = 2, ContentTitle = "Paragraph N°1", ContentContent = "Content of paragraph N°1", Title = "Title Paragraph N°1", Content = "Content of paragraph N°1", Info = "Infos", Query = "SELECT * FROM Table", Comment = "Comment of paragraph N°1"}
            );*/
    }
}