﻿using Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace DbContextLib
{
    public class UserDbContext : IdentityDbContext<IdentityUser>
    {
        public DbSet<UserEntity> Users { get; set; }
        public DbSet<BlackListEntity> BlackLists { get; set; }
        public DbSet<InquiryEntity> Inquiries { get; set; }
        public DbSet<InquiryTableEntity> InquiryTables { get; set; }
        public DbSet<LessonEntity> Lessons { get; set; }
        public DbSet<ParagraphEntity> Paragraphs { get; set; }
        public DbSet<SolutionEntity> Solutions { get; set; }
        public DbSet<SuccessEntity> Successes { get; set; }
        public DbSet<NotepadEntity> Notepads { get; set; }

        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(Environment.GetEnvironmentVariable("CO_STRING", EnvironmentVariableTarget.Process));
                //optionsBuilder.UseNpgsql("Host=localhost;Database=SQLuedo;Username=admin;Password=motdepasse");
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Permet d'avoir les champs de la classe mère dans les classes filles et de ne pas avoir de table pour la classe mère
            builder.Entity<ContentLessonEntity>().UseTpcMappingStrategy();

            builder.Entity<SuccessEntity>().HasKey(s => s.UserId);
            builder.Entity<SuccessEntity>().HasKey(s => s.InquiryId);
            builder.Entity<InquiryEntity>().HasKey(s => s.Id);
            base.OnModelCreating(builder);
        }
    }
}