﻿namespace Dto;

public class NotepadDto
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int InquiryId { get; set; }
    public string Notes { get; set; }

    public NotepadDto()
    {
    }

    public NotepadDto(int id, int userId, int inquiryId, string notes)
    {
        Id = id;
        UserId = userId;
        InquiryId = inquiryId;
        Notes = notes;
    }

    public NotepadDto(int userId, int inquiryId, string notes)
    {
        UserId = userId;
        InquiryId = inquiryId;
        Notes = notes;
    }
}