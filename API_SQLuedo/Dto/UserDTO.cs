﻿namespace Dto
{
    public class UserDto : IEquatable<UserDto>
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }

        public UserDto()
        {
        }

        public UserDto(int id, string username, string password, string email, bool isAdmin)
        {
            Id = id;
            Username = username;
            Password = password;
            Email = email;
            IsAdmin = isAdmin;
        }

        public UserDto(string username, string password, string email, bool isAdmin)
        {
            Username = username;
            Password = password;
            Email = email;
            IsAdmin = isAdmin;
        }

        public override string ToString()
        {
            return $"{Id}\t{Username}\t{Email}\t{IsAdmin}";
        }

        public bool Equals(UserDto? other)
        {
            return (this.Id == other?.Id);
        }

        public override bool Equals(object? obj)
        {
            if (object.ReferenceEquals(obj, null))
            {
                return false;
            }

            if (object.ReferenceEquals(this, obj))
            {
                return true;
            }

            if (this.GetType() != obj.GetType())
            {
                return false;
            }

            return this.Equals(obj as UserDto);
        }

        public override int GetHashCode()
        {
            return Id;
        }
    }
}