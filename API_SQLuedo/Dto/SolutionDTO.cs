﻿namespace Dto;

public class SolutionDto
{
    public int OwnerId { get; set; }
    public string MurdererFirstName { get; set; }
    public string MurdererLastName { get; set; }
    public string MurderPlace { get; set; }
    public string MurderWeapon { get; set; }
    public string Explanation { get; set; }

    public SolutionDto()
    {
    }

    public SolutionDto(int ownerId, string murdererFirstName, string murdererLastName, string murderPlace,
        string murderWeapon, string explanation)
    {
        OwnerId = ownerId;
        MurdererFirstName = murdererFirstName;
        MurdererLastName = murdererLastName;
        MurderPlace = murderPlace;
        MurderWeapon = murderWeapon;
        Explanation = explanation;
    }

    public SolutionDto(string murdererFirstName, string murdererLastName, string murderPlace, string murderWeapon,
        string explanation)
    {
        MurdererFirstName = murdererFirstName;
        MurdererLastName = murdererLastName;
        MurderPlace = murderPlace;
        MurderWeapon = murderWeapon;
        Explanation = explanation;
    }
}