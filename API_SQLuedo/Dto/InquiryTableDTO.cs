﻿namespace Dto;

public class InquiryTableDto
{
    public int OwnerId { get; set; }
    public string DatabaseName { get; set; }
    public string ConnectionInfo { get; set; }

    public InquiryTableDto()
    {
    }

    public InquiryTableDto(int ownerId, string databaseName, string connectionInfo)
    {
        OwnerId = ownerId;
        DatabaseName = databaseName;
        ConnectionInfo = connectionInfo;
    }
}