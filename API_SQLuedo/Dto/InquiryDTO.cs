﻿using System.Runtime.Serialization;

namespace Dto;

[DataContract]
public class InquiryDto : IEquatable<InquiryDto>
{
    [DataMember]
    public int Id { get; set; }

    [DataMember]
    public string Title { get; set; }

    [DataMember]
    public string Description { get; set; }

    [DataMember]
    public bool IsUser { get; set; }

    public InquiryDto()
    {
    }

    public InquiryDto(int id, string title, string description, bool isUser)
    {
        Id = id;
        Title = title;
        Description = description;
        IsUser = isUser;
    }

    public InquiryDto(string title, string description, bool isUser)
    {
        Title = title;
        Description = description;
        IsUser = isUser;
    }

    public override string ToString()
    {
        return $"{Id}\t{Title}\t{Description}\t{IsUser}";
    }

    public override bool Equals(object obj)
    {
        if (object.ReferenceEquals(obj, null))
        {
            return false;
        }

        if (object.ReferenceEquals(this, obj))
        {
            return true;
        }

        if (this.GetType() != obj.GetType())
        {
            return false;
        }

        return this.Equals(obj as InquiryDto);
    }

    public bool Equals(InquiryDto other)
    {
        return (this.Id == other.Id);
    }

    public override int GetHashCode()
    {
        return Id;
    }
}