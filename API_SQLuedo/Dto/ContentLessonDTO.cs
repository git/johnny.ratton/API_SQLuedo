﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Dto;

[DataContract]
public class ContentLessonDto
{
    [DataMember(Name = "id")]
    public int Id { get; set; }
    [DataMember(Name = "contentContent")]
    public string ContentContent { get; set; }
    [DataMember(Name = "contentTitle")]
    public string ContentTitle { get; set; }
    [DataMember(Name = "lessonId")]
    public int LessonId { get; set; }

    public ContentLessonDto(int id, string contentContent, string contentTitle, int lessonId)
    {
        Id = id;
        ContentContent = contentContent;
        ContentTitle = contentTitle;
        LessonId = lessonId;
    }

    protected ContentLessonDto(string contentContent, string contentTitle, int lessonId)
    {
        ContentContent = contentContent;
        ContentTitle = contentTitle;
        LessonId = lessonId;
    }

    public ContentLessonDto()
    {
    }
}