﻿namespace Dto;

public class BlackListDto
{
    public string Email { get; set; }
    public DateOnly ExpirationDate { get; set; }

    public BlackListDto()
    {
    }

    public BlackListDto(string email, DateOnly expirationDate)
    {
        Email = email;
        ExpirationDate = expirationDate;
    }
}