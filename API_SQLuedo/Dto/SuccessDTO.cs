﻿namespace Dto;

public class SuccessDto : IEquatable<SuccessDto>
{
    public int UserId { get; set; }
    public int InquiryId { get; set; }
    public bool IsFinished { get; set; }

    public SuccessDto()
    {
    }

    public SuccessDto(int userId, int inquiryId, bool isFinished)
    {
        UserId = userId;
        InquiryId = inquiryId;
        IsFinished = isFinished;
    }

    public override string ToString()
    {
        return $"User :{UserId}\t Enquête : {InquiryId}\t{IsFinished}";
    }

    public override bool Equals(object obj)
    {
        if (object.ReferenceEquals(obj, null))
        {
            return false;
        }

        if (object.ReferenceEquals(this, obj))
        {
            return true;
        }

        if (this.GetType() != obj.GetType())
        {
            return false;
        }

        return this.Equals(obj as SuccessDto);
    }

    public bool Equals(SuccessDto other)
    {
        return (this.UserId == other.UserId && this.InquiryId == other.InquiryId);
    }

    public override int GetHashCode()
    {
        return UserId * InquiryId;
    }
}