﻿using System.Runtime.Serialization;

namespace Dto;

[DataContract]
public class ParagraphDto : ContentLessonDto, IEquatable<ParagraphDto>
{
    [DataMember(Name = "title")]
    public string Title { get; set; }
    [DataMember(Name = "content")]
    public string Content { get; set; }
    [DataMember(Name = "info")]
    public string Info { get; set; }
    [DataMember(Name = "query")]
    public string Query { get; set; }
    [DataMember(Name = "comment")]
    public string Comment { get; set; }

    public ParagraphDto(string contentTitle, string contentContent, string title, string content, string info, string query, string comment, int lessonId) :
        base(contentContent,
            contentTitle, lessonId)
    {
        Title = title;
        Content = content;
        Info = info;
        Query = query;
        Comment = comment;
    }
    public ParagraphDto(string title, string content, string info, string query, string comment, int lessonId) :
        base(content,
            title, lessonId)
    {
        Title = title;
        Content = content;
        Info = info;
        Query = query;
        Comment = comment;
    }

    public ParagraphDto(int id, string title, string content, string info, string query, string comment, int lessonId) :
        base(id, content,
            title, lessonId)
    {
        Id = id;
        Title = title;
        Content = content;
        Info = info;
        Query = query;
        Comment = comment;
    }

    public ParagraphDto() : base()
    {
    }

    public override string ToString()
    {
        return $"{Id}\t{Title}\t{Content}\t{Info}\t{Query}\t{Comment}";
    }

    public override bool Equals(object obj)
    {
        if (object.ReferenceEquals(obj, null))
        {
            return false;
        }

        if (object.ReferenceEquals(this, obj))
        {
            return true;
        }

        if (this.GetType() != obj.GetType())
        {
            return false;
        }

        return this.Equals(obj as ParagraphDto);
    }

    public bool Equals(ParagraphDto other)
    {
        return (this.Id == other.Id);
    }

    public override int GetHashCode()
    {
        return Id;
    }
}