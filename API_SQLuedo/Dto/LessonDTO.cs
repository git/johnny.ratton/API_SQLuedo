﻿using System.Net;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
namespace Dto;

[DataContract]
public class LessonDto : IEquatable<LessonDto>
{
    [DataMember]
    public int Id { get; set; }
    [DataMember]
    public string? Title { get; set; }
    [DataMember]
    public string? LastPublisher { get; set; }
    [DataMember]
    public DateOnly? LastEdit { get; set; }
    [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
    public ICollection<ContentLessonDto> Content { get; set; } = new List<ContentLessonDto>();

    public LessonDto()
    {
    }
    public LessonDto(int id, string title, string lastPublisher, DateOnly lastEdit)
    {
        Id = id;
        Title = title;
        LastPublisher = lastPublisher;
        LastEdit = lastEdit;
    }
    
    public LessonDto(int id, string title, string lastPublisher, DateOnly? lastEdit, ICollection<ContentLessonDto> content)
    {
        Id = id;
        Title = title;
        LastPublisher = lastPublisher;
        LastEdit = lastEdit;
        Content = content;
    }
    public LessonDto(string title, string lastPublisher, DateOnly lastEdit)
    {
        Title = title;
        LastPublisher = lastPublisher;
        LastEdit = lastEdit;
    }

    public override string ToString()
    {
        return $"{Id}\t{Title}\t{LastPublisher}\t{LastEdit}";
    }

    public override bool Equals(object obj)
    {
        if (object.ReferenceEquals(obj, null))
        {
            return false;
        }

        if (object.ReferenceEquals(this, obj))
        {
            return true;
        }

        if (this.GetType() != obj.GetType())
        {
            return false;
        }

        return this.Equals(obj as LessonDto);
    }

    public bool Equals(LessonDto other)
    {
        return (this.Id == other.Id);
    }

    public override int GetHashCode()
    {
        return Id;
    }
}