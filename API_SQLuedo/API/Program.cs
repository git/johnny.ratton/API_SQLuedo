using API;
using API.Service;
using Asp.Versioning;
using DbContextLib;
using DbDataManager.Service;
using Dto;
using Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Model;
using Shared;
using StubbedContextLib;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IQueryService<QueryDto>, QueryDataServiceApi>();

builder.Services.AddScoped<IUserService<UserEntity>, UserDataService>();
builder.Services.AddScoped<IUserService<UserDto>, UserDataServiceApi>();

builder.Services.AddScoped<IBlackListService<BlackListEntity>, BlackListDataService>();
builder.Services.AddScoped<IBlackListService<BlackListDto>, BlackListDataServiceAPI>();

builder.Services.AddScoped<IInquiryService<InquiryEntity>, InquiryDataService>();
builder.Services.AddScoped<IInquiryService<InquiryDto>, InquiryDataServiceApi>();

builder.Services.AddScoped<ISolutionService<SolutionEntity>, SolutionDataService>();
builder.Services.AddScoped<ISolutionService<SolutionDto>, SolutionDataServiceAPI>();

builder.Services.AddScoped<IInquiryTableService<InquiryTableEntity>, InquiryTableDataService>();
builder.Services.AddScoped<IInquiryTableService<InquiryTableDto>, InquiryTableDataServiceAPI>();

builder.Services.AddScoped<IParagraphService<ParagraphEntity>, ParagraphDataService>();
builder.Services.AddScoped<IParagraphService<ParagraphDto>, ParagraphDataServiceApi>();

builder.Services.AddScoped<ISuccessService<SuccessEntity>, SuccessDataService>();
builder.Services.AddScoped<ISuccessService<SuccessDto>, SuccessDataServiceApi>();

builder.Services.AddScoped<ILessonService<LessonEntity>, LessonDataService>();
builder.Services.AddScoped<ILessonService<LessonDto>, LessonDataServiceApi>();

builder.Services.AddScoped<INotepadService<NotepadEntity>, NotepadDataService>();
builder.Services.AddScoped<INotepadService<NotepadDto>, NotepadDataServiceAPI>();


builder.Services.AddDbContext<DbContext,UserDbContext>((serviceProvider, options) =>
{
    if (builder.Environment.IsProduction())
    {
        Console.WriteLine("I am in production mode");
        var address = Environment.GetEnvironmentVariable("DB_HOST", EnvironmentVariableTarget.Process)
                      ?? throw new ArgumentException("Missing DB_HOST environment variable");
        var database = Environment.GetEnvironmentVariable("DB_DATABASE", EnvironmentVariableTarget.Process)
                       ?? throw new ArgumentException("Missing DB_DATABASE environment variable");
        var user = Environment.GetEnvironmentVariable("DB_USER", EnvironmentVariableTarget.Process)
                   ?? throw new ArgumentException("Missing DB_USER environment variable");
        var password = Environment.GetEnvironmentVariable("DB_PASSWORD", EnvironmentVariableTarget.Process)
                       ?? throw new ArgumentException("Missing DB_PASSWORD environment variable");

        var coString = $"Host={address};Database={database};Username={user};Password={password}";
        options.UseNpgsql(coString);
    }
    else
    {
        Console.WriteLine("I am in development mode");
        options.UseNpgsql("Host=localhost;Database=SQLuedo;Username=admin;Password=motdepasse");
    }
});
builder.Services.AddIdentityApiEndpoints<IdentityUser>().AddEntityFrameworkStores<UserDbContext>();

builder.Services.AddAuthorization();
builder.Services.AddApiVersioning(o =>
{
    o.AssumeDefaultVersionWhenUnspecified = true;
    o.DefaultApiVersion = new ApiVersion(1);
    o.ReportApiVersions = true;
}).AddApiExplorer(
    options =>
    {
        options.GroupNameFormat = "'v'VVV";
        options.SubstituteApiVersionInUrl = true;
    });
//builder.Services.AddIdentityApiEndpoints<IdentityUser>()
//    .AddEntityFrameworkStores<UserDbContext>();
builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1", new OpenApiInfo { Title = "Demo API", Version = "v1" });
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] { }
        }
    });
});
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOrigin",
        builder =>
        {
            builder.WithOrigins("https://localhost:7171")
                .AllowAnyMethod()
                .AllowAnyHeader();
        });
});

var configuration = new ConfigurationBuilder()
    .SetBasePath(Directory.GetCurrentDirectory())
    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
    .Build();

builder.Services.AddSingleton(configuration);


var app = builder.Build();

app.UseCors("AllowSpecificOrigin");

// Création de l'utilisateur admin pour la base de données Identity

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    // Création base de données via les migrations
    await using var dbContext = services.GetRequiredService<UserDbContext>();
    //await dbContext.Database.MigrateAsync();

    var userManager = services.GetRequiredService<UserManager<IdentityUser>>();

    try
    {
        var user = new IdentityUser { UserName = "admin@example.com", Email = "admin@example.com" };
        var result = await userManager.CreateAsync(user, "Mdp_1234");
        if (result.Succeeded)
        {
            Console.WriteLine("Utilisateur admin créé avec succès.");
        }
        else
        {
            foreach (var error in result.Errors)
            {
                Console.WriteLine($"Erreur lors de la création de l'utilisateur : {error.Description}");
            }
        }
    }
    catch (Exception ex)
    {
        var logger = services.GetRequiredService<ILogger<Program>>();
        logger.LogError(ex, "Une erreur s'est produite lors de la création de l'utilisateur.");
    }
}


app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapIdentityApi<IdentityUser>();

app.Run();