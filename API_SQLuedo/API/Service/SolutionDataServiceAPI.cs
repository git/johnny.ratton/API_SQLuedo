using Dto;
using Entities;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class SolutionDataServiceAPI(ISolutionService<SolutionEntity> solutionService) : ISolutionService<SolutionDto>
{
    public SolutionDto GetSolutionByInquiryId(int id)
    {
        return solutionService.GetSolutionByInquiryId(id).FromEntityToDto();
    }
}