﻿using Dto;
using Entities;
using Model.OrderCriteria;
using Npgsql;
using Shared;
using Shared.Mapper;
using Microsoft.Extensions.Configuration;
using System.Text;
using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace API.Service;

public class QueryDataServiceApi : IQueryService<QueryDto>{
    private readonly IConfiguration _configuration;

    public QueryDataServiceApi(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    public QueryDto ExecuteQuery(string query, string database)
    {
        string connectionString = _configuration.GetConnectionString("DefaultConnection");
        connectionString = connectionString.Replace("{database}", database);

        if (string.IsNullOrEmpty(database))
        {
            return new QueryDto { Result = "Le nom de la base de données est requis." };
        }

        try
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(query, connection))
                {
                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        List<Dictionary<string, object>> resultList = new List<Dictionary<string, object>>();


                        List<string> columnNames = new List<string>();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            columnNames.Add(reader.GetName(i));
                        }

                        while (reader.Read())
                        {
                            Dictionary<string, object> row = new Dictionary<string, object>();
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                row[columnNames[i]] = reader[i].ToString();
                            }

                            resultList.Add(row);
                        }
                        string resultJson = JsonConvert.SerializeObject(resultList);

                        QueryDto queryDto = new QueryDto { Result = resultJson };
                        return queryDto;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            return new QueryDto { Result = ex.Message };
        }
    }

    public QueryDto GetTables(string database)
    {
        string connectionString = _configuration.GetConnectionString("DefaultConnection");
        connectionString = connectionString.Replace("{database}", database);

        try
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand())
                {
                    command.Connection = connection;
                    // Donner et exclure les bonnes permissions au rôle en question

                    // GRANT SELECT ON TABLE information_schema.tables TO votre_utilisateur;

                    // GRANT SELECT ON TABLE information_schema.columns TO votre_utilisateur;

                    // REVOKE ALL ON SCHEMA information_schema FROM PUBLIC;

                    command.CommandText =
                        "SELECT table_name FROM information_schema.tables WHERE table_schema = 'public';";

                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        List<string> tableNames = new List<string>();

                        while (reader.Read())
                        {
                            tableNames.Add(reader["table_name"].ToString());
                        }

                        List<object> tablesDict = new List<object>();
                        foreach (string tableName in tableNames)
                        {
                            tablesDict.Add(new { TableName = tableName});
                        }

                        string tablesJson = JsonConvert.SerializeObject(tablesDict);

                        QueryDto queryDto = new QueryDto { Result = tablesJson };
                        return queryDto;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            return new QueryDto { Result = ex.Message };
        }
    }

    public QueryDto GetColumns(string database, string table)
    {
        string connectionString = _configuration.GetConnectionString("DefaultConnection");
        connectionString = connectionString.Replace("{database}", database);

        using (NpgsqlConnection connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand())
                {
                    command.Connection = connection;
                    // Donner et exclure les bonnes permissions au rôle en question

                    //GRANT SELECT ON TABLE information_schema.tables TO votre_utilisateur;

                    //GRANT SELECT ON TABLE information_schema.columns TO votre_utilisateur;

                    //REVOKE ALL ON SCHEMA information_schema FROM PUBLIC;

                    command.CommandText =
                        $"SELECT column_name FROM information_schema.columns WHERE table_name = '{table}';";

                    Console.WriteLine(command.CommandText);

                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        List<string> columnsNames = new List<string>();

                        while (reader.Read())
                        {
                            columnsNames.Add(reader["column_name"].ToString());
                        }

                       /*List<object> columnsDict = new List<object>();
                        foreach (string colName in columnsNames)
                        {
                            columnsDict.Add(new { ColumnName = colName});
                        }*/

                        string tablesJson = JsonConvert.SerializeObject(columnsNames);

                        Console.WriteLine(tablesJson);

                        QueryDto queryDto = new QueryDto { Result = tablesJson };
                        return queryDto;
                    }
                }
            }
        
    }
}