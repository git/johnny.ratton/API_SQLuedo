﻿using Dto;
using Entities;
using Model.OrderCriteria;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class ParagraphDataServiceApi(IParagraphService<ParagraphEntity> paragraphService)
    : IParagraphService<ParagraphDto>
{
    public IEnumerable<ParagraphDto> GetParagraphs(int page, int number, ParagraphOrderCriteria orderCriteria)
    {
        var paragraphsEntities = paragraphService.GetParagraphs(page, number, orderCriteria);
        return paragraphsEntities.Select(e => e.FromEntityToDto()).ToList();
    }

    public IEnumerable<ParagraphDto> GetParagraphsByLessonId(int lessonId)
    {
        return paragraphService.GetParagraphsByLessonId(lessonId).Select(p => p.FromEntityToDto());
    }

    public ParagraphDto GetParagraphById(int id) => paragraphService.GetParagraphById(id).FromEntityToDto();

    public ParagraphDto GetParagraphByTitle(string title) =>
        paragraphService.GetParagraphByTitle(title).FromEntityToDto();

    public bool DeleteParagraph(int id) => paragraphService.DeleteParagraph(id);

    public ParagraphDto UpdateParagraph(int id, ParagraphDto paragraph) =>
        paragraphService.UpdateParagraph(id, paragraph.FromDtoToEntity()).FromEntityToDto();

    public ParagraphDto CreateParagraph(string contentTitle, string contentContent,string title, string content, string info, string query, string comment,
        int lessonId) => paragraphService.CreateParagraph(contentTitle, contentContent, title, content, info, query, comment, lessonId).FromEntityToDto();
}