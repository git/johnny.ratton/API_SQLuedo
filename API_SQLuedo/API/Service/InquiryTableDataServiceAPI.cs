using Dto;
using Entities;
using Shared;

namespace API.Service;

public class InquiryTableDataServiceAPI(IInquiryTableService<InquiryTableEntity> inquiryTableService) : IInquiryTableService<InquiryTableDto>
{
    public string GetDatabaseNameByInquiryId(int id)
    {
        return inquiryTableService.GetDatabaseNameByInquiryId(id);
    }
}