using Dto;
using Entities;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class NotepadDataServiceAPI(INotepadService<NotepadEntity> notepadService) : INotepadService<NotepadDto>
{
    public NotepadDto GetNotepadFromUserAndInquiryId(int userId, int inquiryId)
    {
        return notepadService.GetNotepadFromUserAndInquiryId(userId, inquiryId).FromEntityToDto();
    }

    public void SetNotepadFromUserAndInquiryId(int userId, int inquiryId, string notes)
    {
        notepadService.SetNotepadFromUserAndInquiryId(userId,inquiryId,notes);
    }

    public void UpdateNotepadFromUserAndInquiryId(int userId, int inquiryId, string notes)
    {
        notepadService.UpdateNotepadFromUserAndInquiryId(userId,inquiryId,notes);
    }
}