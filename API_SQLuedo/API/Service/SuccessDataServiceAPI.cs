﻿using Dto;
using Entities;
using Model.OrderCriteria;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class SuccessDataServiceApi(ISuccessService<SuccessEntity> sucessService) : ISuccessService<SuccessDto>
{
    public IEnumerable<SuccessDto> GetSuccesses(int page, int number, SuccessOrderCriteria orderCriteria)
    {
        var successesEntities = sucessService.GetSuccesses(page, number, orderCriteria);
        return successesEntities.Select(e => e.FromEntityToDto()).ToList();
    }

    public IEnumerable<SuccessDto> GetSuccessesByUserId(int id) =>
        sucessService.GetSuccessesByUserId(id).Select(s => s.FromEntityToDto());

    public IEnumerable<SuccessDto> GetSuccessesByInquiryId(int id) =>
        sucessService.GetSuccessesByInquiryId(id).Select(s => s.FromEntityToDto());

    public bool DeleteSuccess(int idUser, int idInquiry) => sucessService.DeleteSuccess(idUser, idInquiry);

    public SuccessDto UpdateSuccess(int idUser, int idInquiry, SuccessDto success) =>
        sucessService.UpdateSuccess(idUser, idInquiry, success.FromDtoToEntity()).FromEntityToDto();

    public SuccessDto CreateSuccess(int userId, int inquiryId, bool isFinished) =>
        sucessService.CreateSuccess(userId, inquiryId, isFinished).FromEntityToDto();
}