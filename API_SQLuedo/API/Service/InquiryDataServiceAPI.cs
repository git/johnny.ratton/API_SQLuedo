﻿using Dto;
using Entities;
using Model.OrderCriteria;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class InquiryDataServiceApi(IInquiryService<InquiryEntity> inquiryService) : IInquiryService<InquiryDto>
{
    public InquiryDto UpdateInquiry(int id, InquiryDto inquiry)
    {
        return inquiryService.UpdateInquiry(id, inquiry.FromDtoToEntity()).FromEntityToDto();
    }

    public InquiryDto CreateInquiry(string title, string description, bool isUser)
    {
        return inquiryService.CreateInquiry(title, description, isUser).FromEntityToDto();
    }

    public bool DeleteInquiry(int id)
    {
        return inquiryService.DeleteInquiry(id);
    }

    public IEnumerable<InquiryDto> GetInquiries(int page, int number, InquiryOrderCriteria orderCriteria)
    {
        var inquiries = inquiryService.GetInquiries(page, number, orderCriteria);
        return inquiries.Select(i => i.FromEntityToDto()).ToList();
    }

    public int GetNumberOfInquiries()
    {
        return inquiryService.GetNumberOfInquiries();
    }

    public InquiryDto GetInquiryById(int id) => inquiryService.GetInquiryById(id).FromEntityToDto();

    public InquiryDto GetInquiryByTitle(string title) => inquiryService.GetInquiryByTitle(title).FromEntityToDto();
}