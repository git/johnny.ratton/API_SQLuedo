﻿using Dto;
using Entities;
using Model.OrderCriteria;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class LessonDataServiceApi(ILessonService<LessonEntity> lessonService) : ILessonService<LessonDto>
{
    public IEnumerable<LessonDto> GetLessons(int page, int number, LessonOrderCriteria orderCriteria)
    {
        var lessonsEntities = lessonService.GetLessons(page, number, orderCriteria);
        return lessonsEntities.Select(e => e.FromEntityToDto()).ToList();
    }

    public int GetNumberOfLessons()
    {
        return lessonService.GetNumberOfLessons();
    }

    public LessonDto GetLessonById(int id) => lessonService.GetLessonById(id).FromEntityToDto();

    public LessonDto GetLessonByTitle(string title) => lessonService.GetLessonByTitle(title).FromEntityToDto();

    public bool DeleteLesson(int id) => lessonService.DeleteLesson(id);

    public LessonDto UpdateLesson(int id, LessonDto lesson) =>
        lessonService.UpdateLesson(id, lesson.FromDtoToEntity()).FromEntityToDto();

    public LessonDto UpdateLesson(int id, LessonEntity lesson) =>
        lessonService.UpdateLesson(id, lesson).FromEntityToDto();

    public LessonDto CreateLesson(int id, string title, string lastPublisher, DateOnly lastEdit) =>
        lessonService.CreateLesson(id, title, lastPublisher, lastEdit).FromEntityToDto();
}