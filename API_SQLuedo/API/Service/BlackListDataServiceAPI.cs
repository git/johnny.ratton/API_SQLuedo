using Dto;
using Entities;
using Model.OrderCriteria;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class BlackListDataServiceAPI (IBlackListService<BlackListEntity> userService) : IBlackListService<BlackListDto>
{
    public IEnumerable<BlackListDto> GetBannedUsers(int page, int number, BlackListOdrerCriteria orderCriteria) =>
        userService.GetBannedUsers(page, number, orderCriteria).Select(b => b.FromEntityToDto());

    public int GetNumberOfBannedUsers() => userService.GetNumberOfBannedUsers();
    public BlackListDto? GetUserBannedByEmail(string email)
    {
        var res = userService.GetUserBannedByEmail(email);
        if (res == null)
        {
            return null;
        }
        return res.FromEntityToDto();
    }

    public bool BanUser(string username) => userService.BanUser(username);
    public bool UnbanUser(string email) => userService.UnbanUser(email);
}