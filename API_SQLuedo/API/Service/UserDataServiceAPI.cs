﻿using Dto;
using Entities;
using Model.OrderCriteria;
using Shared;
using Shared.Mapper;

namespace API.Service;

public class UserDataServiceApi(IUserService<UserEntity> userService) : IUserService<UserDto>
{
    public IEnumerable<UserDto> GetUsers(int page, int number, UserOrderCriteria orderCriteria)
    {
        var usersEntities = userService.GetUsers(page, number, orderCriteria);
        return usersEntities.Select(e => e.FromEntityToDto()).ToList();
    }
    
    public IEnumerable<UserDto> GetNotAdminUsers(int page, int number, UserOrderCriteria orderCriteria)
    {
        var usersEntities = userService.GetNotAdminUsers(page, number, orderCriteria);
        return usersEntities.Select(e => e.FromEntityToDto()).ToList();
    }
    
    public int GetNumberOfUsers()
    {
        return userService.GetNumberOfUsers();
    }
    public UserDto GetUserById(int id) => userService.GetUserById(id).FromEntityToDto();

    public UserDto GetUserByUsername(string username) => userService.GetUserByUsername(username).FromEntityToDto();
    public UserDto GetUserByEmail(string email) => userService.GetUserByEmail(email).FromEntityToDto();

    public bool DeleteUser(int id) => userService.DeleteUser(id);
    
    public bool DeleteUserByUsername(string username) => userService.DeleteUserByUsername(username);

    public UserDto UpdateUser(int id, UserDto user) =>
        userService.UpdateUser(id, user.FromDtoToEntity()).FromEntityToDto();

    public UserDto CreateUser(string username, string password, string email, bool isAdmin) =>
        userService.CreateUser(username, password, email, isAdmin).FromEntityToDto();

    public bool IsEmailTaken(string email) => userService.IsEmailTaken(email);

    public bool IsUsernameTaken(string username) => userService.IsUsernameTaken(username);

    public UserDto PromoteUser(int id) =>
        userService.PromoteUser(id).FromEntityToDto();
}