﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Dto;
using Model.OrderCriteria;
using Shared;
using Asp.Versioning;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]
[ApiVersion("1.0")]
[ApiController]
public class InquiriesController : Controller
{
    private readonly IInquiryService<InquiryDto> _inquiryDataService;

    private readonly ILogger<InquiriesController> _logger;

    public InquiriesController(IInquiryService<InquiryDto> inquiryDataService, ILogger<InquiriesController> logger)
    {
        _inquiryDataService = inquiryDataService;
        _logger = logger;
    }

    [HttpGet("inquiries/{page:int}/{number:int}/{orderCriteria}")]
    [ProducesResponseType(typeof(InquiryDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetInquiries(int page, int number, InquiryOrderCriteria orderCriteria)
    {
        var nbInquiry = _inquiryDataService.GetInquiries(page, number, orderCriteria).Count();
        if (nbInquiry == 0)
        {
            _logger.LogError("[ERREUR] Aucune enquête trouvée.");
            return StatusCode(204);
        }

        _logger.LogInformation("[INFORMATION] {nb} Enquête(s) trouvée(s)", nbInquiry);
        return Ok(_inquiryDataService.GetInquiries(page, number, orderCriteria));
    }

    [HttpGet("inquiry/{id:int}")]
    [ProducesResponseType(typeof(InquiryDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetInquiryById(int id)
    {
        try
        {
            _logger.LogInformation("[INFORMATION] L'enquête avec l'id {id} a été trouvé.", id);
            return Ok(_inquiryDataService.GetInquiryById(id));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucune enquête trouvée avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpGet("inquiry/{title:alpha}")]
    [ProducesResponseType(typeof(InquiryDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetInquiryByTitle(string title)
    {
        try
        {
            _logger.LogInformation("[INFORMATION] L'enquête avec le titre {title} a été trouvé.", title);
            return Ok(_inquiryDataService.GetInquiryByTitle(title));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucune enquête trouvée avec le titre {title}.", title);
            return NotFound();
        }
    }

    [HttpDelete("inquiry/{id:int}")]
    [ProducesResponseType(typeof(InquiryDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult DeleteInquiry(int id)
    {
        var success = _inquiryDataService.DeleteInquiry(id);
        if (success)
        {
            _logger.LogInformation("[INFORMATION] L'enquête avec l'id {id} a été supprimé.", id);
            return Ok(_inquiryDataService.DeleteInquiry(id));
        }
        else
        {
            _logger.LogError("[ERREUR] Aucune enquête trouvée avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(InquiryDto), 201)]
    [ProducesResponseType(typeof(string), 400)]
    public IActionResult CreateInquiry([FromBody] InquiryDto dto)
    {
        if (dto.Title == null || dto.Description == null)
        {
            return BadRequest();
        }

        _logger.LogInformation(
            "[INFORMATION] Une enquête a été créé : title - {title}, description - {description}, isUser - {isUser}",
            dto.Title, dto.Description, dto.IsUser);
        return Created(nameof(GetInquiries),
            _inquiryDataService.CreateInquiry(dto.Title, dto.Description, dto.IsUser));
    }

    [HttpPut("inquiry/{id:int}")]
    [ProducesResponseType(typeof(InquiryDto), 200)]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult UpdateInquiry(int id, [FromBody] InquiryDto InquiryDto)
    {
        if (id != InquiryDto.Id)
        {
            _logger.LogError("[ERREUR] Problème ID - La mise à jour de l'enquête avec l'id {id} a échouée.", id);
            return BadRequest();
        }

        if (!ModelState.IsValid)
        {
            _logger.LogError(
                "[ERREUR] Problème controlleur - La mise à jour de l'enquête avec l'id {id} a échouée.", id);
            return BadRequest();
        }

        if (InquiryDto != null)
        {
            _logger.LogInformation("[INFORMATION] La mise à jour de l'enquête avec l'id {id} a été effectuée", id);
            return Ok(_inquiryDataService.UpdateInquiry(id, InquiryDto));
        }

        _logger.LogError("[ERREUR] Aucune enquête trouvée avec l'id {id}.", id);
        return NotFound();
    }

    [HttpGet("inquiries/number")]
    [ProducesResponseType(typeof(InquiryDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetNumberOfInquiries()
    {
        var nbInquiry = _inquiryDataService.GetNumberOfInquiries();
        _logger.LogInformation("[INFORMATION] {nb} Enquête(s) trouvée(s)", nbInquiry);
        return Ok(new KeyValuePair<string, int>("nbInquiries", nbInquiry));
    }
}