﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Dto;
using Model.OrderCriteria;
using Shared;
using Asp.Versioning;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]
[ApiVersion("1.0")]
[ApiController]
public class SuccessesController : Controller
{
    private readonly ISuccessService<SuccessDto> _successDataService;

    private readonly ILogger<SuccessesController> _logger;

    public SuccessesController(ISuccessService<SuccessDto> successDataService, ILogger<SuccessesController> logger)
    {
        _successDataService = successDataService;
        _logger = logger;
    }

    [HttpGet("successes/{page:int}/{number:int}/{orderCriteria}")]
    [ProducesResponseType(typeof(SuccessDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    [ProducesResponseType(typeof(string), 400)]
    public IActionResult GetSuccesses(int page, int number, SuccessOrderCriteria orderCriteria)
    {
        if (page < 1 || number < 1)
        {
            _logger.LogError("[ERREUR] La page ou le nombre de succès est inférieur à 1.");
            return BadRequest("La page ou le nombre de succès est inférieur à 1.");
        }

        var nbUser = _successDataService.GetSuccesses(page, number, orderCriteria).ToList().Count;
        if (nbUser == 0)
        {
            _logger.LogError("[ERREUR] Aucun Succès trouvé.");
            return StatusCode(204);
        }

        _logger.LogInformation("[INFORMATION] {nb} Succès(s) trouvé(s)", nbUser);
        return Ok(_successDataService.GetSuccesses(page, number, orderCriteria));
    }

    [HttpGet("success/user/{id:int}")]
    [ProducesResponseType(typeof(SuccessDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetSuccessByUserId(int id)
    {
        if (id < 1)
        {
            _logger.LogError("[ERREUR] L'id de l'utilisateur est inférieur à 1.");
            return BadRequest("L'id de l'utilisateur est inférieur à 1.");
        }

        try
        {
            _logger.LogInformation("[INFORMATION] Le succès avec l'id de l'utilisateur {id} a été trouvé.", id);
            return Ok(_successDataService.GetSuccessesByUserId(id));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucun utilisateur trouvé avec l'id de l'utilisateur {id}.", id);
            return NotFound("Aucun utilisateur trouvé avec l'id de l'utilisateur.");
        }
    }

    [HttpGet("success/inquiry/{id:int}")]
    [ProducesResponseType(typeof(SuccessDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetSuccessByInquiryId(int id)
    {
        if (id < 1)
        {
            _logger.LogError("[ERREUR] L'id de l'enquête doit être inférieur à 1.");
            return BadRequest("L'id de l'enquête doit être inférieur à 1.");
        }

        try
        {
            _logger.LogInformation("[INFORMATION] Utilisateur avec l'id de l'enquête {inquiryId} a été trouvé.",
                id);
            return Ok(_successDataService.GetSuccessesByInquiryId(id));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucun utilisateur trouvé avec l'id de l'enquête {inquiryId}.", id);
            return NotFound("Aucune enquête trouvée avec l'id de l'enquête.");
        }
    }

    [HttpDelete("success/{idUser:int}/{idInquiry:int}")]
    [ProducesResponseType(typeof(SuccessDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult DeleteSuccess(int idUser, int idInquiry)
    {
        if (idUser < 1 || idInquiry < 1)
        {
            _logger.LogInformation("[INFORMATION] L'id de l'utilisateur ou de l'enquête doit être supérieur à 1.");
            return BadRequest("L'id de l'utilisateur ou de l'enquête doit être supérieur à 1.");
        }

        var success = _successDataService.DeleteSuccess(idUser, idInquiry);
        if (success)
        {
            _logger.LogInformation("[INFORMATION] Le succès avec l'id {id} a été supprimé.", idUser);
            return Ok(success);
        }
        else
        {
            _logger.LogError("[ERREUR] Aucun succès trouvé avec l'id {id}.", idUser);
            return NotFound();
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(SuccessDto), 201)]
    [ProducesResponseType(typeof(string), 409)]
    [ProducesResponseType(typeof(string), 404)]
    [ProducesResponseType(typeof(string), 400)]
    public IActionResult CreateSuccess([FromBody] SuccessDto dto)
    {
        if (dto.UserId < 1 || dto.InquiryId < 1)
        {
            _logger.LogError("[ERREUR] L'id de l'utilisateur ou de l'enquête doit être supérieur à 1.");
            return BadRequest("L'id de l'utilisateur ou de l'enquête doit être supérieur à 1.");
        }

        try
        {
            var s = _successDataService.CreateSuccess(dto.UserId, dto.InquiryId, dto.IsFinished);
            _logger.LogInformation(
                "[INFORMATION] Un succès a été créé : userId - {userId}, inquiryId - {inquiryId}, isFinished - {isFinished}",
                dto.UserId, dto.InquiryId, dto.IsFinished);
            return Created(nameof(GetSuccesses), s);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }

    [HttpPut("success/{idUser:int}/{idInquiry:int}")]
    [ProducesResponseType(typeof(SuccessDto), 200)]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult UpdateSuccess(int idUser, int idInquiry, [FromBody] SuccessDto successDto)
    {
        if (idUser < 1 || idInquiry < 1)
        {
            _logger.LogError("[ERREUR] L'id de l'utilisateur ou de l'enquête doit être supérieur à 1.");
            return BadRequest("L'id de l'utilisateur ou de l'enquête doit être supérieur à 1.");
        }

        if (idUser != successDto.UserId || idInquiry != successDto.InquiryId)
        {
            _logger.LogError(
                "[ERREUR] Problème ID - La mise à jour du succès avec l'id de l'utilisateur {id} a échouée.",
                idUser);
            return BadRequest();
        }

        if (!ModelState.IsValid)
        {
            _logger.LogError(
                "[ERREUR] Problème controlleur - La mise à jour du succès avec l'id de l'utilisateur {id} a échouée.",
                idUser);
            return BadRequest();
        }

        try
        {
            var s = _successDataService.UpdateSuccess(idUser, idInquiry, successDto);
            _logger.LogInformation(
                "[INFORMATION] La mise à jour du succès avec l'id de l'utilisateur {id} a été effectuée", idUser);
            return Ok(s);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }

    private IActionResult HandleError(Exception e)
    {
        switch (e.Message)
        {
            case { } msg when msg.Contains("userId"):
                _logger.LogError("[ERREUR] Impossible de trouver l'utilisateur pour la manipulation du succès");
                return NotFound("Impossible de trouver l'utilisateur pour la manipulation du succès");
            case { } msg when msg.Contains("inquiryId"):
                _logger.LogError("[ERREUR] Impossible de trouver l'enquête pour la manipulation du succès");
                return NotFound("Impossible de trouver l'enquête pour la manipulation du succès");
            case { } msg when msg.Contains("success"):
                _logger.LogError("[ERREUR] Impossible de manipuler le succès car il n'existe pas");
                return Conflict("Impossible de manipuler le succès car il n'existe pas");
            default:
                _logger.LogError("[ERREUR] Erreur inattendue, impossible de manipuler le succès");
                return BadRequest("Erreur inattendue, impossible de manipuler le succès");
        }
    }
}