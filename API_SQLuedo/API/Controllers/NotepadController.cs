using Asp.Versioning;
using Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]
[ApiVersion("1.0")]
[ApiController]
public class NotepadController : Controller
{
    private readonly INotepadService<NotepadDto> _notepadDataService;

    private readonly ILogger<NotepadController> _logger;

    public NotepadController(INotepadService<NotepadDto> notepadService, ILogger<NotepadController> logger)
    {
        _notepadDataService = notepadService;
        _logger = logger;
    }

    [HttpGet("notepad/{userId:int}/{inquiryId:int}")]
    [ProducesResponseType(typeof(NotepadDto), 200)]
    [ProducesResponseType(typeof(void), 404)]
    public IActionResult GetNotepadByUserAndInquiryById(int userId, int inquiryId)
    {
        try
        {
            _logger.LogInformation(
                "[INFORMATION] Le bloc-notes de l'utilisateur avec l'id {id} et l'enquête avec l'id {idInquiry} a été trouvé.",
                userId, inquiryId);
            return Ok(_notepadDataService.GetNotepadFromUserAndInquiryId(userId,inquiryId));
        }
        catch (ArgumentException)
        {
            _logger.LogError(
                "[ERREUR] Aucun bloc-notes trouvé pour l'utilisateur avec l'id {id} et l'enquête avec l'id {inquiryId}.",
                userId, inquiryId);
            return NotFound();
        }
    }
    
    [HttpPost("notepad")]
    [ProducesResponseType(typeof(void), 200)]
    [ProducesResponseType(typeof(void), 404)]
    public IActionResult SetNotepadByUserAndInquiryById([FromBody] NotepadDto notepad)
    {
        try
        {
            if (notepad.InquiryId < 0 || notepad.UserId < 0 || notepad.Notes == null)
            {
                return BadRequest();
            }
            _notepadDataService.SetNotepadFromUserAndInquiryId(notepad.UserId, notepad.InquiryId, notepad.Notes);
            _logger.LogInformation(
                "[INFORMATION] Le bloc-notes de l'utilisateur avec l'id {id} et l'enquête avec l'id {idInquiry} a été créé.",
                notepad.UserId, notepad.InquiryId);
            return Ok();
        }
        catch (ArgumentException)
        {
            _logger.LogError(
                "[ERREUR] Aucun bloc-notes n'a pu être créé pour l'utilisateur avec l'id {id} et l'enquête avec l'id {inquiryId}.",
                notepad.UserId, notepad.InquiryId);
            return NotFound();
        }
    }
    
    [HttpPut("notepad")]
    [ProducesResponseType(typeof(void), 203)]
    [ProducesResponseType(typeof(void), 404)]
    public IActionResult UpdateNotepadByUserAndInquiryById([FromBody] NotepadDto notepad)
    {
        try
        {
            if (notepad.InquiryId < 0 || notepad.UserId < 0 || notepad.Notes == null)
            {
                return BadRequest();
            }
            _notepadDataService.UpdateNotepadFromUserAndInquiryId(notepad.UserId, notepad.InquiryId, notepad.Notes);
            _logger.LogInformation(
                "[INFORMATION] Le bloc-notes de l'utilisateur avec l'id {id} et l'enquête avec l'id {idInquiry} a été mis à jour.",
                notepad.UserId, notepad.InquiryId);
            return Ok();
        }
        catch (ArgumentException)
        {
            _logger.LogError(
                "[ERREUR] Aucun bloc-notes n'a pu être mis à jour pour l'utilisateur avec l'id {id} et l'enquête avec l'id {inquiryId}.",
                notepad.UserId, notepad.InquiryId);
            return NotFound();
        }
    }
}