﻿using Dto;
using Asp.Versioning;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared;
using Model.OrderCriteria;

namespace API.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize]
    [ApiVersion("1.0")]
    [ApiController]
    public class QueryController(ILogger<QueryController> logger, IQueryService<QueryDto> queryService) : ControllerBase
    {
        [HttpPost("{database}/execute")]
        [ProducesResponseType(typeof(QueryDto), 200)]
        [ProducesResponseType(typeof(string), 204)]
        public IActionResult ExecuteQuery([FromBody]string query, string database)
        {
            var queryResult = queryService.ExecuteQuery(query, database);
            if (queryResult == null)
            {
                logger.LogError("[ERREUR] La requête n'a rien renvoyé.");
                return StatusCode(204);
            }

            logger.LogInformation("[INFORMATION] La requête a renvoyé : {result} ", queryResult);
            return Content(queryResult.Result, "application/json");
        }

        [HttpGet("{database}/Tables")]
        [ProducesResponseType(typeof(QueryDto), 200)]
        [ProducesResponseType(typeof(string), 204)]
        public IActionResult GetTables(string database)
        {
            var queryResult = queryService.GetTables(database);
            if (queryResult == null)
            {
                logger.LogError("[ERREUR] La requête n'a rien renvoyé.");
                return StatusCode(204);
            }

            logger.LogInformation("[INFORMATION] La requête a renvoyé : {result} ", queryResult);
            return Content(queryResult.Result, "application/json");
        }


        [HttpGet("{database}/{table}/Columns")]
        [ProducesResponseType(typeof(QueryDto), 200)]
        [ProducesResponseType(typeof(string), 204)]
        public IActionResult GetColumns(string database,string table)
        {
            var queryResult = queryService.GetColumns(database,table);
            if (queryResult == null)
            {
                logger.LogError("[ERREUR] La requête n'a rien renvoyé.");
                return StatusCode(204);
            }

            logger.LogInformation("[INFORMATION] La requête a renvoyé : {result} ", queryResult);
            return Content(queryResult.Result, "application/json");
        }
    }
}
