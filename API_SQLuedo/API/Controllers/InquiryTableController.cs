using Asp.Versioning;
using Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Shared;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]
[ApiVersion("1.0")]
[ApiController]
public class InquiryTableController : Controller
{
    private readonly IInquiryTableService<InquiryTableDto> _inquiryTableService;

    private readonly ILogger<InquiryTableController> _logger;

    public InquiryTableController(IInquiryTableService<InquiryTableDto> inquiryTableService, ILogger<InquiryTableController> logger)
    {
        _inquiryTableService = inquiryTableService;
        _logger = logger;
    }

    [HttpGet("database/{id:int}")]
    [ProducesResponseType(typeof(string), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetDatabaseNameByInquiryById(int id)
    {
        try
        {
            _logger.LogInformation("[INFORMATION] La base de données pour l'enquête avec l'id {id} a été trouvé.", id);
            return Ok(_inquiryTableService.GetDatabaseNameByInquiryId(id));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucune base de données trouvée pour l'enquête avec l'id {id}.", id);
            return NotFound();
        }
    }
}