using Dto;
using Asp.Versioning;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Shared;
using Model.OrderCriteria;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]
[ApiVersion("1.0")]
[ApiController]
public class UsersController(ILogger<UsersController> logger, IUserService<UserDto> userService) : ControllerBase
{
    [HttpGet("users/{page:int}/{number:int}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetUsers(int page, int number, UserOrderCriteria orderCriteria)
    {
        var users = userService.GetUsers(page, number, orderCriteria).ToList();
        if (users.Count == 0)
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé.");
            return StatusCode(204);
        }

        logger.LogInformation("[INFORMATION] {nb} Utilisateur(s) trouvé(s)", users.Count);
        return Ok(users);
    }

    [HttpGet("users/not-admin/{page:int}/{number:int}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetNotAdminUsers(int page, int number, UserOrderCriteria orderCriteria)
    {
        var users = userService.GetNotAdminUsers(page, number, orderCriteria).ToList();
        if (users.Count == 0)
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé.");
            return StatusCode(204);
        }

        logger.LogInformation("[INFORMATION] {nb} Utilisateur(s) trouvé(s)", users.Count);
        return Ok(users);
    }

    [HttpGet("users/number")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetNumberOfUsers()
    {
        var users = userService.GetNumberOfUsers();
        if (users == 0)
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé.");
            return StatusCode(204);
        }

        logger.LogInformation("[INFORMATION] {users} Utilisateur(s) trouvé(s)", users);
        return Ok(new KeyValuePair<string, int>("nbUsers", users));
    }

    [HttpGet("user/{id:int}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetUserById(int id)
    {
        try
        {
            logger.LogInformation("[INFORMATION] Utilisateur avec l'id {id} a été trouvé.", id);
            return Ok(userService.GetUserById(id));
        }
        catch (ArgumentException)
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpGet("user/{username:alpha}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetUserByUsername(string username)
    {
        try
        {
            logger.LogInformation("[INFORMATION] Utilisateur avec l'username {username} a été trouvé.", username);
            return Ok(userService.GetUserByUsername(username));
        }
        catch (ArgumentException)
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé avec l'username {username}.", username);
            return NotFound("Utilisateur non trouvé !");
        }
    }

    [HttpGet("user/email/{email}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetByEmail(string email)
    {
        try
        {
            logger.LogInformation("[INFORMATION] Utilisateur avec l'username {email} a été trouvé.", email);
            return Ok(userService.GetUserByEmail(email));
        }
        catch (ArgumentException)
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé avec l'username {email}.", email);
            return NotFound();
        }
    }

    [HttpDelete("user/{id:int}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult DeleteUser(int id)
    {
        var success = userService.DeleteUser(id);
        if (success)
        {
            logger.LogInformation("[INFORMATION] L'utilisateur avec l'id {id} a été supprimé.", id);
            return Ok();
        }
        else
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpDelete("user/username/{username:alpha}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult DeleteUserByUsername(string username)
    {
        var success = userService.DeleteUserByUsername(username);
        if (success)
        {
            logger.LogInformation("[INFORMATION] L'utilisateur avec le pseudo {username} a été supprimé.",
                username);
            return Ok();
        }
        else
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé avec le pseudo {username}.", username);
            return NotFound();
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(UserDto), 201)]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 409)]
    [ProducesResponseType(typeof(string), 410)]
    public IActionResult CreateUser([FromBody] UserDto dto)
    {
        if (dto.Username == null || dto.Password == null || dto.Email == null)
        {
            return BadRequest();
        }

        if (userService.IsEmailTaken(dto.Email))
        {
            return StatusCode(409, "Email déjà utilisé");
        }

        if (userService.IsUsernameTaken(dto.Username))
        {
            return StatusCode(410, "Username déjà utilisé");
        }

        // return Ok(userService.CreateUser(username, password, email, isAdmin));
        logger.LogInformation(
            "[INFORMATION] Un utilisateur a été créé : username - {username}, password - {password}, email - {email}, isAdmin - {isAdmin}",
            dto.Username, dto.Password, dto.Email, dto.IsAdmin);
        return Created(nameof(GetUsers),
            userService.CreateUser(dto.Username, dto.Password, dto.Email, dto.IsAdmin));
    }

    [HttpPut("user/{id:int}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 404)]
    [ProducesResponseType(typeof(string), 409)]
    public IActionResult UpdateUser(int id, [FromBody] UserDto userDto)
    {
        if (id != userDto.Id)
        {
            logger.LogError("[ERREUR] Problème ID - La mise à jour de l'utilisateur avec l'id {id} a échouée.", id);
            return BadRequest();
        }

        if (!ModelState.IsValid)
        {
            logger.LogError(
                "[ERREUR] Problème controller - La mise à jour de l'utilisateur avec l'id {id} a échouée.", id);
            return BadRequest();
        }

        if (userDto.Username == userService.GetUserById(id).Username && !userService.IsEmailTaken(userDto.Email)
            || (userDto.Email == userService.GetUserById(id).Email &&
                !userService.IsUsernameTaken(userDto.Username))
            || (!userService.IsEmailTaken(userDto.Email) && !userService.IsUsernameTaken(userDto.Username)))
        {
            logger.LogInformation("[INFORMATION] La mise à jour de l'utilisateur avec l'id {id} a été effectuée",
                id);
            return Ok(userService.UpdateUser(id, userDto));
        }

        logger.LogError("[ERREUR] Email ou nom d'utilisateur déjà utilisé");
        return StatusCode(409, "Email ou nom d'utilisateur déjà utilisé");
    }

    [HttpPut("user/promote/{id:int}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult PromoteUser(int id)
    {
        var userPromoted = userService.GetUserById(id);
        if (userPromoted != null)
        {
            userPromoted = userService.PromoteUser(id);
            logger.LogInformation("[INFORMATION] La promotion de l'utilisateur avec l'id {id} a été effectuée");
            return Ok(userPromoted);
        }
        else
        {
            logger.LogInformation("[INFORMATION] La promotion de l'utilisateur avec l'id {id} à échouée",
                id);
            return NotFound();
        }
    }
}