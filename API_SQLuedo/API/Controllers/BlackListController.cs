using Asp.Versioning;
using Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Model.OrderCriteria;
using Shared;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]
[ApiVersion("1.0")]
[ApiController]
public class BlackListController(ILogger<UsersController> logger, IBlackListService<BlackListDto> blackListService)
    : ControllerBase
{
    [HttpGet("user/ban/{page:int}/{number:int}")]
    [ProducesResponseType(typeof(IEnumerable<BlackListDto>), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetBannedUsers(int page, int number, BlackListOdrerCriteria orderCriteria)
    {
        var users = blackListService.GetBannedUsers(page, number, orderCriteria).ToList();
        if (users.Count == 0)
        {
            logger.LogError("[ERREUR] Aucun email banni trouvé.");
            return StatusCode(204);
        }

        logger.LogInformation("[INFORMATION] {nb} Email(s) banni(s) trouvé(s)", users.Count);
        return Ok(users);
    }

    [HttpGet("user/ban/number")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetNumberOfBannedUsers()
    {
        var nb = blackListService.GetNumberOfBannedUsers();
        logger.LogInformation("[INFORMATION] {nb} Email(s) banni(s) trouvé(s)", nb);
        return Ok(new KeyValuePair<string,int>("number",nb));
    }

    [HttpPost("user/ban")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetUserBannedByEmail([FromBody] string email)
    {
        var res = blackListService.GetUserBannedByEmail(email);
        if (res != null)
        {
            logger.LogInformation("[INFORMATION] Utilisateur banni avec l'email {email} a été trouvé.", email);
            return Ok(res);
        }

        logger.LogError("[ERREUR] Aucun utilisateur banni trouvé avec l'email {email}.", email);
        return NotFound("Utilisateur non trouvé !");
    }

    [HttpDelete("user/ban/{username:alpha}")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult BanUser(string username)
    {
        var success = blackListService.BanUser(username);
        if (success)
        {
            logger.LogInformation("[INFORMATION] L'utilisateur avec le pseudo {username} a été banni pour 2 ans.", username);
            return Ok(new KeyValuePair<string,bool>("success", true));
        }
        else
        {
            logger.LogError("[ERREUR] Aucun utilisateur trouvé avec le pseudo {username}.", username);
            return NotFound();
        }
    }

    [HttpPost("user/unban")]
    [ProducesResponseType(typeof(UserDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult UnbanUser([FromBody] string email)
    {
        var success = blackListService.UnbanUser(email);
        if (success)
        {
            logger.LogInformation("[INFORMATION] L'utilisateur avec l'email {email} a été débanni.", email);
            return Ok(new KeyValuePair<string,bool>("success", true));
        }
        else
        {
            logger.LogError("[ERREUR] Aucun utilisateur banni trouvé avec l'email {email}.", email);
            return NotFound();
        }
    }
}