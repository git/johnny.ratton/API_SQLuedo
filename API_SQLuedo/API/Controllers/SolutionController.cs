using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Dto;
using Model.OrderCriteria;
using Shared;
using Asp.Versioning;

namespace API.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize]
    [ApiVersion("1.0")]
    [ApiController]
    public class SolutionController : Controller
    {
        private readonly ISolutionService<SolutionDto> _solutionDataService;

        private readonly ILogger<SolutionController> _logger;

        public SolutionController(ISolutionService<SolutionDto> solutionDataService, ILogger<SolutionController> logger)
        {
            _solutionDataService = solutionDataService;
            _logger = logger;
        }

        [HttpGet("solution/{id:int}")]
        [ProducesResponseType(typeof(SolutionDto), 200)]
        [ProducesResponseType(typeof(string), 404)]
        public IActionResult GetSolutionByInquiryById(int id)
        {
            try
            {
                _logger.LogInformation("[INFORMATION] L'enquête avec l'id {id} a été trouvé.", id);
                return Ok(_solutionDataService.GetSolutionByInquiryId(id));
            }
            catch (ArgumentException)
            {
                _logger.LogError("[ERREUR] Aucune enquête trouvée avec l'id {id}.", id);
                return NotFound();
            }
        }
    }
}