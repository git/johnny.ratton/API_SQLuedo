﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Dto;
using Model.OrderCriteria;
using Shared;
using Asp.Versioning;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
        [Authorize]
        [ApiVersion("1.0")]
        [ApiController]
        public class LessonsController : Controller
{
    private readonly ILessonService<LessonDto> _lessonDataService;

    private readonly ILogger<LessonsController> _logger;

    public LessonsController(ILessonService<LessonDto> lessonDataService, ILogger<LessonsController> logger)
    {
        _lessonDataService = lessonDataService;
        _logger = logger;
    }

    [HttpGet("lessons/{page:int}/{number:int}/{orderCriteria}")]
    [ProducesResponseType(typeof(LessonDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetLessons(int page, int number, LessonOrderCriteria orderCriteria)
    {
        var nbLesson = _lessonDataService.GetLessons(page, number, orderCriteria).Count();
        if (nbLesson == 0)
        {
            _logger.LogError("[ERREUR] Aucune leçon trouvée.");
            return StatusCode(204);
        }

        _logger.LogInformation("[INFORMATION] {nb} Leçon(s) trouvée(s)", nbLesson);
        return Ok(_lessonDataService.GetLessons(page, number, orderCriteria));
    }
    
    [HttpGet("lessons/number")]
    [ProducesResponseType(typeof(LessonDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetNumberOfLessons()
    {
        var nbLesson = _lessonDataService.GetNumberOfLessons();
        if (nbLesson == 0)
        {
            _logger.LogError("[ERREUR] Aucune leçon trouvée.");
            return StatusCode(204);
        }

        _logger.LogInformation("[INFORMATION] {nb} Leçon(s) trouvée(s)", nbLesson);
        return Ok(new KeyValuePair<string,int>("nbLessons", nbLesson));
    }

    [HttpGet("lesson/{id:int}")]
    [ProducesResponseType(typeof(LessonDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetLessonById(int id)
    {
        try
        {
            _logger.LogInformation("[INFORMATION] La leçon avec l'id {id} a été trouvé.", id);
            return Ok(_lessonDataService.GetLessonById(id));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucune leçon trouvée avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpGet("lesson/{title:alpha}")]
    [ProducesResponseType(typeof(LessonDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetLessonByTitle(string title)
    {
        try
        {
            _logger.LogInformation("[INFORMATION] La leçon avec le titre {title} a été trouvé.", title);
            return Ok(_lessonDataService.GetLessonByTitle(title));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucune leçon trouvée avec le titre {title}.", title);
            return NotFound();
        }
    }

    [HttpDelete("lesson/{id:int}")]
    [ProducesResponseType(typeof(LessonDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult DeleteLesson(int id)
    {
        var success = _lessonDataService.DeleteLesson(id);
        if (success)
        {
            _logger.LogInformation("[INFORMATION] La leçon avec l'id {id} a été supprimé.", id);
            return Ok(_lessonDataService.DeleteLesson(id));
        }
        else
        {
            _logger.LogError("[ERREUR] Aucune leçon trouvée avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(LessonDto), 201)]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 500)]
    public IActionResult CreateLesson([FromBody] LessonDto dto)
    {
        if (dto.Title == null || dto.LastPublisher == null)
        {
            return BadRequest();
        }

        try
        {
            var createdLesson = _lessonDataService.CreateLesson(dto.Id, dto.Title, dto.LastPublisher,
                dto.LastEdit ?? DateOnly.FromDateTime(DateTime.Now));
            if (createdLesson != null)
            {
                _logger.LogInformation(
                    "[INFORMATION] Une leçon a été créé : title - {title}, lastPublisher - {publisher}, lastEdit - {lastEdit}",
                    dto.Title, dto.LastPublisher, dto.LastEdit);
                return Created(nameof(GetLessons), createdLesson);
            }
            else
            {
                return StatusCode(500);
            }
        }
        catch (ArgumentException e)
        {
            _logger.LogError("[ERREUR] " + e.Message);
            return Created();
        }
    }

    [HttpPut("lesson/{id:int}")]
    [ProducesResponseType(typeof(LessonDto), 200)]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult UpdateLesson(int id, [FromBody] LessonDto LessonDto)
    {
        if (id != LessonDto.Id)
        {
            _logger.LogError("[ERREUR] Problème ID - La mise à jour de la leçon avec l'id {id} a échouée.", id);
            return BadRequest();
        }

        if (!ModelState.IsValid)
        {
            _logger.LogError("[ERREUR] Problème controlleur - La mise à jour de la leçon avec l'id {id} a échouée.",
                id);
            return BadRequest();
        }

        if (LessonDto != null)
        {
            _logger.LogInformation("[INFORMATION] La mise à jour de la leçon avec l'id {id} a été effectuée", id);
            return Ok(_lessonDataService.UpdateLesson(id, LessonDto));
        }

        _logger.LogError("[ERREUR] Aucune leçon trouvée avec l'id {id}.", id);
        return NotFound();
    }
}