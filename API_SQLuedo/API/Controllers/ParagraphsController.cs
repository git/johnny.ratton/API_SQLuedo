﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Dto;
using Model.OrderCriteria;
using Shared;
using Asp.Versioning;

namespace API.Controllers;

[Route("api/v{version:apiVersion}/[controller]")]
[Authorize]
[ApiVersion("1.0")]
[ApiController]
public class ParagraphsController : Controller
{
    private readonly IParagraphService<ParagraphDto> _paragraphDataService;

    private readonly ILogger<ParagraphsController> _logger;

    public ParagraphsController(IParagraphService<ParagraphDto> paragraphDataService,
        ILogger<ParagraphsController> logger)
    {
        _paragraphDataService = paragraphDataService;
        _logger = logger;
    }

    [HttpGet("paragraphs/{page:int}/{number:int}/{orderCriteria}")]
    [ProducesResponseType(typeof(ParagraphDto), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetParagraphs(int page, int number, ParagraphOrderCriteria orderCriteria)
    {
        var nbParagraph = _paragraphDataService.GetParagraphs(page, number, orderCriteria).ToList().Count;
        if (nbParagraph == 0)
        {
            _logger.LogError("[ERREUR] Aucun paragraphe trouvé.");
            return StatusCode(204);
        }

        _logger.LogInformation("[INFORMATION] {nb} Paragraphe(s) trouvé(s)", nbParagraph);
        return Ok(_paragraphDataService.GetParagraphs(page, number, orderCriteria));
    }

    [HttpGet("paragraphs/lesson/{id:int}")]
    [ProducesResponseType(typeof(IEnumerable<ParagraphDto>), 200)]
    [ProducesResponseType(typeof(string), 204)]
    public IActionResult GetParagraphsByLessonId(int id)
    {
        var paragraphs = _paragraphDataService.GetParagraphsByLessonId(id);
        if (paragraphs.Count() == 0)
        {
            _logger.LogError("[ERREUR] Aucun paragraphe trouvé.");
            return StatusCode(204);
        }

        _logger.LogInformation("[INFORMATION] {nb} Paragraphe(s) trouvé(s)", paragraphs.Count());
        return Ok(paragraphs);
    }

    [HttpGet("paragraph/{id:int}")]
    [ProducesResponseType(typeof(ParagraphDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetParagraphById(int id)
    {
        try
        {
            _logger.LogInformation("[INFORMATION] Le paragraphe avec l'id {id} a été trouvé.", id);
            return Ok(_paragraphDataService.GetParagraphById(id));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucun paragraphe trouvé avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpGet("paragraph/{title:alpha}")]
    [ProducesResponseType(typeof(ParagraphDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult GetParagraphByTitle(string title)
    {
        try
        {
            _logger.LogInformation("[INFORMATION] Le paragraphe avec le titre {title} a été trouvé.", title);
            return Ok(_paragraphDataService.GetParagraphByTitle(title));
        }
        catch (ArgumentException)
        {
            _logger.LogError("[ERREUR] Aucun paragraphe trouvé avec le titre {title}.", title);
            return NotFound();
        }
    }

    [HttpDelete("paragraph/{id:int}")]
    [ProducesResponseType(typeof(ParagraphDto), 200)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult DeleteParagraph(int id)
    {
        var success = _paragraphDataService.DeleteParagraph(id);
        if (success)
        {
            _logger.LogInformation("[INFORMATION] Le paragraphe avec l'id {id} a été supprimé.", id);
            return Ok(_paragraphDataService.DeleteParagraph(id));
        }
        else
        {
            _logger.LogError("[ERREUR] Aucun paragraphe trouvé avec l'id {id}.", id);
            return NotFound();
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(ParagraphDto), 201)]
    [ProducesResponseType(typeof(string), 400)]
    public IActionResult CreateParagraph([FromBody] ParagraphDto dto)
    {
        if (dto.ContentTitle == null || dto.ContentContent == null || dto.Title == null || dto.Content == null ||
            dto.Info == null || dto.Query == null ||
            dto.Comment == null)
        {
            return BadRequest();
        }

        _logger.LogInformation(
            "[INFORMATION] Un paragraphe a été créé : title - {title}, content - {content}, info - {info}, query - {query}, comment - {comment}",
            dto.Title, dto.Content, dto.Info, dto.Query, dto.Comment);
        return Created(nameof(GetParagraphs),
            _paragraphDataService.CreateParagraph(dto.ContentTitle, dto.ContentContent, dto.Title, dto.Content,
                dto.Info, dto.Query, dto.Comment,
                dto.LessonId));
    }

    [HttpPut("paragraph/{id:int}")]
    [ProducesResponseType(typeof(ParagraphDto), 200)]
    [ProducesResponseType(typeof(string), 400)]
    [ProducesResponseType(typeof(string), 404)]
    public IActionResult UpdateParagraph(int id, [FromBody] ParagraphDto ParagraphDto)
    {
        if (id != ParagraphDto.Id)
        {
            _logger.LogError("[ERREUR] Problème ID - La mise à jour du paragraphe avec l'id {id} a échouée.", id);
            return BadRequest();
        }

        if (!ModelState.IsValid)
        {
            _logger.LogError(
                "[ERREUR] Problème controlleur - La mise à jour du paragraphe avec l'id {id} a échouée.", id);
            return BadRequest();
        }

        if (ParagraphDto != null)
        {
            _logger.LogInformation("[INFORMATION] La mise à jour du paragraphe avec l'id {id} a été effectuée", id);
            return Ok(_paragraphDataService.UpdateParagraph(id, ParagraphDto));
        }

        _logger.LogError("[ERREUR] Aucun paragraphe trouvé avec l'id {id}.", id);
        return NotFound();
    }
}