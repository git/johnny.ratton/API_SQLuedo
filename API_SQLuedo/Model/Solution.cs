﻿namespace Model;

public class Solution
{
    public int OwnerId { get; set; }
    public string MurdererFirstName { get; set; }
    public string MurdererLastName { get; set; }
    public string MurderPlace { get; set; }
    public string MurderWeapon { get; set; }
    public string Explaination { get; set; }

    public Solution()
    {
    }

    public Solution(int ownerId, string murdererFirstName, string murdererLastName, string murderPlace,
        string murderWeapon, string explaination)
    {
        OwnerId = ownerId;
        MurdererFirstName = murdererFirstName;
        MurdererLastName = murdererLastName;
        MurderPlace = murderPlace;
        MurderWeapon = murderWeapon;
        Explaination = explaination;
    }

    public Solution(string murdererFirstName, string murdererLastName, string murderPlace, string murderWeapon,
        string explaination)
    {
        MurdererFirstName = murdererFirstName;
        MurdererLastName = murdererLastName;
        MurderPlace = murderPlace;
        MurderWeapon = murderWeapon;
        Explaination = explaination;
    }
}