﻿namespace Model;

public class Paragraph : ContentLesson
{
    public string Info { get; set; }
    public string Query { get; set; }
    public string Comment { get; set; }

    public Paragraph(string title, string content, string info, string query, string comment) : base(content,
        title)
    {
        Info = info;
        Query = query;
        Comment = comment;
    }

    public Paragraph(int id, string title, string content, string info, string query, string comment, int lessonId) :
        base(id, content, title, lessonId)
    {
        Info = info;
        Query = query;
        Comment = comment;
    }
}