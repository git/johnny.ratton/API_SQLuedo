﻿namespace Model;

public class User
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
    public bool IsAdmin { get; set; }

    public User()
    {
    }

    public User(int id, string username, string password, string email, bool isAdmin)
    {
        Id = id;
        Username = username;
        Password = password;
        Email = email;
        IsAdmin = isAdmin;
    }
}