﻿namespace Model;

public class Lesson
{
    public int Id { get; }
    public string Title { get; set; }
    public string LastPublisher { get; set; }
    public DateOnly LastEdit { get; set; }
    public ICollection<ContentLesson> Content { get; set; } = new List<ContentLesson>();

    public Lesson(int id, string title, string lastPublisher, DateOnly lastEdit)
    {
        Id = id;
        Title = title;
        LastPublisher = lastPublisher;
        LastEdit = lastEdit;
    }

    public Lesson(string title, string lastPublisher, DateOnly lastEdit)
    {
        Title = title;
        LastPublisher = lastPublisher;
        LastEdit = lastEdit;
    }
}