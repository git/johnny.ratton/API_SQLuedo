﻿namespace Model;

public class InquiryTable
{
    public int OwnerId { get; set; }
    public string DatabaseName { get; set; }
    public string ConnectionInfo { get; set; }

    public InquiryTable()
    {
    }

    public InquiryTable(int inquiryId, string databaseName, string connectionInfo)
    {
        OwnerId = inquiryId;
        DatabaseName = databaseName;
        ConnectionInfo = connectionInfo;
    }
}