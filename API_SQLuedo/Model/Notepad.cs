﻿namespace Model;

public class Notepad
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public int InquiryId { get; set; }
    public string Notes { get; set; }

    public Notepad()
    {
    }

    public Notepad(int id, int userId, int inquiryId, string notes)
    {
        Id = id;
        UserId = userId;
        InquiryId = inquiryId;
        Notes = notes;
    }

    public Notepad(int userId, int inquiryId, string notes)
    {
        UserId = userId;
        InquiryId = inquiryId;
        Notes = notes;
    }
}