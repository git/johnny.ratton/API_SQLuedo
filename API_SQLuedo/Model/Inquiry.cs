﻿namespace Model;

public class Inquiry
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public bool IsUser { get; set; }

    public Inquiry()
    {
    }

    public Inquiry(int id, string title, string description, bool isUser)
    {
        Id = id;
        Title = title;
        Description = description;
        IsUser = isUser;
    }
}