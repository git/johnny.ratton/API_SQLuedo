﻿namespace Model;

public class Success
{
    public int UserId { get; set; }
    public int InquiryId { get; set; }
    public bool IsFinished { get; set; }

    public Success()
    {
    }

    public Success(int userId, int inquiryId, bool isFinished)
    {
        UserId = userId;
        InquiryId = inquiryId;
        IsFinished = isFinished;
    }
}