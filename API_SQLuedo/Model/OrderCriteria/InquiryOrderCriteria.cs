namespace Model.OrderCriteria;

public enum InquiryOrderCriteria
{
    None,
    ByTitle,
    ByDescription,
    ByIsUser,
    ById
}