namespace Model.OrderCriteria;

public enum LessonOrderCriteria
{
    None,
    ByTitle,
    ByLastPublisher,
    ByLastEdit,
    ById
}