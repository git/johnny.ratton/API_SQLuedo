namespace Model.OrderCriteria;

public enum SuccessOrderCriteria
{
    None,
    ByUserId,
    ByInquiryId,
    ByIsFinished
}