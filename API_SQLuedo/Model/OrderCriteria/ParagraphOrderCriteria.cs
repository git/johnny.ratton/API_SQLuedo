namespace Model.OrderCriteria;

public enum ParagraphOrderCriteria
{
    None,
    ByTitle,
    ByContent,
    ByInfo,
    ByQuery,
    ByComment
}