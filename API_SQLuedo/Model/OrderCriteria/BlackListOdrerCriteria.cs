namespace Model.OrderCriteria;

public enum BlackListOdrerCriteria
{
    None, ByEmail, ByExpirationDate
}