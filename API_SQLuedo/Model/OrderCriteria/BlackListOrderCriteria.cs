namespace Model.OrderCriteria;

public enum UserOrderCriteria
{
    None,
    ById,
    ByUsername,
    ByEmail,
    ByIsAdmin
}