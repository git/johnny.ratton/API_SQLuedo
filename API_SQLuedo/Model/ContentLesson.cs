﻿namespace Model;

public abstract class ContentLesson
{
    public int Id { get; set; }
    public string ContentContent { get; set; }
    public string ContentTitle { get; set; }

    public int LessonId { get; set; }

    protected ContentLesson(string contentContent, string contentTitle)
    {
        ContentContent = contentContent;
        ContentTitle = contentTitle;
    }

    protected ContentLesson(int id, string contentContent, string contentTitle, int lessonId)
    {
        Id = id;
        ContentContent = contentContent;
        ContentTitle = contentTitle;
        LessonId = lessonId;
    }
}