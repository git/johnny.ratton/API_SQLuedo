﻿namespace Model;

public class BlackList
{
    public string Email { get; set; }
    public DateOnly ExpirationDate { get; set; }

    public BlackList(string email, DateOnly expirationDate)
    {
        if (email is null or "")
            throw new ArgumentException("Email cannot be null or empty");
        ArgumentOutOfRangeException.ThrowIfLessThan(expirationDate, DateOnly.FromDateTime(DateTime.Now));
        Email = email;
        ExpirationDate = expirationDate;
    }
}