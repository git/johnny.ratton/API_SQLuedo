using Model;

namespace TestEF.Model;

public class ParagraphTest
{
    [Fact]
    void TestConstructorWithoutId()
    {
        Paragraph paragraph = new Paragraph("Title", "Content","Info","Query","Comment");
        Assert.Equal(0, paragraph.Id);
        Assert.Equal(0, paragraph.LessonId);
        Assert.Equal("Title", paragraph.ContentTitle);
        Assert.Equal("Content", paragraph.ContentContent);
        Assert.Equal("Info", paragraph.Info);
        Assert.Equal("Query", paragraph.Query);
        Assert.Equal("Comment", paragraph.Comment);
    }
    
    [Fact]
    void TestConstructorWithId()
    {
        Paragraph paragraph = new Paragraph(42,"Title", "Content","Info","Query","Comment",42);
        Assert.Equal(42, paragraph.Id);
        Assert.Equal(42, paragraph.LessonId);
        Assert.Equal("Title", paragraph.ContentTitle);
        Assert.Equal("Content", paragraph.ContentContent);
        Assert.Equal("Info", paragraph.Info);
        Assert.Equal("Query", paragraph.Query);
        Assert.Equal("Comment", paragraph.Comment);
    }
}