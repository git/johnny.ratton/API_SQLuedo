using Model;

namespace TestEF.Model;

public class SuccessTest
{
    [Fact]
    void TestEmptyConstructor()
    {
        Success success = new Success();
        Assert.Equal(0,success.UserId);
        Assert.Equal(0,success.InquiryId);
        Assert.False(success.IsFinished);
    }
    
    [Fact]
    void TestFullConstructor()
    {
        Success success = new Success(42,42,true);
        Assert.Equal(42,success.UserId);
        Assert.Equal(42,success.InquiryId);
        Assert.True(success.IsFinished);
    }
}