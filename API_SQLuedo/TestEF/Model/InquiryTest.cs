using Model;

namespace TestEF.Model;

public class InquiryTest
{
    [Fact]
    void TestEmptyContructor()
    {
        Inquiry inquiry = new Inquiry();
        Assert.Equal(0,inquiry.Id);
        Assert.Null(inquiry.Title);
        Assert.Null(inquiry.Description);
        Assert.False(inquiry.IsUser);
    }
    
    [Fact]
    void TestFullContructor()
    {
        Inquiry inquiry = new Inquiry(1,"Title","Description",true);
        Assert.Equal(1,inquiry.Id);
        Assert.Equal("Title",inquiry.Title);
        Assert.Equal("Description",inquiry.Description);
        Assert.True(inquiry.IsUser);
    }
}