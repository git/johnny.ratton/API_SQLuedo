using Model;

namespace TestEF.Model;

public class InquiryTableTest
{
    [Fact]
    void TestEmptyContructor()
    {
        InquiryTable inquiry = new InquiryTable();
        Assert.Equal(0,inquiry.OwnerId);
        Assert.Null(inquiry.ConnectionInfo);
        Assert.Null(inquiry.DatabaseName);
    }
    
    [Fact]
    void TestFullContructor()
    {
        InquiryTable inquiry = new InquiryTable(1,"Database","Connection");
        Assert.Equal(1,inquiry.OwnerId);
        Assert.Equal("Connection",inquiry.ConnectionInfo);
        Assert.Equal("Database",inquiry.DatabaseName);
    }
}