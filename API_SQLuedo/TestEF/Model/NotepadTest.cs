using Model;

namespace TestEF.Model;

public class NotepadTest
{
    [Fact]
    void TestEmptyConstructor()
    {
        Notepad notepad = new Notepad();
        Assert.Equal(0, notepad.Id);
        Assert.Equal(0, notepad.UserId);
        Assert.Equal(0, notepad.InquiryId);
        Assert.Null(notepad.Notes);
    }
    
    [Fact]
    void TestConstructorWithoutId()
    {
        Notepad notepad = new Notepad(42,42,"Notes");
        Assert.Equal(0, notepad.Id);
        Assert.Equal(42, notepad.UserId);
        Assert.Equal(42, notepad.InquiryId);
        Assert.Equal("Notes",notepad.Notes);
    }
    
    [Fact]
    void TestConstructorWithId()
    {
        Notepad notepad = new Notepad(42,42,42,"Notes");
        Assert.Equal(42, notepad.Id);
        Assert.Equal(42, notepad.UserId);
        Assert.Equal(42, notepad.InquiryId);
        Assert.Equal("Notes",notepad.Notes);
    }
}