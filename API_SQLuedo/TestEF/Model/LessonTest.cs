using Model;

namespace TestEF.Model;

public class LessonTest
{
    [Fact]
    void TestConstructorWithoutId()
    {
        Lesson lesson = new Lesson("Title", "JohnDoe", DateOnly.FromDateTime(DateTime.Now));
        Assert.Equal(0, lesson.Id);
        Assert.Equal("Title", lesson.Title);
        Assert.Equal("JohnDoe", lesson.LastPublisher);
        Assert.Equal(DateOnly.FromDateTime(DateTime.Now), lesson.LastEdit);
    }
    
    [Fact]
    void TestConstructorWithId()
    {
        Lesson lesson = new Lesson(42,"Title", "JohnDoe", DateOnly.FromDateTime(DateTime.Now));
        Assert.Equal(42, lesson.Id);
        Assert.Equal("Title", lesson.Title);
        Assert.Equal("JohnDoe", lesson.LastPublisher);
        Assert.Equal(DateOnly.FromDateTime(DateTime.Now), lesson.LastEdit);
    }
}