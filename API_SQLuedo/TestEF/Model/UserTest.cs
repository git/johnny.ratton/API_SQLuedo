using Model;

namespace TestEF.Model;

public class UserTest
{
    [Fact]
    void TestEmptyConstructor()
    {
        User user = new User();
        Assert.Equal(0,user.Id);
        Assert.Null(user.Username);
        Assert.Null(user.Email);
        Assert.Null(user.Password);
        Assert.False(user.IsAdmin);
    }
    
    [Fact]
    void TestFullConstructor()
    {
        User user = new User(42,"Username","Password","Email",true);
        Assert.Equal(42,user.Id);
        Assert.Equal("Username",user.Username);
        Assert.Equal("Email",user.Email);
        Assert.Equal("Password",user.Password);
        Assert.True(user.IsAdmin);
    }
}