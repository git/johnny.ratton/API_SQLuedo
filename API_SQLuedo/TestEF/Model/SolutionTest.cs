using Model;

namespace TestEF.Model;

public class SolutionTest
{
    [Fact]
    void TestEmptyConstructor()
    {
        Solution solution = new Solution();
        Assert.Equal(0,solution.OwnerId);
        Assert.Null(solution.MurdererFirstName);
        Assert.Null(solution.MurdererLastName);
        Assert.Null(solution.MurderPlace);
        Assert.Null(solution.MurderWeapon);
        Assert.Null(solution.Explaination);
    }
    
    [Fact]
    void TestConstructorWithoutId()
    {
        Solution solution = new Solution("John","Doe","Bedroom","Knife","Because");
        Assert.Equal(0,solution.OwnerId);
        Assert.Equal("John",solution.MurdererFirstName);
        Assert.Equal("Doe",solution.MurdererLastName);
        Assert.Equal("Bedroom",solution.MurderPlace);
        Assert.Equal("Knife",solution.MurderWeapon);
        Assert.Equal("Because",solution.Explaination);
    }
    
    [Fact]
    void TestConstructorWithId()
    {
        Solution solution = new Solution(42,"John","Doe","Bedroom","Knife","Because");
        Assert.Equal(42,solution.OwnerId);
        Assert.Equal("John",solution.MurdererFirstName);
        Assert.Equal("Doe",solution.MurdererLastName);
        Assert.Equal("Bedroom",solution.MurderPlace);
        Assert.Equal("Knife",solution.MurderWeapon);
        Assert.Equal("Because",solution.Explaination);
    }
}