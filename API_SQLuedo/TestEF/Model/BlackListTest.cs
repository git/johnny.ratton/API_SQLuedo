using Model;

namespace TestEF.Model;

public class BlackListTest
{
    [Fact]
    void TestConstructorWithRightParameters()
    {
        BlackList blackList = new BlackList("example@email.com", DateOnly.FromDateTime(DateTime.Now));
        Assert.Equal("example@email.com", blackList.Email);
        Assert.Equal(DateOnly.FromDateTime(DateTime.Now), blackList.ExpirationDate);
    }
    
    [Fact]
    void TestConstructorWithNullEmail()
    {
        Assert.Throws<ArgumentException>(() =>
        {
            BlackList blackList = new BlackList(null, DateOnly.FromDateTime(DateTime.Now));
        });
    }
    
    [Fact]
    void TestConstructorWithAnteriorDate()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() =>
        {
            BlackList blackList = new BlackList("example@email.com", DateOnly.FromDateTime(DateTime.Parse("2024/01/01 00:00:00")));
        });
    }
}