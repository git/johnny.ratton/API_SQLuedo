using Entities;

namespace TestEF.EntitiesTests;

public class TestSuccessEntity
{
    private const int UserId = 42;
    private const int InquiryId = 7;
    private const bool IsFinished = true;

    [Fact]
    public void TestDefaultConstructor()
    {
        SuccessEntity success = new SuccessEntity();
        Assert.Equal(0, success.UserId);
        Assert.Null(success.User);
        Assert.Equal(0, success.InquiryId);
        Assert.Null(success.Inquiry);
        Assert.False(success.IsFinished);
    }

    [Fact]
    public void TestConstructorWithIds()
    {
        SuccessEntity success = new SuccessEntity
        {
            UserId = UserId,
            InquiryId = InquiryId,
            IsFinished = IsFinished
        };
        Assert.Equal(UserId, success.UserId);
        Assert.Null(success.User);
        Assert.Equal(InquiryId, success.InquiryId);
        Assert.Null(success.Inquiry);
        Assert.True(success.IsFinished);
    }

    [Fact]
    public void TestConstructorWithNavigationProperties()
    {
        UserEntity user = new UserEntity();
        InquiryEntity inquiry = new InquiryEntity();
        SuccessEntity success = new SuccessEntity
        {
            User = user,
            Inquiry = inquiry,
            IsFinished = IsFinished
        };
        Assert.Equal(0, success.UserId);
        Assert.NotNull(success.User);
        Assert.Equal(0, success.InquiryId);
        Assert.NotNull(success.Inquiry);
        Assert.True(success.IsFinished);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        UserEntity user = new UserEntity();
        InquiryEntity inquiry = new InquiryEntity();
        SuccessEntity success = new SuccessEntity
        {
            UserId = UserId,
            User = user,
            InquiryId = InquiryId,
            Inquiry = inquiry,
            IsFinished = IsFinished
        };
        Assert.Equal(UserId, success.UserId);
        Assert.NotNull(success.User);
        Assert.Equal(InquiryId, success.InquiryId);
        Assert.NotNull(success.Inquiry);
        Assert.True(success.IsFinished);
    }
}