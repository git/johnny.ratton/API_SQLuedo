using Entities;

namespace TestEF.EntitiesTests;

public class TestUserEntity
{
    private const string Username = "username";
    private const string Email = "example@email.com";
    private const string Password = "password";
    private const bool IsAdmin = true;
    private const int Id = 42;

    [Fact]
    public void TestDefaultConstructor()
    {
        UserEntity user = new UserEntity();
        Assert.Equal(0, user.Id);
        Assert.Null(user.Username);
        Assert.Null(user.Email);
        Assert.Null(user.Password);
        Assert.False(user.IsAdmin);
    }

    [Fact]
    public void TestConstructorWithOnlyId()
    {
        UserEntity user = new UserEntity { Id = Id };
        Assert.Equal(Id, user.Id);
        Assert.Null(user.Username);
        Assert.Null(user.Email);
        Assert.Null(user.Password);
        Assert.False(user.IsAdmin);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        UserEntity user = new UserEntity
        {
            Username = Username,
            Password = Password,
            Email = Email,
            IsAdmin = IsAdmin
        };
        Assert.Equal(0, user.Id);
        Assert.Equal(Username, user.Username);
        Assert.Equal(Email, user.Email);
        Assert.Equal(Password, user.Password);
        Assert.True(user.IsAdmin);
    }

    [Fact]
    public void TestConstructorWithoutAllAttributes()
    {
        UserEntity user = new UserEntity
        {
            Id = Id,
            Username = Username,
            Password = Password,
            Email = Email,
            IsAdmin = IsAdmin
        };
        Assert.Equal(Id, user.Id);
        Assert.Equal(Username, user.Username);
        Assert.Equal(Email, user.Email);
        Assert.Equal(Password, user.Password);
        Assert.True(user.IsAdmin);
    }
}