using Entities;

namespace TestEF.EntitiesTests;

public class TestInquiryEntity
{
    private const int Id = 42;
    private const string Title = "Title";
    private const string Description = "_description";
    private const bool IsUser = true;

    [Fact]
    public void TestDefaultConstructor()
    {
        var inquiry = new InquiryEntity();
        Assert.Equal(0, inquiry.Id);
        Assert.Null(inquiry.Title);
        Assert.Null(inquiry.Description);
        Assert.False(inquiry.IsUser);
    }

    [Fact]
    public void TestConstructorWithOnlyId()
    {
        var inquiry = new InquiryEntity
        {
            Id = Id
        };
        Assert.Equal(Id, inquiry.Id);
        Assert.Null(inquiry.Title);
        Assert.Null(inquiry.Description);
        Assert.False(inquiry.IsUser);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        var inquiry = new InquiryEntity
        {
            Title = Title,
            Description = Description,
            IsUser = IsUser
        };
        Assert.Equal(0, inquiry.Id);
        Assert.Equal(Title, inquiry.Title);
        Assert.Equal(Description, inquiry.Description);
        Assert.True(inquiry.IsUser);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        var inquiry = new InquiryEntity
        {
            Id = Id,
            Title = Title,
            Description = Description,
            IsUser = IsUser
        };
        Assert.Equal(Id, inquiry.Id);
        Assert.Equal(Title, inquiry.Title);
        Assert.Equal(Description, inquiry.Description);
        Assert.True(inquiry.IsUser);
    }
}