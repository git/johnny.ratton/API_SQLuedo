using Entities;

namespace TestEF.EntitiesTests;

public class TestSolutionEntity
{
    private const string MurdererFirstName = "John";
    private const string MurdererLastName = "Doe";
    private const string MurderPlace = "WhiteHouse";
    private const string MurderWeapon = "Nuclear Bomb";
    private const string Explaination = "This is an explaination";
    private const int InquiryId = 42;
    private static readonly InquiryEntity Inquiry = new();

    [Fact]
    public void TestDefaultConstructor()
    {
        SolutionEntity solution = new SolutionEntity();
        Assert.Equal(0, solution.OwnerId);
        Assert.Null(solution.Owner);
        Assert.Null(solution.MurdererFirstName);
        Assert.Null(solution.MurdererLastName);
        Assert.Null(solution.MurderPlace);
        Assert.Null(solution.MurderWeapon);
        Assert.Null(solution.Explaination);
    }

    [Fact]
    public void TestConstructorWithOnlyId()
    {
        SolutionEntity solution = new SolutionEntity
        {
            OwnerId = InquiryId
        };
        Assert.Equal(InquiryId, solution.OwnerId);
        Assert.Null(solution.Owner);
        Assert.Null(solution.MurdererFirstName);
        Assert.Null(solution.MurdererLastName);
        Assert.Null(solution.MurderPlace);
        Assert.Null(solution.MurderWeapon);
        Assert.Null(solution.Explaination);
    }

    [Fact]
    public void TestConstructorWithoutOwnerId()
    {
        SolutionEntity solution = new SolutionEntity
        {
            Owner = Inquiry,
            MurdererFirstName = MurdererFirstName,
            MurdererLastName = MurdererLastName,
            MurderPlace = MurderPlace,
            MurderWeapon = MurderWeapon,
            Explaination = Explaination
        };

        Assert.Equal(0, solution.OwnerId);
        Assert.Equal(Inquiry, solution.Owner);
        Assert.Equal(MurdererFirstName, solution.MurdererFirstName);
        Assert.Equal(MurdererLastName, solution.MurdererLastName);
        Assert.Equal(MurderPlace, solution.MurderPlace);
        Assert.Equal(MurderWeapon, solution.MurderWeapon);
        Assert.Equal(Explaination, solution.Explaination);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        SolutionEntity solution = new SolutionEntity
        {
            OwnerId = InquiryId,
            Owner = Inquiry,
            MurdererFirstName = MurdererFirstName,
            MurdererLastName = MurdererLastName,
            MurderPlace = MurderPlace,
            MurderWeapon = MurderWeapon,
            Explaination = Explaination
        };

        Assert.Equal(InquiryId, solution.OwnerId);
        Assert.Equal(Inquiry, solution.Owner);
        Assert.Equal(MurdererFirstName, solution.MurdererFirstName);
        Assert.Equal(MurdererLastName, solution.MurdererLastName);
        Assert.Equal(MurderPlace, solution.MurderPlace);
        Assert.Equal(MurderWeapon, solution.MurderWeapon);
        Assert.Equal(Explaination, solution.Explaination);
    }

    [Fact]
    public void Constructor_ShouldSetProperties_WhenCalledWithSixParameters()
    {
        // Arrange
        const int ownerId = 1;
        const string murdererFirstName = "John";
        const string murdererLastName = "Doe";
        const string murderPlace = "WhiteHouse";
        const string murderWeapon = "Nuclear Bomb";
        const string explanation = "This is an explanation";

        // Act
        var solutionEntity = new SolutionEntity
        {
            OwnerId = ownerId,
            MurdererFirstName = murdererFirstName,
            MurdererLastName = murdererLastName,
            MurderPlace = murderPlace,
            MurderWeapon = murderWeapon,
            Explaination = explanation
        };

        // Assert
        Assert.Equal(ownerId, solutionEntity.OwnerId);
        Assert.Equal(murdererFirstName, solutionEntity.MurdererFirstName);
        Assert.Equal(murdererLastName, solutionEntity.MurdererLastName);
        Assert.Equal(murderPlace, solutionEntity.MurderPlace);
        Assert.Equal(murderWeapon, solutionEntity.MurderWeapon);
        Assert.Equal(explanation, solutionEntity.Explaination);
    }
}