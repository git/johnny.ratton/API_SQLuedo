using Entities;

namespace TestEF.EntitiesTests;

public class TestNotepadEntity
{
    private const int Id = 42;
    private const int UserId = 42;
    private static readonly UserEntity UserEntity = new();
    private const int InquiryId = 42;
    private static readonly InquiryEntity InquiryEntity = new();
    private const string Notes = "This is some notes example";

    [Fact]
    public void TestDefaultConstructor()
    {
        NotepadEntity notepad = new NotepadEntity();
        Assert.Equal(0, notepad.Id);
        Assert.Equal(0, notepad.UserId);
        Assert.Null(notepad.User);
        Assert.Equal(0, notepad.InquiryId);
        Assert.Null(notepad.Inquiry);
        Assert.Null(notepad.Notes);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        NotepadEntity notepad = new NotepadEntity
        {
            UserId = UserId,
            User = UserEntity,
            InquiryId = InquiryId,
            Inquiry = InquiryEntity,
            Notes = Notes
        };
        Assert.Equal(0, notepad.Id);
        Assert.Equal(UserId, notepad.UserId);
        Assert.Equal(UserEntity, notepad.User);
        Assert.Equal(InquiryId, notepad.InquiryId);
        Assert.Equal(InquiryEntity, notepad.Inquiry);
        Assert.Equal(Notes, notepad.Notes);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        NotepadEntity notepad = new NotepadEntity
        {
            Id = Id,
            UserId = UserId,
            User = UserEntity,
            InquiryId = InquiryId,
            Inquiry = InquiryEntity,
            Notes = Notes
        };
        Assert.Equal(Id, notepad.Id);
        Assert.Equal(UserId, notepad.UserId);
        Assert.Equal(UserEntity, notepad.User);
        Assert.Equal(InquiryId, notepad.InquiryId);
        Assert.Equal(InquiryEntity, notepad.Inquiry);
        Assert.Equal(Notes, notepad.Notes);
    }
}