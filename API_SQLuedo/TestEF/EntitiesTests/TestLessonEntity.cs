using Entities;

namespace TestEF.EntitiesTests;

public class TestLessonEntity
{
    private const int Id = 42;
    private const string Title = "Title";
    private const string LastPublisher = "Last Publisher";
    private static readonly DateOnly LastEdit = new();

    [Fact]
    public void TestDefaultConstructor()
    {
        LessonEntity lesson = new LessonEntity();
        Assert.Equal(0, lesson.Id);
        Assert.Null(lesson.Title);
        Assert.Null(lesson.LastPublisher);
        Assert.Equal(new DateOnly(0001, 01, 01), LastEdit);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        LessonEntity lesson = new LessonEntity
        {
            Title = Title,
            LastPublisher = LastPublisher,
            LastEdit = LastEdit
        };
        Assert.Equal(0, lesson.Id);
        Assert.Equal(Title, lesson.Title);
        Assert.Equal(LastPublisher, lesson.LastPublisher);
        Assert.Equal(LastEdit, lesson.LastEdit);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        LessonEntity lesson = new LessonEntity
        {
            Id = Id,
            Title = Title,
            LastPublisher = LastPublisher,
            LastEdit = LastEdit
        };
        Assert.Equal(Id, lesson.Id);
        Assert.Equal(Title, lesson.Title);
        Assert.Equal(LastPublisher, lesson.LastPublisher);
        Assert.Equal(LastEdit, lesson.LastEdit);
    }
}