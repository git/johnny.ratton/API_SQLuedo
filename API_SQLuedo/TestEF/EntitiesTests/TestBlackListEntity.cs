﻿using Model;

namespace TestEF.EntitiesTests;

public class TestBlackListEntity
{
    [Fact]
    public void Constructor_ShouldSetProperties_WhenCalledWithTwoParameters()
    {
        // Arrange
        const string email = "test@example.com";
        DateOnly expirationDate = DateOnly.FromDateTime(DateTime.Now.AddDays(10));

        // Act
        var blackList = new BlackList(email, expirationDate);

        // Assert
        Assert.Equal(email, blackList.Email);
        Assert.Equal(expirationDate, blackList.ExpirationDate);
    }

    [Fact]
    public void Constructor_ShouldThrowArgumentException_WhenEmailIsEmpty()
    {
        // Arrange
        string email = string.Empty;
        DateOnly expirationDate = DateOnly.FromDateTime(DateTime.Now.AddDays(10));

        // Act & Assert
        Assert.Throws<ArgumentException>(() => new BlackList(email, expirationDate));
    }

    [Fact]
    public void Constructor_ShouldThrowArgumentException_WhenEmailIsNull()
    {
        // Arrange
        string email = null;
        DateOnly expirationDate = DateOnly.FromDateTime(DateTime.Now.AddDays(10));

        // Act & Assert
        Assert.Throws<ArgumentException>(() => new BlackList(email, expirationDate));
    }

    [Fact]
    public void Constructor_ShouldThrowArgumentOutOfRangeException_WhenExpirationDateIsInThePast()
    {
        // Arrange
        string email = "test@example.com";
        DateOnly expirationDate = DateOnly.FromDateTime(DateTime.Now.AddDays(-1));

        // Act & Assert
        Assert.Throws<ArgumentOutOfRangeException>(() => new BlackList(email, expirationDate));
    }
}