using Entities;

namespace TestEF.EntitiesTests;

public class TestInquiryTableEntity
{
    private const int InquiryId = 42;
    private static readonly InquiryEntity Inquiry = new();
    private const string DatabaseName = "Database";
    private const string ConnectionInfo = "Connection";

    [Fact]
    public void TestDefaultConstructor()
    {
        InquiryTableEntity tableEntity = new InquiryTableEntity();
        Assert.Equal(0, tableEntity.OwnerId);
        Assert.Null(tableEntity.Owner);
        Assert.Null(tableEntity.DatabaseName);
        Assert.Null(tableEntity.ConnectionInfo);
    }

    [Fact]
    public void TestConstructorWithOnlyId()
    {
        InquiryTableEntity tableEntity = new InquiryTableEntity
        {
            OwnerId = InquiryId
        };
        Assert.Equal(InquiryId, tableEntity.OwnerId);
        Assert.Null(tableEntity.Owner);
        Assert.Null(tableEntity.DatabaseName);
        Assert.Null(tableEntity.ConnectionInfo);
    }


    [Fact]
    public void TestConstructorWithoutId()
    {
        InquiryTableEntity tableEntity = new InquiryTableEntity
        {
            Owner = Inquiry,
            DatabaseName = DatabaseName,
            ConnectionInfo = ConnectionInfo
        };
        Assert.Equal(0, tableEntity.OwnerId);
        Assert.Equal(Inquiry, tableEntity.Owner);
        Assert.Equal(DatabaseName, tableEntity.DatabaseName);
        Assert.Equal(ConnectionInfo, tableEntity.ConnectionInfo);
    }

    [Fact]
    public void TestConstructorWithoutNavigationProperty()
    {
        InquiryTableEntity tableEntity = new InquiryTableEntity
        {
            OwnerId = InquiryId,
            DatabaseName = DatabaseName,
            ConnectionInfo = ConnectionInfo
        };
        Assert.Equal(InquiryId, tableEntity.OwnerId);
        Assert.Null(tableEntity.Owner);
        Assert.Equal(DatabaseName, tableEntity.DatabaseName);
        Assert.Equal(ConnectionInfo, tableEntity.ConnectionInfo);
    }
}