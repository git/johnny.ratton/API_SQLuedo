using Entities;

namespace TestEF.EntitiesTests;

public class TestParagraphEntity
{
    private const int Id = 42;
    private const string Title = "Title";
    private const string Info = "Info";
    private const string Content = "Content";
    private const string Query = "Query";
    private const string Comment = "Comment";

    [Fact]
    public void TestDefaultConstructor()
    {
        var paragraph = new ParagraphEntity();
        Assert.Equal(0, paragraph.Id);
        Assert.Null(paragraph.Title);
        Assert.Null(paragraph.Info);
        Assert.Null(paragraph.Content);
        Assert.Null(paragraph.Query);
        Assert.Null(paragraph.Comment);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        var paragraph = new ParagraphEntity
        {
            Title = Title,
            Content = Content,
            Info = Info,
            Query = Query,
            Comment = Comment
        };
        Assert.Equal(0, paragraph.Id);
        Assert.Equal(Title, paragraph.Title);
        Assert.Equal(Info, paragraph.Info);
        Assert.Equal(Content, paragraph.Content);
        Assert.Equal(Query, paragraph.Query);
        Assert.Equal(Comment, paragraph.Comment);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        var paragraph = new ParagraphEntity
        {
            Id = Id,
            Title = Title,
            Content = Content,
            Info = Info,
            Query = Query,
            Comment = Comment,
            LessonId = 10
        };
        Assert.Equal(Id, paragraph.Id);
        Assert.Equal(Title, paragraph.Title);
        Assert.Equal(Info, paragraph.Info);
        Assert.Equal(Content, paragraph.Content);
        Assert.Equal(Query, paragraph.Query);
        Assert.Equal(Comment, paragraph.Comment);
        Assert.Equal(10, paragraph.LessonId);
    }
}