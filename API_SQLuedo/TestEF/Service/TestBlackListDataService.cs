using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;

namespace TestEF.Service;

public class TestBlackListDataService
{
    private readonly UserDbContext _dbContext;
    private readonly BlackListDataService _blackListDataService;

    public TestBlackListDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _blackListDataService = new BlackListDataService(_dbContext);
    }
    [Fact]
    public void BanUser_Success()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test101", Email = "example101@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test102", Email = "example102@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test103", Email = "example103@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.BanUser("Test101");
        Assert.True(banResult);
    }
    
    [Fact]
    public void GetNbBannedUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test61", Email = "example61@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test62", Email = "example62@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test63", Email = "example63@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var banResult1 = _blackListDataService.BanUser("Test61");
        var banResult2 = _blackListDataService.BanUser("Test62");
        var banResult3 = _blackListDataService.BanUser("Test63");
        Assert.True(banResult1);
        Assert.True(banResult2);
        Assert.True(banResult3);
        Assert.Equal(3, _dbContext.BlackLists.Count());
    }
    
    [Fact]
    public void BanUser_Fail()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test71", Email = "example71@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test72", Email = "example72@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test73", Email = "example73@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.BanUser("Test42");
        Assert.False(banResult);
    }
    
    [Fact]
    public void IsBanned_Success()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test81", Email = "example81@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test82", Email = "example82@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test83", Email = "example83@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.BanUser("Test81");
        Assert.True(banResult);
        Assert.NotNull(_blackListDataService.GetUserBannedByEmail("example81@email.com"));
    }
    
    [Fact]
    public void UnbanUser_Success()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test91", Email = "example91@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test92", Email = "example92@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test93", Email = "example93@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.BanUser("Test91");
        Assert.True(banResult);
        Assert.True(_blackListDataService.UnbanUser("example91@email.com"));
    }

    [Fact]
    public void UnbanUser_Fail_Cause_Email_Null()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test91", Email = "example91@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test92", Email = "example92@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test93", Email = "example93@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.BanUser("Test91");
        Assert.True(banResult);
        Assert.False(_blackListDataService.UnbanUser(null));
    }

    [Fact]
    public void GetNumberOfBanned_Success()
    {
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "email@email.com", ExpirationDate = new DateOnly(2024,03,30)});
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eeemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.GetNumberOfBannedUsers();
        Assert.Equal(3,banResult);
    }

    [Fact]
    public void GetBannedUser_Success_Positive_Value_Ordered_None()
    {
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "email@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eeemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.GetBannedUsers(1,3,0);
        Assert.Equal(3, banResult.Count());
    }

    [Fact]
    public void GetBannedUser_Success_Positive_Value_Ordered_Email()
    {
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "email@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eeemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.GetBannedUsers(1, 3, BlackListOdrerCriteria.ByEmail);
        Assert.Equal(3, banResult.Count());
    }


    [Fact]
    public void GetBannedUser_Success_Positive_Value_Ordered_ExpirationDate()
    {
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "email@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eeemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.GetBannedUsers(1, 3, BlackListOdrerCriteria.ByExpirationDate);
        Assert.Equal(3, banResult.Count());
    }

    [Fact]
    public void GetBannedUser_Success_Positive_Value_Ordered_Default()
    {
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "email@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eeemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.GetBannedUsers(1, 3,default);
        Assert.Equal(3, banResult.Count());
    }



    [Fact]
    public void GetBannedUser_Success_Negative_Value()
    {
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "email@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.BlackLists.Add(new BlackListEntity { Email = "eeemail@email.com", ExpirationDate = new DateOnly(2024, 03, 30) });
        _dbContext.SaveChanges();
        var banResult = _blackListDataService.GetBannedUsers(-1, -3, 0);
        Assert.Equal(3, banResult.Count());
    }
}