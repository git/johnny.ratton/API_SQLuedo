using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace TestEF.Service;

public class TestSolutionDataService
{
    private readonly UserDbContext _dbContext;
    private readonly SolutionDataService _solutionDataService;

    public TestSolutionDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _solutionDataService = new SolutionDataService(_dbContext);
    }

    [Fact]
    public void GetSolution_Success()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Description", IsUser = false });
        var solution = new SolutionEntity
        {
            MurdererFirstName = "Maxime",
            MurdererLastName = "Sapountzis",
            MurderPlace = "La cuisine",
            MurderWeapon = "Le couteau",
            Explaination = "Parce que",
            OwnerId = 42,
        };
        _dbContext.Solutions.Add(solution);
        _dbContext.SaveChanges();

        var result = _solutionDataService.GetSolutionByInquiryId(42);

        Assert.Equal(solution.MurdererFirstName,result.MurdererFirstName);
        Assert.Equal(solution.MurdererLastName,result.MurdererLastName);
        Assert.Equal(solution.MurderPlace,result.MurderPlace);
        Assert.Equal(solution.MurderWeapon,result.MurderWeapon);
        Assert.Equal(solution.Explaination,result.Explaination);
        Assert.Equal(solution.OwnerId,result.OwnerId);
    }
    
    [Fact]
    public void GetSolution_NotFound()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Description", IsUser = false });
        var solution = new SolutionEntity
        {
            MurdererFirstName = "Maxime",
            MurdererLastName = "Sapountzis",
            MurderPlace = "La cuisine",
            MurderWeapon = "Le couteau",
            Explaination = "Parce que",
            OwnerId = 42,
        };
        _dbContext.Solutions.Add(solution);
        _dbContext.SaveChanges();

        Assert.Throws<ArgumentException>(() =>
        {
            _solutionDataService.GetSolutionByInquiryId(10);
        });
    }
}