﻿using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;

namespace TestEF.Service;

public class TestInquiryDataService
{
    private readonly UserDbContext _dbContext;
    private readonly InquiryDataService _inquiryDataService;

    public TestInquiryDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _inquiryDataService = new InquiryDataService(_dbContext);
    }

    [Fact]
    public void GetInquiries_WithNoneCriteria_ReturnsCorrectNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, 2, InquiryOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetNumberOfInquiries();

        Assert.Equal(3, result);
    }
    
    [Fact]
    public void GetInquiries_OrderedByTitle_ReturnsCorrectNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, 2, InquiryOrderCriteria.ByTitle);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetInquiries_OrderedByDescription_ReturnsCorrectNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, 2, InquiryOrderCriteria.ByDescription);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetInquiries_OrderedByIsUser_ReturnsCorrectNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, 2, InquiryOrderCriteria.ByIsUser);

        Assert.Equal(2, result.Count());
    }

    [Fact]
    public void GetInquiries_OrderedById_ReturnsCorrectNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, 2, InquiryOrderCriteria.ById);

        Assert.Equal(2, result.Count());
    }

    [Fact]
    public void GetInquiries_OrderedByDefault_ReturnsCorrectNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, 2, default);

        Assert.Equal(2, result.Count());
    }

    [Fact]
    public void GetInquiries_OrderedDefault_ReturnsCorrectNumberOfInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 3, Title = "Test3", Description = "Desc3", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, 2, default);

        Assert.Equal(2, result.Count());
    }

    [Fact]
    public void GetInquiryById_ReturnsCorrectInquiry()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiryById(1);

        Assert.Equal("Test1", result.Title);
    }

    [Fact]
    public void GetInquiryByTitle_ReturnsCorrectInquiry()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiryByTitle("Test1");

        Assert.Equal(1, result.Id);
    }

    [Fact]
    public void CreateInquiry_AddsNewInquiry()
    {
        var result = _inquiryDataService.CreateInquiry("Test", "Desc", true);

        Assert.Equal(1, _dbContext.Inquiries.Count());
        Assert.Equal("Test", result.Title);
    }

    [Fact]
    public void DeleteInquiry_RemovesInquiry()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.SaveChanges();

        _inquiryDataService.DeleteInquiry(1);

        Assert.Empty(_dbContext.Inquiries);
    }

    [Fact]
    public void UpdateInquiry_UpdatesExistingInquiry()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.SaveChanges();

        var updatedInquiry = new InquiryEntity { Id = 1, Title = "Updated", Description = "Desc", IsUser = false };
        var result = _inquiryDataService.UpdateInquiry(1, updatedInquiry);

        Assert.Equal("Updated", result.Title);
    }

    [Fact]
    public void GetInquiries_WithBadPage_ReturnsClassicInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(-1, 2, InquiryOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetInquiries_WithBadNumber_ReturnsClassicInquiries()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 2, Title = "Test2", Description = "Desc2", IsUser = false });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.GetInquiries(1, -42, InquiryOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }

    [Fact]
    public void GetInquiryById_WithoutId_ThrowsException()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.SaveChanges();

        Assert.Throws<ArgumentException>(() => _inquiryDataService.GetInquiryById(2));
    }

    [Fact]
    public void GetInquiryByTitle_WithoutTitle_ThrowsException()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.SaveChanges();

        Assert.Throws<ArgumentException>(() => _inquiryDataService.GetInquiryByTitle("Test2"));
    }

    [Fact]
    public void DeleteInquiry_WithoutId_ReturnsFalse()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.SaveChanges();

        var result = _inquiryDataService.DeleteInquiry(2);

        Assert.False(result);
    }

    [Fact]
    public void UpdateInquiry_WithoutId_ThrowsException()
    {
        _dbContext.Inquiries.Add(new InquiryEntity { Id = 1, Title = "Test1", Description = "Desc1", IsUser = true });
        _dbContext.SaveChanges();

        var updatedInquiry = new InquiryEntity { Id = 2, Title = "Updated", Description = "Desc", IsUser = false };

        Assert.Throws<ArgumentException>(() => _inquiryDataService.UpdateInquiry(2, updatedInquiry));
    }
}