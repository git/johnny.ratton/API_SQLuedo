using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;

namespace TestEF.Service;

public class TestParagraphDataService
{
    private readonly UserDbContext _dbContext;
    private readonly ParagraphDataService _paragraphDataService;
    private readonly LessonEntity _lesson;

    public TestParagraphDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _lesson = new LessonEntity
        {
            Id = 1, Title = "Test", LastPublisher = "Publisher", LastEdit = DateOnly.MinValue
        };
        _paragraphDataService = new ParagraphDataService(_dbContext);
    }

    [Fact]
    public void GetParagraphs_WithoutCriteria_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(1, 2, ParagraphOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetParagraphs_OrderdByTitle_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(1, 2, ParagraphOrderCriteria.ByTitle);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetParagraphs_OrderedByContent_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(1, 2, ParagraphOrderCriteria.ByContent);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetParagraphs_OrderedByQuery_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(1, 2, ParagraphOrderCriteria.ByQuery);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetParagraphs_OrderedByInfo_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(1, 2, ParagraphOrderCriteria.ByInfo);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetParagraphs_OrderedByComment_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(1, 2, ParagraphOrderCriteria.ByComment);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetParagraphs_WithBadPage_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(-42, 2, ParagraphOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetParagraphs_WithBadNumber_ReturnsCorrectNumberOfParagraphs()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContentContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContentContent2", ContentTitle = "ContentTitl2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 3, ContentContent = "ContentContent3", ContentTitle = "ContentTitle3", Title = "Test3",
            Content = "Content3", Info = "Info3", Query = "Query3", Comment = "Comment3",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphs(1, -42, ParagraphOrderCriteria.None);

        Assert.Equal(3, result.Count());
    }

    [Fact]
    public void GetParagraphById_ReturnsCorrectParagraph()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContenContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 2, ContentContent = "ContenContent2", ContentTitle = "ContentTitle2", Title = "Test2",
            Content = "Content2", Info = "Info2", Query = "Query2", Comment = "Comment2",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var result = _paragraphDataService.GetParagraphById(1);

        Assert.Equal("Test1", result.Title);
    }

    [Fact]
    public void CreateParagraph_AddsNewParagraph()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        var result = _paragraphDataService.CreateParagraph("ContentTitle", "ContentContent", "Test", "Content", "Info",
            "Query", "Comment", 1);

        Assert.Equal(1, _dbContext.Paragraphs.Count());
        Assert.Equal("Test", result.Title);
    }

    [Fact]
    public void DeleteParagraph_RemovesParagraph()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContenContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });

        _dbContext.SaveChanges();

        var b = _paragraphDataService.DeleteParagraph(1);

        Assert.Empty(_dbContext.Paragraphs);
    }

    [Fact]
    public void UpdateParagraph_UpdatesExistingParagraph()
    {
        _dbContext.Lessons.Add(_lesson);
        _dbContext.SaveChanges();

        _dbContext.Paragraphs.Add(new ParagraphEntity
        {
            Id = 1, ContentContent = "ContenContent1", ContentTitle = "ContentTitle1", Title = "Test1",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        });
        _dbContext.SaveChanges();

        var updatedParagraph = new ParagraphEntity
        {
            Id = 1, ContentContent = "ContenContent1", ContentTitle = "ContentTitle1", Title = "Updated",
            Content = "Content1", Info = "Info1", Query = "Query1", Comment = "Comment1",
            LessonId = 1
        };
        var result = _paragraphDataService.UpdateParagraph(1, updatedParagraph);

        Assert.Equal("Updated", result.Title);
    }
}