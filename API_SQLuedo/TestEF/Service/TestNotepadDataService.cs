using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using Model.OrderCriteria;

namespace TestEF.Service;

public class TestNotepadDataService
{
    private readonly UserDbContext _dbContext;
    private readonly NotepadDataService _notepadDataService;

    public TestNotepadDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _notepadDataService = new NotepadDataService(_dbContext);
    }

    [Fact]
    public void GetNotepad_Success()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "These are some notes" };
        _dbContext.Notepads.Add(notepad);
        _dbContext.SaveChanges();

        var result = _notepadDataService.GetNotepadFromUserAndInquiryId(42, 42);

        Assert.Equal(notepad,result);
    }
    
    [Fact]
    public void GetNotepad_Fail_UserNotFound()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "These are some notes" };
        _dbContext.Notepads.Add(notepad);
        _dbContext.SaveChanges();
        Assert.Throws<ArgumentException>(() =>
        {
            _notepadDataService.GetNotepadFromUserAndInquiryId(10, 42);
        });
    }
    
    [Fact]
    public void GetNotepad_Fail_InquiryNotFound()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "These are some notes" };
        _dbContext.Notepads.Add(notepad);
        _dbContext.SaveChanges();
        Assert.Throws<ArgumentException>(() =>
        {
            _notepadDataService.GetNotepadFromUserAndInquiryId(42, 10);
        });
    }
    
    [Fact]
    public void GetNotepad_Fail_NotepadNotFound()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 1, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        Assert.Throws<ArgumentException>(() =>
        {
            _notepadDataService.GetNotepadFromUserAndInquiryId(42, 1);
        });
    }
    
    [Fact]
    public void SetNotepad_Success()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "Some notes again" };
        _notepadDataService.SetNotepadFromUserAndInquiryId(42,42, "Some notes again");
        _dbContext.SaveChanges();

        var result = _notepadDataService.GetNotepadFromUserAndInquiryId(42, 42);

        Assert.Equal(notepad.UserId,result.UserId);
        Assert.Equal(notepad.InquiryId,result.InquiryId);
        Assert.Equal(notepad.Notes,result.Notes);
    }
    
    [Fact]
    public void SetNotepad_Fail_UserNotFound()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "These are some notes" };
        _dbContext.SaveChanges();
        Assert.Throws<ArgumentException>(() =>
        {
            _notepadDataService.SetNotepadFromUserAndInquiryId(10, 42, "Some notes");
        });
    }
    
    [Fact]
    public void SetNotepad_Fail_InquiryNotFound()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "These are some notes" };
        _dbContext.SaveChanges();
        Assert.Throws<ArgumentException>(() =>
        {
            _notepadDataService.SetNotepadFromUserAndInquiryId(42, 10, "Some notes");
        });
    }
    
    [Fact]
    public void SetNotepad_Fail_NotepadAlreadyExists()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "Some notes" };
        _dbContext.Notepads.Add(notepad);
        _dbContext.SaveChanges();
        Assert.Throws<ArgumentException>(() =>
        {
            _notepadDataService.SetNotepadFromUserAndInquiryId(42, 42, "Some notes");
        });
    }
    
    [Fact]
    public void UpdateNotepad_Success()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Descritpion", IsUser = false });
        _dbContext.Users.Add(new UserEntity
            { Id = 42, Username = "Username", Email = "email@example.com", Password = "password", IsAdmin = true });
        var notepad = new NotepadEntity { UserId = 42, InquiryId = 42, Notes = "Some notes" };
        _dbContext.Notepads.Add(notepad);
        _dbContext.SaveChanges();
        _notepadDataService.UpdateNotepadFromUserAndInquiryId(42,42, "New Notes");
        _dbContext.SaveChanges();
        Assert.Equal("New Notes", _notepadDataService.GetNotepadFromUserAndInquiryId(42,42).Notes);
    }
}