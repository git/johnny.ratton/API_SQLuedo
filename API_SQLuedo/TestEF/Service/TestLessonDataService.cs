using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;

namespace TestEF.Service;

public class TestLessonDataService
{
    private readonly UserDbContext _dbContext;
    private readonly LessonDataService _lessonDataService;

    public TestLessonDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _lessonDataService = new LessonDataService(_dbContext);
    }

    [Fact]
    public void GetLessons_WithNoneCriteria_ReturnsCorrectNumberOfLessons()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 3, Title = "Test3", LastPublisher = "Publisher3", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessons(1, 2, LessonOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetLessons_OrderedByTitle_ReturnsCorrectNumberOfLessons()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 3, Title = "Test3", LastPublisher = "Publisher3", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessons(1, 2, LessonOrderCriteria.ByTitle);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetLessons_OrderedByLastPublisher_ReturnsCorrectNumberOfLessons()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 3, Title = "Test3", LastPublisher = "Publisher3", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessons(1, 2, LessonOrderCriteria.ByLastPublisher);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetLessons_OrderedByLastEdit_ReturnsCorrectNumberOfLessons()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 3, Title = "Test3", LastPublisher = "Publisher3", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessons(1, 2, LessonOrderCriteria.ByLastEdit);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetLessons_WithBadPage_ReturnsCorrectNumberOfLessons()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 3, Title = "Test3", LastPublisher = "Publisher3", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessons(-42, 2, LessonOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetLessons_WithBadNumber_ReturnsCorrectNumberOfLessons()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 3, Title = "Test3", LastPublisher = "Publisher3", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessons(1, -42, LessonOrderCriteria.None);

        Assert.Equal(3, result.Count());
    }

    [Fact]
    public void GetLessonById_ReturnsCorrectLesson()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessonById(1);

        Assert.Equal("Test1", result.Title);
    }

    [Fact]
    public void GetLessonByTitle_ReturnsCorrectLesson()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 2, Title = "Test2", LastPublisher = "Publisher2", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var result = _lessonDataService.GetLessonByTitle("Test1");

        Assert.Equal(1, result.Id);
    }

    [Fact]
    public void CreateLesson_AddsNewLesson()
    {
        var result = _lessonDataService.CreateLesson(1, "Test", "Publisher", DateOnly.MinValue);

        Assert.Equal(1, _dbContext.Lessons.Count());
        Assert.Equal("Test", result.Title);
    }

    [Fact]
    public void DeleteLesson_RemovesLesson()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        _lessonDataService.DeleteLesson(1);

        Assert.Empty(_dbContext.Lessons);
    }

    [Fact]
    public void UpdateLesson_UpdatesExistingLesson()
    {
        _dbContext.Lessons.Add(new LessonEntity
            { Id = 1, Title = "Test1", LastPublisher = "Publisher1", LastEdit = DateOnly.MinValue });
        _dbContext.SaveChanges();

        var updatedLesson = new LessonEntity
            { Id = 1, Title = "Updated", LastPublisher = "Publisher", LastEdit = DateOnly.MinValue };
        var result = _lessonDataService.UpdateLesson(1, updatedLesson);

        Assert.Equal("Updated", result.Title);
    }
}