using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace TestEF.Service;

public class TestInquiryTableDataService
{
    private readonly UserDbContext _dbContext;
    private readonly InquiryTableDataService _inquiryTableDataService;

    public TestInquiryTableDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _inquiryTableDataService = new InquiryTableDataService(_dbContext);
    }

    [Fact]
    public void GetDatabaseFromInquiryId_Success()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Description", IsUser = false });
        var inquiryTable = new InquiryTableEntity()
        {
            OwnerId = 42,
            DatabaseName = "Database",
            ConnectionInfo = "ConnectionString"
        };
        _dbContext.InquiryTables.Add(inquiryTable);
        _dbContext.SaveChanges();
        var result = _inquiryTableDataService.GetDatabaseNameByInquiryId(42);
        Assert.Equal("Database",result);
    }
    
    [Fact]
    public void GetSolution_NotFound()
    {
        _dbContext.Inquiries.Add(new InquiryEntity
            { Id = 42, Title = "Titre", Description = "Description", IsUser = false });
        var inquiryTable = new InquiryTableEntity()
        {
            OwnerId = 42,
            DatabaseName = "Database",
            ConnectionInfo = "ConnectionString"
        };
        _dbContext.InquiryTables.Add(inquiryTable);
        _dbContext.SaveChanges();

        Assert.Throws<ArgumentException>(() =>
        {
            _inquiryTableDataService.GetDatabaseNameByInquiryId(10);
        });
    }
}