using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;

namespace TestEF.Service;

public class TestSuccessDataService
{
    private readonly UserDbContext _dbContext;
    private readonly SuccessDataService _successDataService;
    private readonly List<UserEntity> _users = new();
    private readonly List<InquiryEntity> _inquiries = new();

    public TestSuccessDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _successDataService = new SuccessDataService(_dbContext);

        _users.AddRange(new List<UserEntity>
        {
            new()
            {
                Id = 1,
                Username = "Pseudo",
                Password = "Password",
                Email = "Email@gmail.com",
                IsAdmin = true
            },
            new()
            {
                Id = 2,
                Username = "Pseudo2",
                Password = "Password2",
                Email = "Email2@gmail.com",
                IsAdmin = false
            },
            new()
            {
                Id = 3,
                Username = "Pseudo3",
                Password = "Password3",
                Email = "Email3@gmail.com",
                IsAdmin = false
            }
        });

        _inquiries.AddRange(new List<InquiryEntity>
        {
            new()
            {
                Id = 1,
                Title = "Title",
                Description = "Description",
                IsUser = false
            },
            new()
            {
                Id = 2,
                Title = "Title2",
                Description = "Description2",
                IsUser = true
            },
            new()
            {
                Id = 3,
                Title = "Title3",
                Description = "Description3",
                IsUser = false
            }
        });
        _dbContext.Users.AddRange(_users);
        _dbContext.Inquiries.AddRange(_inquiries);
        _dbContext.SaveChanges();
    }

    [Fact]
    public void GetSuccesses_WithoutCriteria_ReturnsCorrectNumberOfSuccesses()
    {
        var success1 = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 2, InquiryId = 2, IsFinished = false };
        var success3 = new SuccessEntity { UserId = 3, InquiryId = 3, IsFinished = true };

        _dbContext.Successes.AddRange(success1, success2, success3);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccesses(1, 2, SuccessOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetSuccesses_OrderedByUserId_ReturnsCorrectNumberOfSuccesses()
    {
        var success1 = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 2, InquiryId = 2, IsFinished = false };
        var success3 = new SuccessEntity { UserId = 3, InquiryId = 3, IsFinished = true };

        _dbContext.Successes.AddRange(success1, success2, success3);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccesses(1, 2, SuccessOrderCriteria.ByUserId);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetSuccesses_OrderedByInquiryId_ReturnsCorrectNumberOfSuccesses()
    {
        var success1 = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 2, InquiryId = 2, IsFinished = false };
        var success3 = new SuccessEntity { UserId = 3, InquiryId = 3, IsFinished = true };

        _dbContext.Successes.AddRange(success1, success2, success3);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccesses(1, 2, SuccessOrderCriteria.ByInquiryId);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetSuccesses_OrderedByIsFinished_ReturnsCorrectNumberOfSuccesses()
    {
        var success1 = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 2, InquiryId = 2, IsFinished = false };
        var success3 = new SuccessEntity { UserId = 3, InquiryId = 3, IsFinished = true };

        _dbContext.Successes.AddRange(success1, success2, success3);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccesses(1, 2, SuccessOrderCriteria.ByIsFinished);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetSuccesses_WithBadPage_ReturnsCorrectNumberOfSuccesses()
    {
        var success1 = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 2, InquiryId = 2, IsFinished = false };
        var success3 = new SuccessEntity { UserId = 3, InquiryId = 3, IsFinished = true };

        _dbContext.Successes.AddRange(success1, success2, success3);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccesses(-42, 2, SuccessOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetSuccesses_WithBadNumber_ReturnsCorrectNumberOfSuccesses()
    {
        var success1 = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 2, InquiryId = 2, IsFinished = false };
        var success3 = new SuccessEntity { UserId = 3, InquiryId = 3, IsFinished = true };

        _dbContext.Successes.AddRange(success1, success2, success3);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccesses(1, -42, SuccessOrderCriteria.None);

        Assert.Equal(3, result.Count());
    }

    [Fact]
    public void GetSuccessesByUserId_ReturnsCorrectSuccesses()
    {
        var success1 = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 1, InquiryId = 2, IsFinished = false };

        _dbContext.Successes.AddRange(success1, success2);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccessesByUserId(1);

        Assert.Equal(2, result.Count());
    }
/*
    [Fact]
    public void GetSuccessesByInquiryId_ReturnsCorrectSuccesses()
    {
        _dbContext.Inquiries.Add(
            new InquiryEntity
            {
                Id = 4,
                Title = "Title3",
                Description = "Description3",
                IsUser = false
            }
        );
        _dbContext.SaveChanges();

        var success1 = new SuccessEntity { UserId = 1, InquiryId = 4, IsFinished = true };
        var success2 = new SuccessEntity { UserId = 2, InquiryId = 4, IsFinished = false };

        _dbContext.Successes.AddRange(success1, success2);
        _dbContext.SaveChanges();

        var result = _successDataService.GetSuccessesByInquiryId(4);

        Assert.Equal(2, result.Count());
    }*/

    [Fact]
    public void DeleteSuccess_RemovesSuccess()
    {
        var success = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };

        _dbContext.Successes.Add(success);
        _dbContext.SaveChanges();

        _successDataService.DeleteSuccess(1, 1);

        Assert.Empty(_dbContext.Successes);
    }

    [Fact]
    public void UpdateSuccess_UpdatesExistingSuccess()
    {
        var success = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = true };

        _dbContext.Successes.Add(success);
        _dbContext.SaveChanges();

        var updatedSuccess = new SuccessEntity { UserId = 1, InquiryId = 1, IsFinished = false };
        var result = _successDataService.UpdateSuccess(1, 1, updatedSuccess);

        Assert.False(result.IsFinished);
    }

    [Fact]
    public void CreateSuccess_AddsNewSuccess()
    {
        var result = _successDataService.CreateSuccess(1, 3, true);

        Assert.Single(_dbContext.Successes);
        Assert.True(result.IsFinished);
    }
}