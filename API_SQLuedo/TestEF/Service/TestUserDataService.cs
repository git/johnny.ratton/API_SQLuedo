using DbContextLib;
using DbDataManager.Service;
using Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Model.OrderCriteria;

namespace TestEF.Service;

public class TestUserDataService
{
    private readonly UserDbContext _dbContext;
    private readonly UserDataService _userDataService;

    public TestUserDataService()
    {
        var connection = new SqliteConnection("DataSource=:memory:");
        connection.Open();
        var options = new DbContextOptionsBuilder<UserDbContext>()
            .UseSqlite(connection)
            .Options;

        _dbContext = new UserDbContext(options);
        _userDataService = new UserDataService(_dbContext);
    }

    [Fact]
    public void GetUsers_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test10", Email = "example1@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test12", Email = "example2@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test13", Email = "example3@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetUsers(1, 2, UserOrderCriteria.None);
        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetNotAdminUsers_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test14", Email = "example4@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test15", Email = "example5@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test16", Email = "example6@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNotAdminUsers(1, 2, UserOrderCriteria.None);
        Assert.Equal(1, result.Count());
    }
    
    [Fact]
    public void GetNotAdminUsers_BadPage_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test17", Email = "example7@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test18", Email = "example8@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test19", Email = "example9@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNotAdminUsers(-42, 2, UserOrderCriteria.None);
        Assert.Equal(1, result.Count());
    }
    
    [Fact]
    public void GetNotAdminUsers_BadNumber_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test20", Email = "example10@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test21", Email = "example11@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test22", Email = "example12@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNotAdminUsers(1, -42, UserOrderCriteria.None);
        Assert.Equal(1, result.Count());
    }
    
    [Fact]
    public void GetNotAdminUsers_OrderedById_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test23", Email = "example13@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test24", Email = "example14@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test51", Email = "example15@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNotAdminUsers(1, 2, UserOrderCriteria.ById);
        Assert.Equal(1, result.Count());
    }
    
    [Fact]
    public void GetNotAdminUsers_OrderedByUsername_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test25", Email = "example16@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test26", Email = "example17@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test27", Email = "example18@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNotAdminUsers(1, 2, UserOrderCriteria.ByUsername);
        Assert.Equal(1, result.Count());
    }
    
    [Fact]
    public void GetNotAdminUsers_OrderedByEmail_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test52", Email = "example45@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test53", Email = "example46@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test54", Email = "example17@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNotAdminUsers(1, 2, UserOrderCriteria.ByEmail);
        Assert.Equal(1, result.Count());
    }
    
    [Fact]
    public void GetNotAdminUsers_OrderedByIsAdmin_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test28", Email = "example19@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test29", Email = "example20@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test30", Email = "example21@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNotAdminUsers(1, 2, UserOrderCriteria.ByIsAdmin);
        Assert.Equal(1, result.Count());
    }
    
    [Fact]
    public void GetNumberOfUsers_ReturnsCorrectNumberOfUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test31", Email = "example22@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test32", Email = "example23@email.com", Password = "password", IsAdmin = false });
        _dbContext.Users.Add(new UserEntity() { Id = 3, Username = "Test33", Email = "example24@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        var result = _userDataService.GetNumberOfUsers();
        Assert.Equal(3, result);
    }

    [Fact]
    public void GetUserById_ReturnsCorrectUser()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test34", Email = "example25@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test35", Email = "example26@email.com", Password = "password", IsAdmin = false });
        _dbContext.SaveChanges();
        var result = _userDataService.GetUserById(1);
        Assert.Equal("Test34", result.Username);
    }

    [Fact]
    public void GetUserByUsername_ReturnsCorrectUser()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test36", Email = "example27@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test37", Email = "example28@email.com", Password = "password", IsAdmin = false });
        _dbContext.SaveChanges();
        var result = _userDataService.GetUserByUsername("Test36");
        Assert.Equal(1, result.Id);
    }
    [Fact]
    public void GetUserByEmail_ReturnsCorrectUser()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test38", Email = "example29@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test39", Email = "example30@email.com", Password = "password", IsAdmin = false });
        _dbContext.SaveChanges();
        var result = _userDataService.GetUserByEmail("example29@email.com");
        Assert.Equal(1, result.Id);
    }

    [Fact]
    public void CreateUser_AddsNewUser()
    {
        var result = _userDataService.CreateUser("Test42", "password", "eamil@example1.com", true);
        Assert.Equal(1, _dbContext.Users.Count());
        Assert.Equal("Test42", result.Username);
    }

    [Fact]
    public void DeleteUser_RemovesUser()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        _userDataService.DeleteUser(1);
        Assert.Empty(_dbContext.Inquiries);
    }
    
    [Fact]
    public void DeleteUserByUsername_RemovesUser()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        _userDataService.DeleteUserByUsername("Test1");
        Assert.Empty(_dbContext.Inquiries);
    }

    [Fact]
    public void UpdateUser_UpdatesExistingUser()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        var updatedUser = new UserEntity() { Id = 1, Username = "Updated", Email = "updated@example.com", Password = "updated", IsAdmin = false };
        var result = _userDataService.UpdateUser(1, updatedUser);

        Assert.Equal("Updated", result.Username);
        Assert.Equal("updated@example.com", result.Email);
        Assert.Equal("updated", result.Password);
        Assert.False(result.IsAdmin);
    }

    [Fact]
    public void GetUsers_WithBadPage_ReturnsClassicUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test40", Email = "example31@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test41", Email = "example32@email.com", Password = "password", IsAdmin = false });
        _dbContext.SaveChanges();

        var result = _userDataService.GetUsers(-1, 2, UserOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }
    
    [Fact]
    public void GetUsers_WithBadNumber_ReturnsClassicUsers()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test43", Email = "example33@email.com", Password = "password", IsAdmin = true });
        _dbContext.Users.Add(new UserEntity() { Id = 2, Username = "Test44", Email = "example34@email.com", Password = "password", IsAdmin = false });
        _dbContext.SaveChanges();

        var result = _userDataService.GetUsers(1, -42, UserOrderCriteria.None);

        Assert.Equal(2, result.Count());
    }

    [Fact]
    public void GetUserById_WithoutId_ThrowsException()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();
        Assert.Throws<ArgumentException>(() => _userDataService.GetUserById(2));
    }

    [Fact]
    public void GetUserByUsername_WithoutUsername_ThrowsException()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        Assert.Throws<ArgumentException>(() => _userDataService.GetUserByUsername("Test2"));
    }
    
    [Fact]
    public void GetUserByEmail_WithoutUsername_ThrowsException()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        Assert.Throws<ArgumentException>(() => _userDataService.GetUserByEmail("test@test"));
    }

    [Fact]
    public void DeleteUser_WithoutId_ReturnsFalse()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        var result = _userDataService.DeleteUser(2);

        Assert.False(result);
    }
    [Fact]
    public void isEmailTaken_ReturnFalse()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test81", Email = "example81@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        var result = _userDataService.IsEmailTaken("example895@email.com");

        Assert.False(result);
    }
    [Fact]
    public void isEmailTaken_ReturnTrue()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test82", Email = "example82@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        var result = _userDataService.IsEmailTaken("example82@email.com");

        Assert.True(result);
    }
    
    [Fact]
    public void DeleteUserByUsername_WithoutExisting_ReturnsFalse()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        var result = _userDataService.DeleteUserByUsername("Test2");

        Assert.False(result);
    }

    [Fact]
    public void UpdateUser_WithoutId_ThrowsException()
    {
        _dbContext.Users.Add(new UserEntity() { Id = 1, Username = "Test1", Email = "example@email.com", Password = "password", IsAdmin = true });
        _dbContext.SaveChanges();

        var updatedUser = new UserEntity() { Id = 1, Username = "Updated", Email = "updated@email.com", Password = "updated", IsAdmin = false };

        Assert.Throws<ArgumentException>(() => _userDataService.UpdateUser(2, updatedUser));
    }
}