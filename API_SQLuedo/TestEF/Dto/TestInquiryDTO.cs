using Dto;

namespace TestEF.Dto;

public class TestInquiryDto
{
    private const int _id = 1;
    private const string _title = "Title";
    private const string _description = "_description";
    private const bool _isUser = true;

    [Fact]
    public void TestConstructorWithId()
    {
        InquiryDto inquiry = new InquiryDto(_id, _title, _description, _isUser);
        Assert.Equal(_id, inquiry.Id);
        Assert.Equal(_title, inquiry.Title);
        Assert.Equal(_description, inquiry.Description);
        Assert.True(inquiry.IsUser);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        InquiryDto inquiry = new InquiryDto(_title, _description, _isUser);
        Assert.Equal(0, inquiry.Id);
        Assert.Equal(_title, inquiry.Title);
        Assert.Equal(_description, inquiry.Description);
        Assert.True(inquiry.IsUser);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        InquiryDto inquiry = new InquiryDto(_id, _title, _description, _isUser);
        Assert.Equal(_id, inquiry.Id);
        Assert.Equal(_title, inquiry.Title);
        Assert.Equal(_description, inquiry.Description);
        Assert.True(inquiry.IsUser);
    }
}