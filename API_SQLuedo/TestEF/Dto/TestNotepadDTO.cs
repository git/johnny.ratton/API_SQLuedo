using Dto;

namespace TestEF.Dto;

public class TestNotepadDto
{
    private const int Id = 42;
    private const int UserId = 42;
    private const int InquiryId = 42;
    private const string Notes = "This is some notes example";

    [Fact]
    public void TestDefaultConstructor()
    {
        NotepadDto notepad = new NotepadDto();
        Assert.Equal(0, notepad.Id);
        Assert.Equal(0, notepad.UserId);
        Assert.Equal(0, notepad.InquiryId);
        Assert.Null(notepad.Notes);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        NotepadDto notepad = new NotepadDto(UserId, InquiryId, Notes);
        Assert.Equal(0, notepad.Id);
        Assert.Equal(UserId, notepad.UserId);
        Assert.Equal(InquiryId, notepad.InquiryId);
        Assert.Equal(Notes, notepad.Notes);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        NotepadDto notepad = new NotepadDto(Id, UserId, InquiryId, Notes);
        Assert.Equal(Id, notepad.Id);
        Assert.Equal(UserId, notepad.UserId);
        Assert.Equal(InquiryId, notepad.InquiryId);
        Assert.Equal(Notes, notepad.Notes);
    }
}