using Dto;

namespace TestEF.Dto;

public class TestSolutionDto
{
    private const string MurdererFirstName = "John";
    private const string MurdererLastName = "Doe";
    private const string MurderPlace = "WhiteHouse";
    private const string MurderWeapon = "Nuclear Bomb";
    private const string Explaination = "This is an explaination";
    private const int InquiryId = 42;

    [Fact]
    public void TestDefaultConstructor()
    {
        SolutionDto solution = new SolutionDto();
        Assert.Equal(0, solution.OwnerId);
        Assert.Null(solution.MurdererFirstName);
        Assert.Null(solution.MurdererLastName);
        Assert.Null(solution.MurderPlace);
        Assert.Null(solution.MurderWeapon);
        Assert.Null(solution.Explanation);
    }

    [Fact]
    public void TestConstructorWithoutOwnerId()
    {
        SolutionDto solution =
            new SolutionDto(MurdererFirstName, MurdererLastName, MurderPlace, MurderWeapon, Explaination);
        Assert.Equal(0, solution.OwnerId);
        Assert.Equal(MurdererFirstName, solution.MurdererFirstName);
        Assert.Equal(MurdererLastName, solution.MurdererLastName);
        Assert.Equal(MurderPlace, solution.MurderPlace);
        Assert.Equal(MurderWeapon, solution.MurderWeapon);
        Assert.Equal(Explaination, solution.Explanation);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        SolutionDto solution = new SolutionDto(InquiryId, MurdererFirstName, MurdererLastName, MurderPlace,
            MurderWeapon, Explaination);
        Assert.Equal(InquiryId, solution.OwnerId);
        Assert.Equal(MurdererFirstName, solution.MurdererFirstName);
        Assert.Equal(MurdererLastName, solution.MurdererLastName);
        Assert.Equal(MurderPlace, solution.MurderPlace);
        Assert.Equal(MurderWeapon, solution.MurderWeapon);
        Assert.Equal(Explaination, solution.Explanation);
    }
}