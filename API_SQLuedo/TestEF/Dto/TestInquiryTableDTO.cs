using Dto;

namespace TestEF.Dto;

public class TestInquiryTableDto
{
    private const int Id = 1;
    private const string Database = "database";
    private const string Connection = "_connection";

    [Fact]
    public void TestConstructorWithId()
    {
        InquiryTableDto inquiry = new InquiryTableDto(Id, Database, Connection);
        Assert.Equal(Id, inquiry.OwnerId);
        Assert.Equal(Database, inquiry.DatabaseName);
        Assert.Equal(Connection, inquiry.ConnectionInfo);
    }

    [Fact]
    public void TestDefaultConstructor()
    {
        InquiryTableDto inquiry = new InquiryTableDto();
        Assert.Equal(0, inquiry.OwnerId);
        Assert.Null(inquiry.DatabaseName);
        Assert.Null(inquiry.ConnectionInfo);
    }
}