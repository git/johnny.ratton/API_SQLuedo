using Dto;

namespace TestEF.Dto;

public class TestSuccessDto
{
    private const int UserId = 42;
    private const int InquiryId = 7;
    private const bool IsFinished = true;

    [Fact]
    public void TestDefaultConstructor()
    {
        SuccessDto success = new SuccessDto();
        Assert.Equal(0, success.UserId);
        Assert.Equal(0, success.InquiryId);
        Assert.False(success.IsFinished);
    }

    [Fact]
    public void TestConstructorWithIds()
    {
        SuccessDto success = new SuccessDto(UserId, InquiryId, IsFinished);
        Assert.Equal(UserId, success.UserId);
        Assert.Equal(InquiryId, success.InquiryId);
        Assert.True(success.IsFinished);
    }
}