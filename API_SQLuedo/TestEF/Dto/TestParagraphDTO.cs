using Dto;

namespace TestEF.Dto;

public class TestParagraphDto
{
    private const int Id = 42;
    private const string Title = "Title";
    private const string Info = "Info";
    private const string Content = "Content";
    private const string Query = "Query";
    private const string Comment = "Comment";

    [Fact]
    public void TestDefaultConstructor()
    {
        ParagraphDto paragraph = new ParagraphDto();
        Assert.Equal(0, paragraph.Id);
        Assert.Null(paragraph.Title);
        Assert.Null(paragraph.Info);
        Assert.Null(paragraph.Content);
        Assert.Null(paragraph.Query);
        Assert.Null(paragraph.Comment);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        ParagraphDto paragraph = new ParagraphDto(Title, Content, Info, Query, Comment, 10);
        Assert.Equal(0, paragraph.Id);
        Assert.Equal(Title, paragraph.Title);
        Assert.Equal(Info, paragraph.Info);
        Assert.Equal(Content, paragraph.Content);
        Assert.Equal(Query, paragraph.Query);
        Assert.Equal(Comment, paragraph.Comment);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        ParagraphDto paragraph = new ParagraphDto(Id, Title, Content, Info, Query, Comment, 10);
        Assert.Equal(Id, paragraph.Id);
        Assert.Equal(Title, paragraph.Title);
        Assert.Equal(Info, paragraph.Info);
        Assert.Equal(Content, paragraph.Content);
        Assert.Equal(Query, paragraph.Query);
        Assert.Equal(Comment, paragraph.Comment);
        Assert.Equal(10, paragraph.LessonId);
    }
}