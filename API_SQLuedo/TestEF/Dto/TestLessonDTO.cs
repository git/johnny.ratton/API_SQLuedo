using Dto;

namespace TestEF.Dto;

public class TestLessonDto
{
    private const int Id = 42;
    private const string Title = "Title";
    private const string LastPublisher = "Last Publisher";
    private static readonly DateOnly LastEdit = new();

    [Fact]
    public void TestDefaultConstructor()
    {
        LessonDto lesson = new LessonDto();
        Assert.Equal(0, lesson.Id);
        Assert.Null(lesson.Title);
        Assert.Null(lesson.LastPublisher);
        // Default date
        Assert.Equal(new DateOnly(0001, 01, 01), LastEdit);
    }

    [Fact]
    public void TestConstructorWithoutId()
    {
        LessonDto lesson = new LessonDto(Title, LastPublisher, LastEdit);
        Assert.Equal(0, lesson.Id);
        Assert.Equal(Title, lesson.Title);
        Assert.Equal(LastPublisher, lesson.LastPublisher);
        Assert.Equal(LastEdit, lesson.LastEdit);
    }

    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        LessonDto lesson = new LessonDto(Id, Title, LastPublisher, LastEdit);
        Assert.Equal(Id, lesson.Id);
        Assert.Equal(Title, lesson.Title);
        Assert.Equal(LastPublisher, lesson.LastPublisher);
        Assert.Equal(LastEdit, lesson.LastEdit);
    }
}