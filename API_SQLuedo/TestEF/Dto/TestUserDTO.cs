using Dto;

namespace TestEF.Dto
{
    public class TestUserDto
    {
        private const string Username = "username";
        private const string Email = "example@email.com";
        private const string Password = "password";
        private const bool IsAdmin = true;
        private const int Id = 42;

        [Fact]
        public void TestDefaultConstructor()
        {
            UserDto user = new UserDto();
            Assert.Equal(0, user.Id);
            Assert.Null(user.Username);
            Assert.Null(user.Email);
            Assert.Null(user.Password);
            Assert.False(user.IsAdmin);
        }

        [Fact]
        public void TestConstructorWithoutId()
        {
            UserDto user = new UserDto(Username, Password, Email, IsAdmin);
            Assert.Equal(0, user.Id);
            Assert.Equal(Username, user.Username);
            Assert.Equal(Email, user.Email);
            Assert.Equal(Password, user.Password);
            Assert.True(user.IsAdmin);
        }

        [Fact]
        public void TestConstructorWithoutAllAttributes()
        {
            UserDto user = new UserDto(Id, Username, Password, Email, IsAdmin);
            Assert.Equal(Id, user.Id);
            Assert.Equal(Username, user.Username);
            Assert.Equal(Email, user.Email);
            Assert.Equal(Password, user.Password);
            Assert.True(user.IsAdmin);
        }
        
        [Fact]
        public void TestToString()
        {
            UserDto user = new UserDto(Id, Username, Password, Email, IsAdmin);
            Assert.Equal($"{Id}\t{Username}\t{Email}\t{IsAdmin}", user.ToString());
        }
        
        [Fact]
        public void TestEqualsWithSameAttributesValues()
        {
            UserDto user1 = new UserDto(Id, Username, Password, Email, IsAdmin);
            UserDto user2 = new UserDto(Id, Username, Password, Email, IsAdmin);
            Assert.True(user1.Equals(user2));
        }
        
        [Fact]
        public void TestEqualsWithNullReference()
        {
            UserDto user = new UserDto(Id, Username, Password, Email, IsAdmin);
            Assert.False(user.Equals(null));
        }
        
        [Fact]
        public void TestEqualsWithSameReference()
        {
            UserDto user = new UserDto(Id, Username, Password, Email, IsAdmin);
            Assert.True(user.Equals(user));
        }
        
        [Fact]
        public void TestEqualsWithDifferentType()
        {
            UserDto user = new UserDto(Id, Username, Password, Email, IsAdmin);
            Assert.False(user.Equals("Tests"));
        }
        
        [Fact]
        public void TestGetHashCode()
        {
            UserDto user = new UserDto(Id, Username, Password, Email, IsAdmin);
            Assert.Equal(Id, user.GetHashCode());
        }
    }
}