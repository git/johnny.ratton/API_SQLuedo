﻿using Dto;

namespace TestEF.Dto;

public class TestBlackListDto
{
    private const string _email = "email@email.com";
    private  DateOnly _date = new DateOnly(2025,01,21);


    [Fact]
    public void TestConstructorWithAllAttributes()
    {
        BlackListDto inquiry = new BlackListDto(_email,_date);
        Assert.Equal(_email, inquiry.Email);
        Assert.Equal(_date, inquiry.ExpirationDate);
    }
}