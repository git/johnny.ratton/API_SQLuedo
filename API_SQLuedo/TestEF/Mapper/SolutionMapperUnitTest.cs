﻿using Dto;
using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Mapper;

namespace TestEF.Mapper
{
    public class SolutionMapperUnitTest
    {

        private const int _ownerId = 1;
        private const string _murdererFirstName = "Jacques";
        private const string _murdererLastName = "Chirac";
        private const string _murderPlace = "Bureau de richard";
        private const string _murderWeapon = "Les mots";
        private const string _explanation = "Y'en a pas";



        [Fact]
        public void TestDtoToEntity()
        {
            SolutionDto solution = new SolutionDto(_ownerId,_murdererFirstName,_murdererLastName,_murderPlace,_murderWeapon,_explanation);
            var solutionEntity = solution.FromDtoToEntity();

            Assert.NotNull(solutionEntity);
            Assert.IsType<SolutionEntity>(solutionEntity);
            Assert.Equal(_ownerId, solutionEntity.OwnerId);
            Assert.Equal(_murdererFirstName, solutionEntity.MurdererFirstName);
            Assert.Equal(_murdererLastName, solutionEntity.MurdererLastName);
            Assert.Equal(_murderPlace, solutionEntity.MurderPlace);
            Assert.Equal(_murderWeapon, solutionEntity.MurderWeapon);
            Assert.Equal(_explanation, solutionEntity.Explaination);

        }

        [Fact]
        public void TestDtoToModel()
        {
            SolutionDto solution = new SolutionDto(_ownerId, _murdererFirstName, _murdererLastName, _murderPlace, _murderWeapon, _explanation);
            var solutionMod = solution.FromDtoToModel();

            Assert.NotNull(solutionMod);
            Assert.IsType<Solution>(solutionMod);
            Assert.Equal(_ownerId, solutionMod.OwnerId);
            Assert.Equal(_murdererFirstName, solutionMod.MurdererFirstName);
            Assert.Equal(_murdererLastName, solutionMod.MurdererLastName);
            Assert.Equal(_murderPlace, solutionMod.MurderPlace);
            Assert.Equal(_murderWeapon, solutionMod.MurderWeapon);
            Assert.Equal(_explanation, solutionMod.Explaination);

        }

        [Fact]
        public void TestEntityToDto()
        {
            SolutionEntity solution = new SolutionEntity{OwnerId = _ownerId,MurdererFirstName = _murdererFirstName,MurdererLastName = _murdererLastName,MurderPlace = _murderPlace,MurderWeapon = _murderWeapon,Explaination = _explanation};
            var solutionDto = solution.FromEntityToDto();

            Assert.NotNull(solutionDto);
            Assert.IsType<SolutionDto>(solutionDto);
            Assert.Equal(_ownerId, solutionDto.OwnerId);
            Assert.Equal(_murdererFirstName, solutionDto.MurdererFirstName);
            Assert.Equal(_murdererLastName, solutionDto.MurdererLastName);
            Assert.Equal(_murderPlace, solutionDto.MurderPlace);
            Assert.Equal(_murderWeapon, solutionDto.MurderWeapon);
            Assert.Equal(_explanation, solutionDto.Explanation);
        }

        [Fact]
        public void TestEntityToModel()
        {
            SolutionEntity solution = new SolutionEntity { OwnerId = _ownerId, MurdererFirstName = _murdererFirstName, MurdererLastName = _murdererLastName, MurderPlace = _murderPlace, MurderWeapon = _murderWeapon, Explaination = _explanation };
            var solutionMod = solution.FromEntityToModel();

            Assert.NotNull(solutionMod);
            Assert.IsType<Solution>(solutionMod);
            Assert.Equal(_ownerId, solutionMod.OwnerId);
            Assert.Equal(_murdererFirstName, solutionMod.MurdererFirstName);
            Assert.Equal(_murdererLastName, solutionMod.MurdererLastName);
            Assert.Equal(_murderPlace, solutionMod.MurderPlace);
            Assert.Equal(_murderWeapon, solutionMod.MurderWeapon);
            Assert.Equal(_explanation, solutionMod.Explaination);
        }



        [Fact]
        public void TestModelToDto()
        {
            Solution solution = new Solution(_ownerId, _murdererFirstName, _murdererLastName, _murderPlace, _murderWeapon, _explanation);
            var solutionDto = solution.FromModelToDto();

            Assert.NotNull(solutionDto);
            Assert.IsType<SolutionDto>(solutionDto);
            Assert.Equal(_ownerId, solutionDto.OwnerId);
            Assert.Equal(_murdererFirstName, solutionDto.MurdererFirstName);
            Assert.Equal(_murdererLastName, solutionDto.MurdererLastName);
            Assert.Equal(_murderPlace, solutionDto.MurderPlace);
            Assert.Equal(_murderWeapon, solutionDto.MurderWeapon);
            Assert.Equal(_explanation, solutionDto.Explanation);
        }

        [Fact]
        public void TestModelToEntity()
        {
            Solution solution = new Solution(_ownerId,_murdererFirstName,_murdererLastName,_murderPlace,_murderWeapon,_explanation);
            var solutionEntity = solution.FromModelToEntity();

            Assert.NotNull(solutionEntity);
            Assert.IsType<SolutionEntity>(solutionEntity);
            Assert.Equal(_ownerId, solutionEntity.OwnerId);
            Assert.Equal(_murdererFirstName, solutionEntity.MurdererFirstName);
            Assert.Equal(_murdererLastName, solutionEntity.MurdererLastName);
            Assert.Equal(_murderPlace, solutionEntity.MurderPlace);
            Assert.Equal(_murderWeapon, solutionEntity.MurderWeapon);
            Assert.Equal(_explanation, solutionEntity.Explaination);
        }
    }
}
