﻿using Dto;
using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Mapper;

namespace TestEF.Mapper
{
    public class NotepadMapperUnitTest
    {

        private const int _id = 1;
        private const int _userId = 1;
        private const int _inquiryId = 2;
        private const string _notes = "Notes";



        [Fact]
        public void TestDtoToEntity()
        {
            NotepadDto notepad = new NotepadDto(_id,_userId,_inquiryId,_notes);
            var notepadEntity = notepad.FromDtoToEntity();

            Assert.NotNull(notepadEntity);
            Assert.IsType<NotepadEntity>(notepadEntity);
            Assert.Equal(1, notepadEntity.Id);
            Assert.Equal(_userId, notepadEntity.UserId);
            Assert.Equal(0, notepadEntity.InquiryId);
            Assert.Equal(_notes, notepadEntity.Notes);

        }

        [Fact]
        public void TestDtoToModel()
        {
            NotepadDto notepad = new NotepadDto(_id,_userId,_inquiryId,_notes);
            var notepadMod = notepad.FromDtoToModel();

            Assert.NotNull(notepadMod);
            Assert.IsType<Notepad>(notepadMod);
            Assert.Equal(_id, notepadMod.Id);
            Assert.Equal(_userId, notepadMod.UserId);
            Assert.Equal(_inquiryId, notepadMod.InquiryId);
            Assert.Equal(_notes, notepadMod.Notes);

        }

        [Fact]
        public void TestEntityToDto()
        {
            NotepadEntity notepad = new NotepadEntity{Id = _id, User = new UserEntity(),InquiryId = _inquiryId,Inquiry = new InquiryEntity(),Notes = _notes};
            var NotepadDto = notepad.FromEntityToDto();

            Assert.NotNull(NotepadDto);
            Assert.IsType<NotepadDto>(NotepadDto);
            Assert.Equal(1, NotepadDto.Id);
            Assert.Equal(0, NotepadDto.UserId);
            Assert.Equal(2, NotepadDto.InquiryId);
            Assert.Equal(_notes, NotepadDto.Notes);
        }

        [Fact]
        public void TestEntityToModel()
        {
            NotepadEntity notepad = new NotepadEntity { Id = _id, User = new UserEntity(), InquiryId = _inquiryId, Inquiry = new InquiryEntity(), Notes = _notes };
            var notepadMod = notepad.FromEntityToModel();

            Assert.NotNull(notepadMod);
            Assert.IsType<Notepad>(notepadMod);
            Assert.Equal(1, notepadMod.Id);
            Assert.Equal(0, notepadMod.UserId);
            Assert.Equal(2, notepadMod.InquiryId);
            Assert.Equal(_notes, notepadMod.Notes);
        }



        [Fact]
        public void TestModelToDto()
        {
            Notepad notepad = new Notepad(_id,_userId,_inquiryId,_notes);
            var NotepadDto = notepad.FromModelToDto();

            Assert.NotNull(NotepadDto);
            Assert.IsType<NotepadDto>(NotepadDto);
            Assert.Equal(_id, NotepadDto.Id);
            Assert.Equal(_userId, NotepadDto.UserId);
            Assert.Equal(_inquiryId, NotepadDto.InquiryId);
            Assert.Equal(_notes, NotepadDto.Notes);
        }

        [Fact]
        public void TestModelToEntity()
        {
            Notepad notepad = new Notepad(_id,_userId,_inquiryId,_notes);
            var notepadEntity = notepad.FromModelToEntity();

            Assert.NotNull(notepadEntity);
            Assert.IsType<NotepadEntity>(notepadEntity);
            Assert.Equal(1, notepadEntity.Id);
            Assert.Equal(_userId, notepadEntity.UserId);
            Assert.Equal(0, notepadEntity.InquiryId);
            Assert.Equal(_notes, notepadEntity.Notes);
        }
    }
}
