﻿using Dto;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Shared.Mapper;
using SQLitePCL;

namespace TestEF.Mapper
{
    public class ParagraphMapperUnitTest
    {

        private const int _id = 1;
        private const string _title = "Title";
        private const string _content = "Contenu";

        private const string _info = "Info";
        private const string _query = "Requête";
        private const string _comment = "Comment";
        private const int _lessonId = 2;

        [Fact]
        public void TestDtoToEntity()
        {
            ParagraphDto paragraph = new ParagraphDto(_id,_title,_content,_info,_query,_comment,_lessonId);
            var paragrahEntity = paragraph.FromDtoToEntity();

            Assert.NotNull(paragrahEntity);
            Assert.IsType<ParagraphEntity>(paragrahEntity);
            Assert.Equal(1, paragrahEntity.Id);
            Assert.Equal(_title, paragrahEntity.Title);
            Assert.Equal(_content, paragrahEntity.Content);
            Assert.Equal(_info, paragrahEntity.Info);
            Assert.Equal(_query, paragrahEntity.Query);
            Assert.Equal(_comment, paragrahEntity.Comment);
            Assert.Equal(_lessonId, paragrahEntity.LessonId);

        }


        [Fact]
        public void TestDtoToModel()
        {
            ParagraphDto paragraph = new ParagraphDto(_id, _title, _content, _info, _query, _comment, _lessonId);
            var paragraphMod = paragraph.FromDtoToModel();

            Assert.NotNull(paragraphMod);
            Assert.IsType<Paragraph>(paragraphMod);
            Assert.Equal(0, paragraphMod.Id);
            Assert.Equal(_info, paragraphMod.Info);
            Assert.Equal(_query, paragraphMod.Query);
            Assert.Equal(_comment, paragraphMod.Comment);
            Assert.Equal(0, paragraphMod.LessonId);

        }

        [Fact]
        public void TestEntityToDto()
        {
            ParagraphEntity paragraph = new ParagraphEntity{Id = _id,Title = _title,Content = _content,Info = _info,Query = _query,Comment = _comment,LessonId = _lessonId};
            var paragraphDTO = paragraph.FromEntityToDto();

            Assert.NotNull(paragraphDTO);
            Assert.IsType<ParagraphDto>(paragraphDTO);
            Assert.Equal(1, paragraphDTO.Id);
            Assert.Equal(_title, paragraphDTO.Title);
            Assert.Equal(_content, paragraphDTO.Content);
            Assert.Equal(_info, paragraphDTO.Info);
            Assert.Equal(_query, paragraphDTO.Query);
            Assert.Equal(_comment, paragraphDTO.Comment);
            Assert.Equal(_lessonId, paragraphDTO.LessonId);
        }


        [Fact]
        public void TestEntityToModel()
        {
            ParagraphEntity paragraph = new ParagraphEntity { Id = _id, Title = _title, Content = _content, Info = _info, Query = _query, Comment = _comment, LessonId = _lessonId };
            var paragrahMod = paragraph.FromEntityToModel();

            Assert.NotNull(paragrahMod);
            Assert.IsType<Paragraph>(paragrahMod);
            Assert.Equal(0, paragrahMod.Id);
            Assert.Equal(_info, paragrahMod.Info);
            Assert.Equal(_query, paragrahMod.Query);
            Assert.Equal(_comment, paragrahMod.Comment);
            Assert.Equal(0, paragrahMod.LessonId);
        }



        [Fact]
        public void TestModelToDto()
        {
            Paragraph paragraph = new Paragraph(_id, _title, _content, _info, _query, _comment, _lessonId);
            var paragrahDTO = paragraph.FromModelToDto();

            Assert.NotNull(paragrahDTO);
            Assert.IsType<ParagraphDto>(paragrahDTO);
            Assert.Equal(1, paragrahDTO.Id);
            Assert.Equal(_title, paragrahDTO.Title);
            Assert.Equal(_content, paragrahDTO.Content);
            Assert.Equal(_info, paragrahDTO.Info);
            Assert.Equal(_query, paragrahDTO.Query);
            Assert.Equal(_comment, paragrahDTO.Comment);
            Assert.Equal(_lessonId, paragrahDTO.LessonId);
        }


        [Fact]
        public void TestModelToEntity()
        {
            Paragraph paragraph = new Paragraph(_id, _title, _content, _info, _query, _comment, _lessonId);
            var paragrahEntity = paragraph.FromModelToEntity();

            Assert.NotNull(paragrahEntity);
            Assert.IsType<ParagraphEntity>(paragrahEntity);
            Assert.Equal(1, paragrahEntity.Id);
            Assert.Equal(_info, paragrahEntity.Info);
            Assert.Equal(_query, paragrahEntity.Query);
            Assert.Equal(_comment, paragrahEntity.Comment);
            Assert.Equal(_lessonId, paragrahEntity.LessonId);
        }
    }
}
