﻿using Dto;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Mapper;
using Model;

namespace TestEF.Mapper
{
    public class LessonMapperUnitTest
    {
        private const int _id = 1;
        private const string _title = "Title";
        private const string _lastPublisher = "_lastPublisher";
        private DateOnly date = new DateOnly(2024,03,02);

        [Fact]
        public void TestDtoToEntity()
        {
            LessonDto lesson = new LessonDto(_id, _title, _lastPublisher, date);
            var lessonEntity = lesson.FromDtoToEntity();

            Assert.NotNull(lessonEntity);
            Assert.IsType<LessonEntity>(lessonEntity);
            Assert.Equal(1, lessonEntity.Id);
            Assert.Equal(_title, lessonEntity.Title);
            Assert.Equal(_lastPublisher, lessonEntity.LastPublisher);
        }


        [Fact]
        public void TestDtoToModel()
        {
            LessonDto lesson = new LessonDto(_id, _title, _lastPublisher, date);
            var lessonMod = lesson.FromDtoToModel();

            Assert.NotNull(lessonMod);
            Assert.IsType<Lesson>(lessonMod);
            Assert.Equal(1, lessonMod.Id);
            Assert.Equal(_title, lessonMod.Title);
            Assert.Equal(_lastPublisher, lessonMod.LastPublisher);
        }

        [Fact]
        public void TestEntityToDto()
        {
            LessonEntity lesson = new LessonEntity{Id = _id,Title = _title,LastPublisher = _lastPublisher,LastEdit = date};

            var lessonDto = lesson.FromEntityToDto();

            Assert.NotNull(lessonDto);
            Assert.IsType<LessonDto>(lessonDto);
            Assert.Equal(1, lessonDto.Id);
            Assert.Equal(_title, lessonDto.Title);
            Assert.Equal(_lastPublisher, lessonDto.LastPublisher);
        }

        [Fact]
        public void TestEntityToModel()
        {
            LessonEntity lesson = new LessonEntity { Id = _id, Title = _title, LastPublisher = _lastPublisher, LastEdit = date };

            var lessonMod = lesson.FromEntityToModel();

            Assert.NotNull(lessonMod);
            Assert.IsType<Lesson>(lessonMod);
            Assert.Equal(1, lessonMod.Id);
            Assert.Equal(_title, lessonMod.Title);
            Assert.Equal(_lastPublisher, lessonMod.LastPublisher);
        }



        [Fact]
        public void TestModelToDto()
        {
            Lesson lesson = new Lesson(_id, _title, _lastPublisher, date);

            var lessonDto = lesson.FromModelToDto();

            Assert.NotNull(lessonDto);
            Assert.IsType<LessonDto>(lessonDto);
            Assert.Equal(1, lessonDto.Id);
            Assert.Equal(_title, lessonDto.Title);
            Assert.Equal(_lastPublisher, lessonDto.LastPublisher);
        }

        [Fact]
        public void TestModelToEntity()
        {
            Lesson lesson = new Lesson(_id, _title, _lastPublisher, date);

            var lessonEntity = lesson.FromModelToEntity();

            Assert.NotNull(lessonEntity);
            Assert.IsType<LessonEntity>(lessonEntity);
            Assert.Equal(1, lessonEntity.Id);
            Assert.Equal(_title, lessonEntity.Title);
            Assert.Equal(_lastPublisher, lessonEntity.LastPublisher);
        }
    }
}
