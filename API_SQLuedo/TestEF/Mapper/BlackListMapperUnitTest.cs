﻿using Dto;
using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Mapper;

namespace TestEF.Mapper
{
    public class BlackListMapperUnitTest
    {

        private const string _email = "email@email.com";
        private  DateOnly _date = new DateOnly(2024,12,21) ;


        [Fact]
        public void TestDtoToEntity()
        {
            BlackListDto blacklist = new BlackListDto(_email,_date);
            var blackListEntity = blacklist.FromDtoToEntity();

            Assert.NotNull(blackListEntity);
            Assert.IsType<BlackListEntity>(blackListEntity);
            Assert.Equal(_date, blackListEntity.ExpirationDate);
            Assert.Equal(_email, blackListEntity.Email);

        }

        [Fact]
        public void TestDtoToModel()
        {
            BlackListDto blacklist = new BlackListDto(_email,_date);
            var blackListMod = blacklist.FromDtoToModel();

            Assert.NotNull(blackListMod);
            Assert.IsType<BlackList>(blackListMod);
            Assert.Equal(_date, blackListMod.ExpirationDate);
            Assert.Equal(_email, blackListMod.Email);

        }

        [Fact]
        public void TestEntityToDto()
        {
            BlackListEntity blacklist = new BlackListEntity{Email=_email ,ExpirationDate =_date};
            var BlackListDto = blacklist.FromEntityToDto();

            Assert.NotNull(BlackListDto);
            Assert.IsType<BlackListDto>(BlackListDto);
            Assert.Equal(_date, BlackListDto.ExpirationDate);
            Assert.Equal(_email, BlackListDto.Email);
        }

        [Fact]
        public void TestEntityToModel()
        {
            BlackListEntity blacklist = new BlackListEntity { Email = _email, ExpirationDate = _date };
            var blackListMod = blacklist.FromEntityToModel();

            Assert.NotNull(blackListMod);
            Assert.IsType<BlackList>(blackListMod);
            Assert.Equal(_date, blackListMod.ExpirationDate);
            Assert.Equal(_email, blackListMod.Email);
        }



        [Fact]
        public void TestModelToDto()
        {
            BlackList blacklist = new BlackList(_email,_date);
            var BlackListDto = blacklist.FromModelToDto();

            Assert.NotNull(BlackListDto);
            Assert.IsType<BlackListDto>(BlackListDto);
            Assert.Equal(_date, BlackListDto.ExpirationDate);
            Assert.Equal(_email, BlackListDto.Email);
        }

        [Fact]
        public void TestModelToEntity()
        {
            BlackList blacklist = new BlackList(_email,_date);
            var blackListEntity = blacklist.FromModelToEntity();

            Assert.NotNull(blackListEntity);
            Assert.IsType<BlackListEntity>(blackListEntity);
            Assert.Equal(_date, blackListEntity.ExpirationDate);
            Assert.Equal(_email, blackListEntity.Email);
        }
    }
}
