﻿using Dto;
using Entities;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shared.Mapper;

namespace TestEF.Mapper
{
    public class InquiryTableMapperUnitTest
    {

        private const int _ownerId = 1;
        private const string _databaseName = "Nom";
        private const string _connectionInfo = "Infos";
        


        [Fact]
        public void TestDtoToEntity()
        {
            InquiryTableDto inquiryTable = new InquiryTableDto(_ownerId,_databaseName,_connectionInfo);
            var inquiryTableEntity = inquiryTable.FromDtoToEntity();

            Assert.NotNull(inquiryTableEntity);
            Assert.IsType<InquiryTableEntity>(inquiryTableEntity);
            Assert.Equal(_ownerId, inquiryTableEntity.OwnerId);
            Assert.Equal(_databaseName, inquiryTableEntity.DatabaseName);
            Assert.Equal(_connectionInfo, inquiryTableEntity.ConnectionInfo);

        }

        [Fact]
        public void TestDtoToModel()
        {
            InquiryTableDto inquiryTable = new InquiryTableDto(_ownerId,_databaseName,_connectionInfo);
            var inquiryTableMod = inquiryTable.FromDtoToModel();

            Assert.NotNull(inquiryTableMod);
            Assert.IsType<InquiryTable>(inquiryTableMod);
            Assert.Equal(_ownerId, inquiryTableMod.OwnerId);
            Assert.Equal(_databaseName, inquiryTableMod.ConnectionInfo);
            Assert.Equal(_connectionInfo, inquiryTableMod.DatabaseName);

        }

        [Fact]
        public void TestEntityToDto()
        {
            InquiryTableEntity inquiryTable = new InquiryTableEntity{OwnerId = _ownerId, DatabaseName = _databaseName, ConnectionInfo = _connectionInfo};
            var inquiryTableDto = inquiryTable.FromEntityToDto();

            Assert.NotNull(inquiryTableDto);
            Assert.IsType<InquiryTableDto>(inquiryTableDto);
            Assert.Equal(_ownerId, inquiryTableDto.OwnerId);
            Assert.Equal(_databaseName, inquiryTableDto.DatabaseName);
            Assert.Equal(_connectionInfo, inquiryTableDto.ConnectionInfo);
        }

        [Fact]
        public void TestEntityToModel()
        {
            InquiryTableEntity inquiryTable = new InquiryTableEntity { OwnerId = _ownerId, DatabaseName = _databaseName, ConnectionInfo = _connectionInfo };
            var inquiryTableMod = inquiryTable.FromEntityToModel();

            Assert.NotNull(inquiryTableMod);
            Assert.IsType<InquiryTable>(inquiryTableMod);
            Assert.Equal(_ownerId, inquiryTableMod.OwnerId);
            Assert.Equal(_databaseName, inquiryTableMod.ConnectionInfo);
            Assert.Equal(_connectionInfo, inquiryTableMod.DatabaseName);
        }



        [Fact]
        public void TestModelToDto()
        {
            InquiryTable inquiryTable = new InquiryTable(_ownerId,_databaseName,_connectionInfo);
            var inquiryTableDto = inquiryTable.FromModelToDto();

            Assert.NotNull(inquiryTableDto);
            Assert.IsType<InquiryTableDto>(inquiryTableDto);
            Assert.Equal(_ownerId, inquiryTableDto.OwnerId);
            Assert.Equal(_databaseName, inquiryTableDto.DatabaseName);
            Assert.Equal(_connectionInfo, inquiryTableDto.ConnectionInfo);
        }

        [Fact]
        public void TestModelToEntity()
        {
            InquiryTable inquiryTable = new InquiryTable(_ownerId,_databaseName,_connectionInfo);
            var inquiryTableEntity = inquiryTable.FromModelToEntity();

            Assert.NotNull(inquiryTableEntity);
            Assert.IsType<InquiryTableEntity>(inquiryTableEntity);
            Assert.Equal(_ownerId, inquiryTableEntity.OwnerId);
            Assert.Equal(_databaseName, inquiryTableEntity.DatabaseName);
            Assert.Equal(_connectionInfo, inquiryTableEntity.ConnectionInfo);
        }
    }
}
