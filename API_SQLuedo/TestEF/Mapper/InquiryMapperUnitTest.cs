﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dto;
using Entities;
using Model;
using Shared.Mapper;

namespace TestEF.Mapper
{
    public class InquiryMapperUnitTest
    {
        private const int _id = 1;
        private const string _title = "Title";
        private const string _description = "_description";
        private const bool _isUser = true;

        [Fact]
        public void TestDtoToEntity()
        {
            InquiryDto inquiry = new InquiryDto(_id, _title, _description, _isUser);
            var inquiryEntity = inquiry.FromDtoToEntity();

            Assert.NotNull(inquiryEntity);
            Assert.IsType<InquiryEntity>(inquiryEntity);
            Assert.Equal(1, inquiryEntity.Id);
            Assert.Equal(_title, inquiryEntity.Title);
            Assert.Equal(_description, inquiryEntity.Description);
        }

        [Fact]
        public void TestEntityToDto()
        {
            InquiryEntity inquiry = new InquiryEntity{Id =_id,Title =_title,Description = _description,IsUser= _isUser};

            var inquiryDto = inquiry.FromEntityToDto();

            Assert.NotNull(inquiryDto);
            Assert.IsType<InquiryDto>(inquiryDto);
            Assert.Equal(1, inquiryDto.Id);
            Assert.Equal(_title, inquiryDto.Title);
            Assert.Equal(_description, inquiryDto.Description);
        }


        [Fact]
        public void TestModelToEntity()
        {
            Inquiry inquiry = new Inquiry(_id, _title, _description, _isUser);
            var inquiryEntity = inquiry.FromModelToEntity();

            Assert.NotNull(inquiryEntity);
            Assert.IsType<InquiryEntity>(inquiryEntity);
            Assert.Equal(1, inquiryEntity.Id);
            Assert.Equal(_title, inquiryEntity.Title);
            Assert.Equal(_description, inquiryEntity.Description);
        }

        [Fact]
        public void TestEntityToModel()
        {
            InquiryEntity inquiry = new InquiryEntity { Id = _id, Title = _title, Description = _description, IsUser = _isUser };

            var inquiryMod = inquiry.FromEntityToModel();

            Assert.NotNull(inquiryMod);
            Assert.IsType<Inquiry>(inquiryMod);
            Assert.Equal(1, inquiryMod.Id);
            Assert.Equal(_title, inquiryMod.Title);
            Assert.Equal(_description, inquiryMod.Description);
        }



        [Fact]
        public void TestDtoToModel()
        {
            InquiryDto inquiry = new InquiryDto(_id, _title, _description, _isUser);
            var inquiryMod = inquiry.FromDtoToModel();

            Assert.NotNull(inquiryMod);
            Assert.IsType<Inquiry>(inquiryMod);
            Assert.Equal(1, inquiryMod.Id);
            Assert.Equal(_title, inquiryMod.Title);
            Assert.Equal(_description, inquiryMod.Description);
        }

        [Fact]
        public void TestModelToDto()
        {
            Inquiry inquiry = new Inquiry(_id,_title, _description, _isUser);

            var inquiryDto = inquiry.FromModelToDto();

            Assert.NotNull(inquiryDto);
            Assert.IsType<InquiryDto>(inquiryDto);
            Assert.Equal(1, inquiryDto.Id);
            Assert.Equal(_title, inquiryDto.Title);
            Assert.Equal(_description, inquiryDto.Description);
        }

    }
}
