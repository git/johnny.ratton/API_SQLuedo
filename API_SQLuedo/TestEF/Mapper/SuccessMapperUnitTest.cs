﻿using Dto;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Shared.Mapper;

namespace TestEF.Mapper
{
    public class SuccessMapperUnitTest
    {

        private const int _userId = 1;
        private const int _inquiryId = 2;
        private const bool _isFinished = true;

        [Fact]
        public void TestDtoToEntity()
        {
            SuccessDto success = new SuccessDto(_userId, _inquiryId, _isFinished);
            var successEntity = success.FromDtoToEntity();

            Assert.NotNull(successEntity);
            Assert.IsType<SuccessEntity>(successEntity);
            Assert.Equal(1, successEntity.UserId);
            Assert.Equal(2, successEntity.InquiryId);
            Assert.True(successEntity.IsFinished);

        }

        [Fact]

        public void TestDtoToModel()
        {
            SuccessDto success = new SuccessDto(_userId, _inquiryId, _isFinished);
            var successMod = success.FromDtoToModel();

            Assert.NotNull(successMod);
            Assert.IsType<Success>(successMod);
            Assert.Equal(1, successMod.UserId);
            Assert.Equal(2, successMod.InquiryId);
            Assert.True(successMod.IsFinished);

        }

        [Fact]
        public void TestEntityToDto()
        {
            SuccessEntity success = new SuccessEntity{UserId = _userId,InquiryId = _inquiryId,IsFinished = _isFinished};
            var successDto = success.FromEntityToDto();

            Assert.NotNull(successDto);
            Assert.IsType<SuccessDto>(successDto);
            Assert.Equal(1, successDto.UserId);
            Assert.Equal(2, successDto.InquiryId);
            Assert.True(successDto.IsFinished);
        }

        [Fact]
        public void TestEntityToModel()
        {
            SuccessEntity success = new SuccessEntity { UserId = _userId, InquiryId = _inquiryId, IsFinished = _isFinished };
            var successMod = success.FromEntityToModel();

            Assert.NotNull(successMod);
            Assert.IsType<Success>(successMod);
            Assert.Equal(1, successMod.UserId);
            Assert.Equal(2, successMod.InquiryId);
            Assert.True(successMod.IsFinished);
        }




        [Fact]
        public void TestModelToDto()
        {
            Success success = new Success(_userId, _inquiryId, _isFinished);
            var successDto = success.FromModelToDto();

            Assert.NotNull(successDto);
            Assert.IsType<SuccessDto>(successDto);
            Assert.Equal(1, successDto.UserId);
            Assert.Equal(2, successDto.InquiryId);
            Assert.True((bool)successDto.IsFinished);
        }

        [Fact]
        public void TestModelToEntity()
        {
            Success success = new Success(_userId, _inquiryId, _isFinished);
            var successEntity = success.FromModelToEntity();

            Assert.NotNull(successEntity);
            Assert.IsType<SuccessEntity>(successEntity);
            Assert.Equal(1, successEntity.UserId);
            Assert.Equal(2, successEntity.InquiryId);
            Assert.True(successEntity.IsFinished);
        }
    }
}
