﻿using Dto;
using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using Shared.Mapper;

namespace TestEF.Mapper
{
    public class UserMapperUnitTest
    {

        private const int _userId = 1;
        private const string _username = "Username";
        private const string _password = "password";
        private const string _email = "email@email.com";
        private const bool _isAdmin = true;

        [Fact]
        public void TestDtoToEntity()
        {
            UserDto user = new UserDto(_userId,_username,_password,_email,_isAdmin);
            var userEntity = user.FromDtoToEntity();

            Assert.NotNull(userEntity);
            Assert.IsType<UserEntity>(userEntity);
            Assert.Equal(1, userEntity.Id);
            Assert.Equal("Username", userEntity.Username);
            Assert.Equal("password", userEntity.Password);
            Assert.Equal("email@email.com", userEntity.Email);
            Assert.True(userEntity.IsAdmin);

        }

        [Fact]
        public void TestDtoToModel()
        {
            UserDto user = new UserDto(_userId, _username, _password, _email, _isAdmin);
            var userMod = user.FromDtoToModel();

            Assert.NotNull(userMod);
            Assert.IsType<User>(userMod);
            Assert.Equal(1, userMod.Id);
            Assert.Equal("Username", userMod.Username);
            Assert.Equal("password", userMod.Password);
            Assert.Equal("email@email.com", userMod.Email);
            Assert.True(userMod.IsAdmin);

        }

        [Fact]
        public void TestEntityToDto()
        {
            UserEntity user = new UserEntity{Id = _userId,Username = _username,Password = _password, Email = _email,IsAdmin = _isAdmin};
            var userEntity = user.FromEntityToDto();

            Assert.NotNull(userEntity);
            Assert.IsType<UserDto>(userEntity);
            Assert.Equal(1, userEntity.Id);
            Assert.Equal("Username", userEntity.Username);
            Assert.Equal("password", userEntity.Password);
            Assert.Equal("email@email.com", userEntity.Email);
            Assert.True(userEntity.IsAdmin);
        }

        [Fact]
        public void TestEntityToModel()
        {
            UserEntity user = new UserEntity { Id = _userId, Username = _username, Password = _password, Email = _email, IsAdmin = _isAdmin };
            var userMod = user.FromEntityToModel();

            Assert.NotNull(userMod);
            Assert.IsType<User>(userMod);
            Assert.Equal(1, userMod.Id);
            Assert.Equal("Username", userMod.Username);
            Assert.Equal("password", userMod.Password);
            Assert.Equal("email@email.com", userMod.Email);
            Assert.True(userMod.IsAdmin);
        }



        [Fact]
        public void TestModelToDto()
        {
            User user = new User(_userId, _username, _password, _email, _isAdmin);
            var userDto = user.FromModelToDto();

            Assert.NotNull(userDto);
            Assert.IsType<UserDto>(userDto);
            Assert.Equal(1, userDto.Id);
            Assert.Equal("Username", userDto.Username);
            Assert.Equal("password", userDto.Password);
            Assert.Equal("email@email.com", userDto.Email);
            Assert.True(userDto.IsAdmin);
        }

        [Fact]
        public void TestModelToEntity()
        {
            User user = new User(_userId, _username, _password, _email, _isAdmin);
            var userEntity = user.FromModelToEntity();

            Assert.NotNull(userEntity);
            Assert.IsType<UserEntity>(userEntity);
            Assert.Equal(1, userEntity.Id);
            Assert.Equal("Username", userEntity.Username);
            Assert.Equal("password", userEntity.Password);
            Assert.Equal("email@email.com", userEntity.Email);
            Assert.True(userEntity.IsAdmin);
        }
    }
}
