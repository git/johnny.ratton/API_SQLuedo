﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("BlackList")]
public class BlackListEntity
{
    [Key] public string Email { get; set; }
    public DateOnly ExpirationDate { get; set; }
}