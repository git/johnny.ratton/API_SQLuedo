﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

public abstract class ContentLessonEntity
{
    public int Id { get; set; }
    public string ContentContent { get; set; }
    public string ContentTitle { get; set; }
    [ForeignKey(nameof(Lesson))] public int LessonId { get; set; }
    public LessonEntity Lesson { get; set; } = null!;
}