﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("Paragraph")]
public class ParagraphEntity : ContentLessonEntity
{
    // ID
    // ContentContent
    // ContentTitle
    // LessonId
    public string Title { get; set; }
    public string Content { get; set; }
    public string Info { get; set; }
    public string Query { get; set; }
    public string Comment { get; set; }
}