﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("InquiryTable")]
public class InquiryTableEntity
{
    [Key]
    [ForeignKey(nameof(Owner))]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Required]
    public int OwnerId { get; set; }

    public InquiryEntity Owner { get; set; }
    public string DatabaseName { get; set; }
    public string ConnectionInfo { get; set; }
}