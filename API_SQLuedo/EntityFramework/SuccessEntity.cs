﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("Success")]
public class SuccessEntity
{
    [ForeignKey(nameof(User))] public int UserId { get; set; }
    public UserEntity User { get; set; }

    [ForeignKey(nameof(Inquiry))] public int InquiryId { get; set; }
    public InquiryEntity Inquiry { get; set; }
    public bool IsFinished { get; set; }
}