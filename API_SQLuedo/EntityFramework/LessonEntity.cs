﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("Lesson")]
public class LessonEntity
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string LastPublisher { get; set; }
    public DateOnly LastEdit { get; set; }
    public ICollection<ContentLessonEntity> Content { get; set; } = new List<ContentLessonEntity>();
}