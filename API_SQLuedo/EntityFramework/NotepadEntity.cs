﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("Notepad")]
public class NotepadEntity
{
    public int Id { get; set; }

    [ForeignKey(nameof(User))] public int UserId { get; set; }
    public UserEntity User { get; set; }

    [ForeignKey(nameof(Inquiry))] public int InquiryId { get; set; }
    public InquiryEntity Inquiry { get; set; }
    public string Notes { get; set; }
}