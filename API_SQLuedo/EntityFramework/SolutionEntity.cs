﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities;

[Table("Solution")]
public class SolutionEntity
{
    [Key]
    [ForeignKey(nameof(Owner))]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Required]
    public int OwnerId { get; set; }

    public InquiryEntity? Owner { get; set; }
    public string MurdererFirstName { get; set; }
    public string MurdererLastName { get; set; }
    public string MurderPlace { get; set; }
    public string MurderWeapon { get; set; }
    public string Explaination { get; set; }
}