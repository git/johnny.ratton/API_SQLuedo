using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Model.OrderCriteria;

namespace TestConsoleAPI;

public class LessonTools
{
    private LessonsController _lessonController;

    public LessonTools(LessonsController controller)
    {
        _lessonController = controller;
    }
    
    void PrintLessons()
    {
        Console.WriteLine();
        var lessons = _lessonController.GetLessons(1, 10, LessonOrderCriteria.None) as OkObjectResult;
        foreach (var item in lessons.Value as IEnumerable<LessonDto>)
        {
            Console.WriteLine(item);
        }
    }
    
    void SearchLessonByTitle()
    {
        Console.WriteLine("\nVeuillez saisir le titre de la leçon recherchée : ");
        var title = Console.ReadLine();
        var lesson = _lessonController.GetLessonByTitle(title) as OkObjectResult;
        if (lesson == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(lesson.Value as LessonDto);
    }
    
    void SearchLessonById()
    {
        Console.WriteLine("\nVeuillez saisir l'identifiant de la leçon recherchée : ");
        var id = Console.ReadLine();
        var lesson = _lessonController.GetLessonById(int.Parse(id)) as OkObjectResult;
        if (lesson == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(lesson.Value as LessonDto);
    }
    
    void AddLesson()
    {
        Console.WriteLine("Veuillez saisir le titre :");
        var title = Console.ReadLine();
        Console.WriteLine("Veuillez saisir votre nom :");
        var lastPublisher = Console.ReadLine();
        var res = _lessonController.CreateLesson(
            new LessonDto(title, lastPublisher, DateOnly.FromDateTime(DateTime.Now)));
        if (res.GetType() == typeof(CreatedResult))
        {
            Console.WriteLine("\nLeçon créée avec succès");
        }
        else
        {
            Console.WriteLine("\nErreur lors de la création de la leçon !");
        }
    }
    
    void UpdateLesson()
    {
        Console.WriteLine("Quel est l'identifiant de la leçon à mettre à jour ?");
        var id = int.Parse(Console.ReadLine());
        var res = (_lessonController.GetLessonById(id));
        if (res.GetType() == typeof(OkObjectResult))
        {
            var lesson = (res as OkObjectResult).Value as LessonDto;
            if (lesson == null)
            {
                Console.WriteLine("Erreur, un problème est survenu");
                return;
            }
            else
            {
                Console.WriteLine("Leçon trouvée !\n");
                Console.WriteLine("Veuillez saisir le titre :");
                var title = Console.ReadLine();
                Console.WriteLine("Veuillez saisir votre nom :");
                var lastPublisher = Console.ReadLine();
                var retour = _lessonController.UpdateLesson(id,
                    new LessonDto(id, title, lastPublisher, DateOnly.FromDateTime(DateTime.Now)));
                if (retour.GetType() == typeof(OkObjectResult))
                {
                    Console.WriteLine("Mise à jour effectué avec succès !");
                }
                else
                {
                    Console.WriteLine("Une erreur est survenue lors de la mise à jour.");
                }
            }
        }
        else
        {
            Console.WriteLine("Une erreur est survenue lors de la mise à jour !");
        }
    }
    
    void DeleteLesson()
    {
        Console.WriteLine("Quel est l'identifiant de la leçon à supprimer ?");
        var id = int.Parse(Console.ReadLine());
        var res = _lessonController.DeleteLesson(id);
        if (res.GetType() == typeof(OkObjectResult))
        {
            Console.WriteLine("La suppression a été effectuée avec succès !");
        }
        else
        {
            Console.WriteLine("Erreur lors de la suppression !");
        }
    }
}