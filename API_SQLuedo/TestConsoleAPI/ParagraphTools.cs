using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Model.OrderCriteria;

namespace TestConsoleAPI;

public class ParagraphTools
{
    private ParagraphsController _paragraphController;

    public ParagraphTools(ParagraphsController controller)
    {
        _paragraphController = controller;
    }
    
    void PrintParagraphs()
    {
        Console.WriteLine();
        var paragraphs = _paragraphController.GetParagraphs(1, 10, ParagraphOrderCriteria.None) as OkObjectResult;
        foreach (var item in paragraphs.Value as IEnumerable<ParagraphDto>)
        {
            Console.WriteLine(item);
        }
    }
    
    void SearchParagraphByTitle()
    {
        Console.WriteLine("\nVeuillez saisir le titre du paragraphe recherché : ");
        var title = Console.ReadLine();
        var paragraph = _paragraphController.GetParagraphByTitle(title) as OkObjectResult;
        if (paragraph == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(paragraph.Value as ParagraphDto);
    }
    
    void SearchParagraphById()
    {
        Console.WriteLine("\nVeuillez saisir l'identifiant du paragraphe recherché : ");
        var id = Console.ReadLine();
        var paragraph = _paragraphController.GetParagraphById(int.Parse(id)) as OkObjectResult;
        if (paragraph == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(paragraph.Value as ParagraphDto);
    }
    
    void AddParagraph()
    {
        Console.WriteLine("Veuillez saisir le titre :");
        var title = Console.ReadLine();
        Console.WriteLine("Veuillez saisir un contenu :");
        var content = Console.ReadLine();
        Console.WriteLine("Veuillez saisir une information :");
        var info = Console.ReadLine();
        Console.WriteLine("Veuillez saisir une query :");
        var query = Console.ReadLine();
        Console.WriteLine("Veuillez saisir un commentaire :");
        var comment = Console.ReadLine();
        Console.WriteLine("Veuillez saisir l'id de la leçon :");
        var lesson = Convert.ToInt32(Console.ReadLine());
        var res = _paragraphController.CreateParagraph(new ParagraphDto(title, content, info, query, comment, lesson));
        if (res.GetType() == typeof(CreatedResult))
        {
            Console.WriteLine("\nParagraphe créé avec succès");
        }
        else
        {
            Console.WriteLine("\nErreur lors de la création du paragraphe !");
        }
    }
    
    void UpdateParagraph()
    {
        Console.WriteLine("Quel est l'identifiant du paragraphe à mettre à jour ?");
        var id = int.Parse(Console.ReadLine());
        var res = (_paragraphController.GetParagraphById(id));
        if (res.GetType() == typeof(OkObjectResult))
        {
            var paragraph = (res as OkObjectResult).Value as ParagraphDto;
            if (paragraph == null)
            {
                Console.WriteLine("Erreur, un problème est survenu");
                return;
            }
            else
            {
                Console.WriteLine("Paragraphe trouvé !\n");
                Console.WriteLine("Veuillez saisir le titre :");
                var title = Console.ReadLine();
                Console.WriteLine("Veuillez saisir un contenu :");
                var content = Console.ReadLine();
                Console.WriteLine("Veuillez saisir une information :");
                var info = Console.ReadLine();
                Console.WriteLine("Veuillez saisir une query :");
                var query = Console.ReadLine();
                Console.WriteLine("Veuillez saisir un commentaire :");
                var comment = Console.ReadLine();
                Console.WriteLine("Veuillez saisir l'id de la leçon :");
                var lesson = Convert.ToInt32(Console.ReadLine());
                var retour = _paragraphController.UpdateParagraph(id,
                    new ParagraphDto(id, title, content, info, query, comment, lesson));
                if (retour.GetType() == typeof(OkObjectResult))
                {
                    Console.WriteLine("Mise à jour effectué avec succès !");
                }
                else
                {
                    Console.WriteLine("Une erreur est survenue lors de la mise à jour.");
                }
            }
        }
        else
        {
            Console.WriteLine("Une erreur est survenue lors de la mise à jour !");
        }
    }
    
    void DeleteParagraph()
    {
        Console.WriteLine("Quel est l'identifiant du paragraphe à supprimer ?");
        var id = int.Parse(Console.ReadLine());
        var res = _paragraphController.DeleteParagraph(id);
        if (res.GetType() == typeof(OkObjectResult))
        {
            Console.WriteLine("La suppression a été effectuée avec succès !");
        }
        else
        {
            Console.WriteLine("Erreur lors de la suppression !");
        }
    }
}