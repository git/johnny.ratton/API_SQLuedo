using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Model.OrderCriteria;

namespace TestConsoleAPI;

public class SuccessTools
{
    private SuccessesController _successController;

    public SuccessTools(SuccessesController controller)
    {
        _successController = controller;
    }
    
    void PrintSuccesses()
    {
        Console.WriteLine();
        var successes = _successController.GetSuccesses(1, 10, SuccessOrderCriteria.None) as OkObjectResult;
        foreach (var item in successes.Value as IEnumerable<SuccessDto>)
        {
            Console.WriteLine(item);
        }
    }
    
    void SearchSuccessByUserId()
    {
        Console.WriteLine("\nVeuillez saisir l'identifiant de l'utilisateur du succès recherché : ");
        var id = Console.ReadLine();
        var success = _successController.GetSuccessByUserId(int.Parse(id)) as OkObjectResult;
        if (success == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(success.Value as SuccessDto);
    }
    
    void SearchSuccessByInquiryId()
    {
        Console.WriteLine("\nVeuillez saisir l'identifiant de l'enquête du succès recherché : ");
        var id = Console.ReadLine();
        var success = _successController.GetSuccessByInquiryId(int.Parse(id)) as OkObjectResult;
        if (success == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(success.Value as SuccessDto);
    }

    void AddSuccess()
    {
        Console.WriteLine("Veuillez saisir l'identifiant de l'utilisateur lié au succès :");
        var userId = Console.ReadLine();
        Console.WriteLine("Veuillez saisir l'identifiant de l'enquête lié au succès :");
        var inquiryId = Console.ReadLine();
        Console.WriteLine("Veuillez indiquer si l'enquête a été complété (true/false) :");
        var isFinished = Console.ReadLine();
        var res = _successController.CreateSuccess(new SuccessDto(int.Parse(userId), int.Parse(inquiryId),
            bool.Parse(isFinished)));
        if (res.GetType() == typeof(CreatedResult))
        {
            Console.WriteLine("\nSuccès créé avec succès");
        }
        else
        {
            Console.WriteLine("\nErreur lors de la création du succès !");
        }
    }
    
    void UpdateSuccess()
    {
        Console.WriteLine("Quel est l'identifiant de l'utilisateur lié au succès à mettre à jour ?");
        var id = int.Parse(Console.ReadLine());
        var res = (_successController.GetSuccessByUserId(id));
        if (res.GetType() == typeof(OkObjectResult))
        {
            var lesson = (res as OkObjectResult).Value as SuccessDto;
            if (lesson == null)
            {
                Console.WriteLine("Erreur, un problème est survenu");
                return;
            }
            else
            {
                Console.WriteLine("Succès trouvé !\n");
                Console.WriteLine("Veuillez saisir l'identifiant de l'utilisateur lié au succès :");
                var userId = int.Parse(Console.ReadLine());
                Console.WriteLine("Veuillez saisir l'identifiant de l'enquête lié au succès :");
                var inquiryId = int.Parse(Console.ReadLine());
                Console.WriteLine("Veuillez saisir si l'enquête est terminée (0/1) :");
                var isFinished = Console.ReadLine();
                var retour = _successController.UpdateSuccess(userId, inquiryId,
                    new SuccessDto(userId, inquiryId, bool.Parse(isFinished)));
                if (retour.GetType() == typeof(OkObjectResult))
                {
                    Console.WriteLine("Mise à jour effectué avec succès !");
                }
                else
                {
                    Console.WriteLine("Une erreur est survenue lors de la mise à jour.");
                }
            }
        }
        else
        {
            Console.WriteLine("Une erreur est survenue lors de la mise à jour !");
        }
    }
    
    void DeleteSuccess()
    {
        Console.WriteLine("Quel est l'identifiant de l'utilisateur lié au succès à supprimer ?");
        var idUser = int.Parse(Console.ReadLine());
        Console.WriteLine("Quel est l'identifiant de l'enquête lié au succès à supprimer ?");
        var idInquiry = int.Parse(Console.ReadLine());
        var res = _successController.DeleteSuccess(idUser, idInquiry);
        if (res.GetType() == typeof(OkObjectResult))
        {
            Console.WriteLine("La suppression a été effectuée avec succès !");
        }
        else
        {
            Console.WriteLine("Erreur lors de la suppression !");
        }
    }
}