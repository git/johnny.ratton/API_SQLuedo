using API.Controllers;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Model.OrderCriteria;

namespace TestConsoleAPI;

public class InquiryTools
{
    private InquiriesController _inquiryController;

    public InquiryTools(InquiriesController controller)
    {
        _inquiryController = controller;
    }
    void PrintInquiries()
    {
        Console.WriteLine();
        var inquiries = _inquiryController.GetInquiries(1, 10, InquiryOrderCriteria.None) as OkObjectResult;
        foreach (var item in inquiries.Value as IEnumerable<InquiryDto>)
        {
            Console.WriteLine(item);
        }
    }
    
    void SearchInquiryByTitle()
    {
        Console.WriteLine("\nVeuillez saisir le titre de l'enquête recherchée : ");
        var title = Console.ReadLine();
        var inquiry = _inquiryController.GetInquiryByTitle(title) as OkObjectResult;
        if (inquiry == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(inquiry.Value as InquiryDto);
    }
    
    void SearchInquiryById()
    {
        Console.WriteLine("\nVeuillez saisir l'identifiant de l'enquête recherchée : ");
        var id = Console.ReadLine();
        var inquiry = _inquiryController.GetInquiryById(int.Parse(id)) as OkObjectResult;
        if (inquiry == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(inquiry.Value as InquiryDto);
    }
    
    void AddInquiry()
    {
        Console.WriteLine("Veuillez saisir le titre :");
        var title = Console.ReadLine();
        Console.WriteLine("Veuillez saisir une description :");
        var description = Console.ReadLine();
        Console.WriteLine("Veuillez saisir un isUser (false / true) :");
        var isUser = Convert.ToBoolean(Console.ReadLine());
        Console.WriteLine("Veuillez saisir l'id de la database :");
        var database = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("Veuillez saisir l'id de la solution :");
        var solution = Convert.ToInt32(Console.ReadLine());
        var res = _inquiryController.CreateInquiry(new InquiryDto(title, description, false));
        if (res.GetType() == typeof(CreatedResult))
        {
            Console.WriteLine("\nEnquête créée avec succès");
        }
        else
        {
            Console.WriteLine("\nErreur lors de la création de l'enquête !");
        }
    }
    
    void UpdateInquiry()
    {
        Console.WriteLine("Quel est l'identifiant de l'enquête à mettre à jour ?");
        var id = int.Parse(Console.ReadLine());
        var res = (_inquiryController.GetInquiryById(id));
        if (res.GetType() == typeof(OkObjectResult))
        {
            var user = (res as OkObjectResult).Value as InquiryDto;
            if (user == null)
            {
                Console.WriteLine("Erreur, un problème est survenu");
                return;
            }
            else
            {
                Console.WriteLine("Enquête trouvée !\n");
                Console.WriteLine("Veuillez saisir le titre :");
                var title = Console.ReadLine();
                Console.WriteLine("Veuillez saisir la description :");
                var description = Console.ReadLine();
                Console.WriteLine("Veuillez indiquer si l'enquête est accessible aux visiteurs (0/1) :");
                var isUser = Console.ReadLine();
                Console.WriteLine("Veuillez saisir un commentaire :");
                var database = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Veuillez saisir un commentaire :");
                var inquiryTable = Convert.ToInt32(Console.ReadLine());
                var retour =
                    _inquiryController.UpdateInquiry(id, new InquiryDto(id, title, description, bool.Parse(isUser)));
                if (retour.GetType() == typeof(OkObjectResult))
                {
                    Console.WriteLine("Mise à jour effectué avec succès !");
                }
                else
                {
                    Console.WriteLine("Une erreur est survenue lors de la mise à jour.");
                }
            }
        }
        else
        {
            Console.WriteLine("Une erreur est survenue lors de la mise à jour !");
        }
    }

    void DeleteInquiry()
    {
        Console.WriteLine("Quel est l'identifiant de l'enquête à supprimer ?");
        var id = int.Parse(Console.ReadLine());
        var res = _inquiryController.DeleteInquiry(id);
        if (res.GetType() == typeof(OkObjectResult))
        {
            Console.WriteLine("La suppression a été effectuée avec succès !");
        }
        else
        {
            Console.WriteLine("Erreur lors de la suppression !");
        }
    }
}