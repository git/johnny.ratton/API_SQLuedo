using API.Controllers;
using API.Service;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Model.OrderCriteria;

namespace TestConsoleAPI;

public class UserTools
{
    private UsersController _userController;

    public UserTools(UsersController controller)
    {
        _userController = controller;
    }
    public void PrintUsers()
    {
        Console.WriteLine();
        var users = _userController.GetUsers(1, 10, UserOrderCriteria.None) as OkObjectResult;
        foreach (var item in users.Value as IEnumerable<UserDto>)
        {
            Console.WriteLine(item);
        }
    }
    
    public void SearchUserByUsername()
    {
        Console.WriteLine("\nVeuillez saisir le pseudonyme de l'utilisateur recherché : ");
        var username = Console.ReadLine();
        var user = _userController.GetUserByUsername(username) as OkObjectResult;
        if (user == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(user.Value as UserDto);
    }
    
    public void SearchUserById()
    {
        Console.WriteLine("\nVeuillez saisir l'identifiant de l'utilisateur recherché : ");
        var id = Console.ReadLine();
        var user = _userController.GetUserById(int.Parse(id)) as OkObjectResult;
        if (user == null)
        {
            Console.WriteLine("Erreur, la requête n'a rien donné.");
            return;
        }

        Console.WriteLine(user.Value as UserDto);
    }
    
    public void AddUser()
    {
        Console.WriteLine("Veuillez saisir le pseudonyme :");
        var username = Console.ReadLine();
        Console.WriteLine("Veuillez saisir une adresse email :");
        var email = Console.ReadLine();
        Console.WriteLine("Veuillez saisir un mot de passe :");
        var mdp = Console.ReadLine();
        var res = _userController.CreateUser(new UserDto(username, mdp, email, false));
        if (res.GetType() == typeof(CreatedResult))
        {
            Console.WriteLine("\nUtilisateur créé avec succès");
        }
        else
        {
            Console.WriteLine("\nErreur lors de la création de l'utilisateur !");
        }
    }
    
    public void UpdateUser()
    {
        Console.WriteLine("Quel est l'identifiant de l'utilisateur à mettre à jour ?");
        var id = int.Parse(Console.ReadLine());
        var res = (_userController.GetUserById(id));
        if (res.GetType() == typeof(OkObjectResult))
        {
            var user = (res as OkObjectResult).Value as UserDto;
            if (user == null)
            {
                Console.WriteLine("Erreur, un problème est survenu");
                return;
            }
            else
            {
                Console.WriteLine("Utilisateur trouvé !\n");
                Console.WriteLine("Veuillez saisir le pseudonyme :");
                var username = Console.ReadLine();
                Console.WriteLine("Veuillez saisir l'email :");
                var email = Console.ReadLine();
                var retour =
                    _userController.UpdateUser(id, new UserDto(id, username, user.Password, email, user.IsAdmin));
                if (retour.GetType() == typeof(OkObjectResult))
                {
                    Console.WriteLine("Mise à jour effectué avec succès !");
                }
                else
                {
                    Console.WriteLine("Une erreur est survenue lors de la mise à jour.");
                }
            }
        }
        else
        {
            Console.WriteLine("Une erreur est survenue lors de la mise à jour !");
        }
    }
    
    public void DeleteUser()
    {
        Console.WriteLine("Quel est l'identifiant de lutilisateur à supprimer ?");
        var id = int.Parse(Console.ReadLine());
        var res = _userController.DeleteUser(id);
        if (res.GetType() == typeof(OkObjectResult))
        {
            Console.WriteLine("La suppression a été effectuée avec succès !");
        }
        else
        {
            Console.WriteLine("Erreur lors de la suppression !");
        }
    }

    public void AutoTestsUsersPrintAll()
    {
        // Affichage des utilisateurs
        Console.WriteLine("\n##########################################################\n");
        Console.WriteLine("Affichages des utilisateurs stubbés dans le contexte :\n");
        var res = _userController.GetUsers(1, 10, UserOrderCriteria.None);
        if (res == null)
        {
            Console.WriteLine("\nErreur lors de l'acquisition de la liste des utilisateurs");
        }
        else
        {
            var users = res as IEnumerable<UserDto>;
            if (users == null)
            {
                Console.WriteLine("\nErreur, les ustilisateurs n'ont pas été trouvés !");
            }
            else
            {
                PrintUsers();
            }
        }
    }

    public void AutoTestUsersSearchById()
    {
        // Recherche d'utilisateur par ID
        Console.WriteLine("\n##########################################################\n");
        Console.WriteLine("Affichage de l'utilisateur ayant pour identifiant 1 :\n");
        var res1 = _userController.GetUserById(1);
        if (res1 == null)
        {
            Console.WriteLine("\nErreur lors de l'acquisition de l'utilisateur !");
        }
        else
        {
            var user = (res1 as OkObjectResult).Value ;
            if (user == null)
            {
                Console.WriteLine("\nErreur, l'utilisateur n'existe pas !");
            }
            else
            {
                Console.WriteLine(user);
            }
        }
    }

    public void AutoTestUsersSearchByUsername()
    {
        // Recherche d'utilisateur par pseudonyme
        Console.WriteLine("\n##########################################################\n");
        Console.WriteLine("Affichage de l'utilisateur ayant pour username johnny :\n");
        var res2 = _userController.GetUserByUsername("johnny");
        if (res2 == null)
        {
            Console.WriteLine("\nErreur lors de l'acquisition de l'utilisateur !");
        }
        else
        {
            var user1 = (res2 as OkObjectResult).Value;
            if (user1 == null)
            {
                Console.WriteLine("\nErreur, l'utilisateur n'existe pas !");
            }
            else
            {
                Console.WriteLine(user1);
            }
        }
    }

    public void AutoTestUsersAddAndUpdate()
    {
        // Ajout d'un utilisateur
        Console.WriteLine("\n##########################################################\n");
        Console.WriteLine("Création de l'utilisateur :\n");
        var user2 = new UserDto("JohnDoe", "motdepasse", "johndoe@gmail.com", false);
        Console.WriteLine(user2);
        var res3 = _userController.CreateUser(user2);
        if (res3 == null)
        {
            Console.WriteLine("\nErreur lors de la création de l'utilisateur !");
        }
        else
        {
            Console.WriteLine("\nUtilisateur créé avec succès !");
        }
        // Affichage des utilisateurs
        Console.WriteLine("\n##########################################################\n");
        PrintUsers();

        // Mise à jour d'un utilisateur
        Console.WriteLine("\n##########################################################\n");
        Console.WriteLine("Mise à jour de l'adresse email de l'utilisateur :\n");
        if (_userController.GetUserByUsername("JohnDoe") == null)
        {
            Console.WriteLine("\nErreur lors de la récupération de l'utilisateur !");
        }
        else
        {
            Console.WriteLine(user2);
            Console.WriteLine("\nNouvelle adresse : John.DOE@etu.uca.fr");
            var user3 = new UserDto(user2.Id, user2.Username, user2.Password, "John.DOE@etu.uca.fr", user2.IsAdmin);
            var res4 = _userController.UpdateUser(1, user3);
            if (res4 == null)
            {
                Console.WriteLine("\nErreur lors de la mise à jour de l'utilisateur !");
            }
            else
            {
                Console.WriteLine("\nUtilisateur mis à jour avec succès !");
            }
        }
    }

    public void AutoTestUsersDelete()
    {
        var user2 = new UserDto("JohnDoe", "motdepasse", "johndoe@gmail.com", false);
        // Suppression d'un utilisateur
        Console.WriteLine("\n##########################################################\n");
        Console.WriteLine("Suppression de l'utilisateur JohnDoe:\n");
        if (_userController.GetUserByUsername("JohnDoe") == null)
        {
            Console.WriteLine("\nErreur lors de la récupération de l'utilisateur !");
        }
        else
        {
            Console.WriteLine(user2);
            var res5 = _userController.DeleteUser(user2.Id);
            if (res5 == null)
            {
                Console.WriteLine("\nErreur lors de la suppression de l'utilisateur !");
            }
            else
            {
                Console.WriteLine("\nUtilisateur supprimé avec succès !");
            }
        }
    }
    public void AutoTestsUsers()
    {
        AutoTestsUsersPrintAll();
        AutoTestUsersSearchById();
        AutoTestUsersSearchByUsername();
        AutoTestUsersAddAndUpdate();
        // Affichage des utilisateurs

        Console.WriteLine("\n##########################################################\n");
        PrintUsers();
        AutoTestUsersDelete();
        // Affichage des utilisateurs

        Console.WriteLine("\n##########################################################\n");
        PrintUsers();
    }
}