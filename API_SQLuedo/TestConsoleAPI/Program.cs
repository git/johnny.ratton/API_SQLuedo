﻿// See https://aka.ms/new-console-template for more information

using API.Controllers;
using API.Service;
using DbContextLib;
using DbDataManager.Service;
using Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Model.OrderCriteria;
using TestConsoleAPI;
using Service_LessonDataService = DbDataManager.Service.LessonDataService;
using Service_ParagraphDataService = DbDataManager.Service.ParagraphDataService;
using Service_SuccessDataService = DbDataManager.Service.SuccessDataService;
using Service_UserDataService = DbDataManager.Service.UserDataService;

var connection = new SqliteConnection("DataSource=:memory:");
connection.Open();
var options = new DbContextOptionsBuilder<UserDbContext>()
    .UseSqlite(connection)
    .Options;
using ILoggerFactory factory = new LoggerFactory();
ILogger<UsersController> userLogger = factory.CreateLogger<UsersController>();
ILogger<InquiriesController> inquiryLogger = factory.CreateLogger<InquiriesController>();
ILogger<ParagraphsController> paragraphLogger = factory.CreateLogger<ParagraphsController>();
ILogger<LessonsController> lessonLogger = factory.CreateLogger<LessonsController>();
ILogger<SuccessesController> successLogger = factory.CreateLogger<SuccessesController>();


using (var context = new UserDbContext(options))
{
    var userController = new UsersController(userLogger, new UserDataServiceApi(new Service_UserDataService(context)));
    var inquiryController =
        new InquiriesController(new InquiryDataServiceApi(new InquiryDataService(context)), inquiryLogger);
    var paragraphController =
        new ParagraphsController(new ParagraphDataServiceApi(new Service_ParagraphDataService(context)),
            paragraphLogger);
    var lessonController =
        new LessonsController(new LessonDataServiceApi(new Service_LessonDataService(context)), lessonLogger);
    var successController = new SuccessesController(new SuccessDataServiceApi(new Service_SuccessDataService(context)),
        successLogger);

    UserTools userTools = new UserTools(userController);
    InquiryTools inquiryTools = new InquiryTools(inquiryController);
    LessonTools lessonTools = new LessonTools(lessonController);
    ParagraphTools paragraphTools = new ParagraphTools(paragraphController);
    SuccessTools successTools = new SuccessTools(successController);

    void MenuUsers()
    {
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("|                  MenuUsers                     |");
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("| t - Effectuer des tests automatiques           |");
        Console.WriteLine("| 1 - Afficher les utilisateurs                  |");
        Console.WriteLine("| 2 - Rechercher un utilisateur avec son pseudo  |");
        Console.WriteLine("| 3 - Rechercher un utilisateur avec son ID      |");
        Console.WriteLine("| 4 - Ajouter un utilisateur                     |");
        Console.WriteLine("| 5 - Mettre à jour un utilisateur               |");
        Console.WriteLine("| 6 - Supprimer un utilisateur                   |");
        Console.WriteLine("| q - Quitter                                    |");
        Console.WriteLine("|------------------------------------------------|");
    }

    void MenuInquiries()
    {
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("|                  MenuInquiries                 |");
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("| 1 - Afficher les enquêtes                      |");
        Console.WriteLine("| 2 - Rechercher une enquête avec son titre      |");
        Console.WriteLine("| 3 - Rechercher une enquête avec son ID         |");
        Console.WriteLine("| 4 - Ajouter une enquête                        |");
        Console.WriteLine("| 5 - Mettre à jour une enquête                  |");
        Console.WriteLine("| 6 - Supprimer une enquête                      |");
        Console.WriteLine("| q - Quitter                                    |");
        Console.WriteLine("|------------------------------------------------|");
    }

    void MenuParagraphs()
    {
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("|                  MenuParagraphs                |");
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("| 1 - Afficher les paragraphes                   |");
        Console.WriteLine("| 2 - Rechercher un paragraphe avec son titre    |");
        Console.WriteLine("| 3 - Rechercher un paragraphe avec son ID       |");
        Console.WriteLine("| 4 - Ajouter un paragraphe                      |");
        Console.WriteLine("| 5 - Mettre à jour un paragraphe                |");
        Console.WriteLine("| 6 - Supprimer un paragraphe                    |");
        Console.WriteLine("| q - Quitter                                    |");
        Console.WriteLine("|------------------------------------------------|");
    }

    void MenuLessons()
    {
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("|                  MenuLessons                   |");
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("| 1 - Afficher les leçons                        |");
        Console.WriteLine("| 2 - Rechercher une leçon avec son titre        |");
        Console.WriteLine("| 3 - Rechercher une leçon avec son ID           |");
        Console.WriteLine("| 4 - Ajouter une leçon                          |");
        Console.WriteLine("| 5 - Mettre à jour une leçon                    |");
        Console.WriteLine("| 6 - Supprimer une leçon                        |");
        Console.WriteLine("| q - Quitter                                    |");
        Console.WriteLine("|------------------------------------------------|");
    }

    void MenuSuccesses()
    {
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("|                  MenuSuccesses                 |");
        Console.WriteLine("|------------------------------------------------|");
        Console.WriteLine("| 1 - Afficher les succès                        |");
        Console.WriteLine("| 2 - Rechercher un succès avec son userId       |");
        Console.WriteLine("| 3 - Rechercher un succès avec son inquiryId    |");
        Console.WriteLine("| 4 - Ajouter un succès                          |");
        Console.WriteLine("| 5 - Mettre à jour un succès                    |");
        Console.WriteLine("| 6 - Supprimer un succès                        |");
        Console.WriteLine("| q - Quitter                                    |");
        Console.WriteLine("|------------------------------------------------|");
    }

    MenuUsers();
    //MenuInquiries();
    //MenuParagraphs();
    //MenuLessons();
    //MenuSuccesses();
    Console.WriteLine("\nSaisie :");
    var saisie = Console.ReadLine();

    while (saisie != "q")
    {
        switch (saisie)
        {
            case "1":
                userTools.PrintUsers();
                //inquiryTools.PrintInquiries();
                //paragraphTools.PrintParagraphs();
                //lessonTools.PrintLessons();
                //successTools.PrintSuccesses();
                break;
            case "2":
                userTools.SearchUserByUsername();
                //inquiryTools.SearchInquiryByTitle();
                //paragraphTools.SearchParagraphByTitle();
                //lessonTools.SearchLessonByTitle();
                //successTools.SearchSuccessByUserId();
                break;
            case "3":
                userTools.SearchUserById();
                //inquiryTools.SearchInquiryById();
                //paragraphTools.SearchParagraphById();
                //lessonTools.SearchLessonById();
                //successTools.SearchSuccessByInquiryId();
                break;
            case "4":
                userTools.AddUser();
                //inquiryTools.AddInquiry();
                //paragraphTools.AddParagraph();
                //lessonTools.AddLesson();
                //successTools.AddSuccess();
                break;
            case "5":
                userTools.UpdateUser();
                //inquiryTools.UpdateInquiry();
                //paragraphTools.UpdateParagraph();
                //lessonTools.UpdateLesson();
                //successTools.UpdateSuccess();
                break;
            case "6":
                userTools.DeleteUser();
                //inquiryTools.DeleteInquiry();
                //paragraphTools.DeleteParagraph();
                //lessonTools.DeleteLesson();
                //successTools.UpdateSuccess();
                break;
            case "t":
                userTools.AutoTestsUsers();
                break;
            default:
                break;
        }

        Console.WriteLine("\nAppuyez sur n'importe quelle touche pour continuer...");
        Console.ReadKey();
        Console.Clear();
        MenuUsers();
        //MenuInquiries();
        //MenuParagraphs();
        //MenuLessons();
        //MenuSuccesses();
        Console.WriteLine("\nSaisie :");
        saisie = Console.ReadLine();
    }
}

    