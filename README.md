# API_SQLuedo

## Configuration de l'API

### Au niveau de la solution du projet :

- > Création des migrations

```sh
    dotnet ef migrations add MigrationName --project .\DbContextLib --context UserDbContext --startup-project .\API
```

- > Mise à jour de la base de données :

```sh
    dotnet ef database update --project .\DbContextLib --startup-project .\API
```

- > Exécuter le projet de l'API avec votre IDE

![alt text](Ressources/image.png)

<br>

## Caractéristiques de l'API

### Source de données
---
L'API est actuellement reliée à un projet __EntityFramework__ qui lui fournit les données dont elle a besoin.
La partie EntityFramework est, elle, reliée à une base de donnée __PostgreSQL__ mais peut très bien être reliée à un autre fournisseur de BDD en injectant un DbContextOptions configuré dans le au moment de l'instanciation du UserDbContext.

---
### Injecter un fournisseur de BDD alternatif
```C#
    var options = new DbContextOptionsBuilder()
        .UseNpgsql()
        .Options;

    var dbContext = new UserDbContext(options);
``` 
---
### Configuration de la base de données

Pour ce qui est de la base de données, il faut évidemment installer PostgreSQL.<br>
La classe UserDbContext utilise par défaut PostgreSQL à partir d'un utilisateur __admin__ qu'il faut créer au préalable. L'utilisateur __admin__ devra posséder le droit de créer une base de données et devra disposer de tous les droits d'édition sur celle-ci.

```SQL
    CREATE USER admin WITH PASSWORD "motdepasse";
    GRANT pg_read_all_data TO admin;
    GRANT pg_write_all_data TO admin;
    ALTER USER admin CREATEDB;
```
---
### Dans le DbContext

Il faut également adapter la configuration de la connexion à la base de données dans le UserDbContext afin de l'adapter en fonction de la configuration que vous décidez de faire. Par défaut, si vous avez une base PostgreSQL sur la même machine que celle qui fait tourner l'API et que vous avez utilisé les commandes SQL fournies à l'identique, cela devrait fonctionner sans problème. Sinon, vous pouvez modifier le DbContext dans la méthode OnConfiguring ci-dessous :
>UserDbContext.cs
```C#
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        { // Modifier la ligne ci-dessous en fonction de votre configuration
            optionsBuilder.UseNpgsql("Host=localhost;Database=SQLuedo;Username=admin;Password=motdepasse");
        }
        base.OnConfiguring(optionsBuilder);
    }
```

---

### Utilisation de l'interface Swagger
Pour tester manuellement l'API, à l'exécution du projet de celle-ci, une interface Swagger s'ouvre dans votre navigateur. Etant donné que l'API est configurée pour demander une autentification à l'utilisateur, vous devrez alors <span style="color: green;">créer un compte sur Swagger</span> (qui ne sera valable que pour l'exécution en cours).

Vous pourrez alors pour cela sélectionner la rubrique <span style="color: green;">Register</span> et entrer une __adresse email__ et un __mot de passe__ quelconque dans l'objet Json puis appuyer sur __Exécuter__.

Ensuite, il vous faudra vous connecter via la rubrique <span style="color: green;">Login</span> pour laquelle vous n'aurez qu'à entrer l'email et le mot de passe précédents. Vous pouvez également activer les cookies si vous le souhaitez pour une persistance de la connexion.

Voilà, vous pouvez maintenant utilisez librement l'API !

---

### Versionnement 

Notre API est versionnée ce qui signifie qu'à chaque requête, la version souhaitée de l'API doit être fournie. Pour l'instant, il n'y a qu'une __version 1.0__.

--- 